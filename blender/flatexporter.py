#
# blender plugin to export for my 3d ascii terminal engine
#

bl_info = {
    "name": "Export Flat",
    "author": "Daniel Balster",
    "version": (0, 3),
    "blender": (3, 0, 0),
    "location": "File > Export > Flat",
    "description": "Export Flat",
    "warning": "",
    "category": "Import-Export",
}

import bpy
import bmesh
import os

vcolor_add = lambda u, v: [u[0]+v[0], u[1]+v[1], u[2]+v[2], u[3]+v[3]]
vcolor_div = lambda u, s: [u[0]/s, u[1]/s, u[2]/s, u[3]/s]

def average_vertexcolors(mesh):
		vertexcolors = {}
		for i in range(len(mesh.faces)):	# get all vcolors that share this vertex
				for j in range(len(mesh.faces[i].v)):
						index = mesh.faces[i].v[j].index
						if j<len(mesh.faces[i].col):
								color = mesh.faces[i].col[j]
								r,g,b,a = color.r, color.g, color.b, color.a
						else:
								r,g,b,a = 255,255,255,255
						vertexcolors.setdefault(index, []).append([r,g,b,a])
		for i in range(len(vertexcolors)):	# average them
				vcolor = [0,0,0,0]	# rgba
				if not i in vertexcolors:
						return {}
				for j in range(len(vertexcolors[i])):
						vcolor = vcolor_add(vcolor, vertexcolors[i][j])
				shared = len(vertexcolors[i])
				vertexcolors[i] = vcolor_div(vcolor, shared)
		return vertexcolors
  
def has_vertex_colors(mesh):
	# Überprüfen, ob color_attributes vorhanden sind und vom Typ 'BYTE_COLOR' oder 'FLOAT_COLOR' sind
	for attr in mesh.attributes:
			if attr.domain == 'POINT' and attr.data_type in {'BYTE_COLOR', 'FLOAT_COLOR'}:
					return True
	return False

class FlatExporter:

		def __init__(self):
				self.stack = []
				self.scenegraph = {}
		
		def open(self,filename):
				self.file = open(filename, "w")
				self.write("# flat exported \n")
		
		def close(self):
				self.file.close()

		def write_color(self,color,name="c"):
				self.begin(name)
				self.write('%d %d %d %d' % tuple(color))
				self.end()
		
		def write(self, *args):
				self.file.write(*args)

		def begin(self, name, attrs=None):
				self.write(f"{name}\n")
				if attrs:
						for key,value in attrs:
								self.write(f"{key} {value}\n")
				self.stack.append(name)

		def end(self):
				name = self.stack.pop()
				self.write(f"end{name}\n")

		def write_matrix(self,matrix,name="default"):
				self.write(f"{name} ")
				self.file.write("%f %f %f %f %f %f %f %f %f %f %f %f\n"
				% (	matrix[0][0],matrix[1][0],matrix[2][0],matrix[3][0],
						matrix[0][1],matrix[1][1],matrix[2][1],matrix[3][1],
						matrix[0][2],matrix[1][2],matrix[2][2],matrix[3][2]))

		def write_vector(self,v):
				self.file.write("%f %f %f " % (v[0],v[1],v[2]))

		def write_modifier(self,modifier):
			if modifier.type=='ARRAY':
				self.write(f"type {modifier.type}\n")
				self.write(f"count {modifier.count}\n")
				if modifier.use_relative_offset:
					self.write(f"relative_offset {modifier.relative_offset_displace[0]} {modifier.relative_offset_displace[1]} {modifier.relative_offset_displace[2]}\n")
				if modifier.use_constant_offset:
					self.write(f"constant_offset {modifier.constant_offset_displace[0]} {modifier.constant_offset_displace[1]} {modifier.constant_offset_displace[2]}\n")
				if modifier.use_object_offset:
					self.write(f"object_offset {modifier.offset_object.name}\n")

		def write_object(self,object):
				#ipdb.set_trace()
				data = object.data
				name = ""
				if data:
						name = data.name
				
				self.begin("node",[
						("name",object.name),
						("asset",name)
				])
				
				self.begin("modifiers")
				for modifier in object.modifiers:
					self.write_modifier(modifier)
				self.end()
    
				# custom python properties
				if "uncullable" in object:
					self.write(f"uncullable {object['uncullable']}\n")
     
				if object.hide_viewport:
					self.write(f"hidden\n")
					
    
				action=False
				if object.animation_data and object.animation_data.action:
					self.write(f"action {object.animation_data.action.name}\n")
					action=True
				
				if action or not (object.scale.x==1 and object.scale.y==1 and object.scale.x==1):
					self.write(f"scale {object.scale.x} {object.scale.y} {object.scale.z}\n")
				if object.rotation_mode=='XYZ':
					if action or not (object.rotation_euler.x==0 and object.rotation_euler.y==0 and object.rotation_euler.z==0):
						self.write(f"rotate XYZ {object.rotation_euler.x} {object.rotation_euler.y} {object.rotation_euler.z}\n")
				if action or not (object.location.x==0 and object.location.y==0 and object.location.z==0):
					self.write(f"location {object.location.x} {object.location.y} {object.location.z}\n")
				
				self.begin("children")
				if object.name in self.scenegraph:
						for child in self.scenegraph[object.name]:
								self.write_object(child)
				self.end()
				self.end()

		def write_bone(self,bone):
				self.begin("bone",[("name",bone.name)])
				# TODO: die hierarchie korrigieren
				self.write_matrix(bone.matrix['ARMATURESPACE'],'local')
				if bone.children:
						for child in bone.children:
								self.write_bone(child)
				self.end()

		def write_armature(self,armature):
				self.begin("armature",[("name",armature.name)])
				for name,bone in armature.bones.items():
						self.write_bone(bone)
				self.end()

		
		#
		# Ubervertex approach: merge everything into single vertices
		#
		#
 
		def write_mesh(self,mesh):
				if mesh.users>0:
						self.begin("mesh",[("name",mesh.name)])

						if mesh.animation_data and mesh.animation_data.action:
								self.write(f"action {mesh.animation_data.action.name}\n")

						if len(mesh.uv_layers) > 0:
								uv_layer = mesh.uv_layers.active.data
						else:
								uv_layer = False
						vertices = []
						loops = []
						use_smooth = False
      
						color_attr = False
						for attr in mesh.attributes:
							if attr.domain == 'POINT' and attr.data_type in {'BYTE_COLOR', 'FLOAT_COLOR'}:
								color_attr = attr
								break
      
						if color_attr:
							self.write(f"vertexcolors True\n")
						for material in mesh.materials:
							self.write(f"material {material.name}\n")
							loops.append([])
						for polygon in mesh.polygons:
							use_smooth = polygon.use_smooth
							material_index = polygon.material_index
							loop = []
							for index in range(0,len(polygon.loop_indices)):
									vertex_index = polygon.vertices[index]


									v = []
									for co in mesh.vertices[vertex_index].co:
											v += [co]
									for no in mesh.vertices[vertex_index].normal:
											v += [no]
           
									if color_attr:
										v += color_attr.data[vertex_index].color
									else:
										v += [1,1,1,99]

									# todo: multiple channels
									# todo: vertex color
									# todo: vertex weights for skinning
									if uv_layer:
											for uv in uv_layer[polygon.loop_indices[index]].uv:
													v += [uv]
									else:
											v += [0,0]

									if v not in vertices:
											vertices.append(v)
									
									loop.append( vertices.index(v) )
							loops[material_index].append(loop)

						self.write(f"smooth {use_smooth}\n")

						self.write("vertices ")
						for vertex in vertices:
								for v in vertex:
										self.write(f"{v} ")
						self.write("\n")

						batches = []
						self.write("indices ")
						for batch in loops:
							count = 0
							for loop in batch:
								if len(loop)==4:
										self.write(f"{loop[0]} {loop[1]} {loop[2]} ")
										self.write(f"{loop[2]} {loop[3]} {loop[0]} ")
										count += 6
								elif len(loop)==3:
										self.write(f"{loop[0]} {loop[1]} {loop[2]} ")
										count += 3
							batches.append(count)
						self.write("\n")

						# for each material
						self.write("batches ")
						for batch in batches:
							self.write(f"{batch} ")
						self.write("\n")
					
						self.end()

		def write_camera(self,camera):
				if camera.users>0:
						self.begin("camera",[("name",camera.name)])
						self.write(f"near {camera.clip_start}\n")
						self.write(f"far {camera.clip_end}\n")
						self.write(f"aspect 1.777\n")
						if camera.sensor_fit=="VERTICAL":
								self.write(f"yfov {camera.sensor_height}\n")
						if camera.sensor_fit=="HORIZONTAL":
								self.write(f"xfov {camera.sensor_width}\n")
						if camera.animation_data and camera.animation_data.action:
								self.write(f"action {camera.animation_data.action.name}\n")
						#self.begin("vertices")
						#for v in mesh.vertices: self.write_vector(v.co,"v")
						self.end()

		def write_material(self,material):
				if material.users>0:
						self.begin("material",[("name",material.name)])

						self.write(f"use_backface_culling {material.use_backface_culling}\n")
						self.write(f"blend_method {material.blend_method}\n")

						if material.node_tree and material.node_tree.animation_data and material.node_tree.animation_data.action:
							self.write(f"action {material.node_tree.animation_data.action.name}\n")
						if material.node_tree:
								for node in material.node_tree.nodes:
										if node.type=='TEX_IMAGE':
												filepath = bpy.path.abspath(node.image.filepath)
												self.write(f"image {filepath}\n")

								if "Principled BSDF" in material.node_tree.nodes:
									inputs = material.node_tree.nodes["Principled BSDF"].inputs
									alpha = inputs["Alpha"].default_value
									diffuse = inputs["Base Color"].default_value
									self.write(f"diffuse {diffuse[0]} {diffuse[1]} {diffuse[2]} {diffuse[3]}\n")
									self.write(f"alpha {alpha}\n")
										
						#self.write(f"filepath {image.filepath[2:]}\n")
						self.end()

		def write_fcurve(self,fcurve):
				self.begin("fcurve")
    
				# boo, horrible
				datapath = fcurve.data_path
				if datapath=="nodes[\"Principled BSDF\"].inputs[0].default_value":
					datapath = "diffuse"
				if datapath=="nodes[\"Principled BSDF\"].inputs[4].default_value":
					datapath = "alpha"
    
				self.write(f"datapath {datapath}.{'XYZW'[fcurve.array_index]}\n")
				for k in fcurve.keyframe_points:
						self.write(f"keyframe {k.interpolation} {k.co.x} {k.co.y} {k.handle_left.x} {k.handle_left.y} {k.handle_right.x} {k.handle_right.y}\n")
				self.end()

		def write_action(self,action):
				if action.users<1: return
				self.begin("action",[("name",action.name)])
				self.write(f"start {action.frame_start}\n")
				self.write(f"end {action.frame_end}\n")
				self.write(f"cyclic {action.use_cyclic}\n")
				self.begin("fcurves")
				for fcurve in action.fcurves:
						self.write_fcurve(fcurve)
				self.end()
				self.end()

		def write_spline(self,spline):
			self.begin("spline",[("type",spline.type)])
			for p in spline.bezier_points:
				self.write(f"bezier {p.co.x} {p.co.y} {p.co.z} {p.handle_left.x} {p.handle_left.y} {p.handle_left.z} {p.handle_right.x} {p.handle_right.y} {p.handle_right.z}\n")
			self.end()

		def write_curve(self,curve):
			self.begin("curve",[("name",curve.name)])
			self.begin("splines")
			for spline in curve.splines:
					self.write_spline(spline)
			self.end()
			self.end()


#for action in bpy.data.actions:

def write_flat(context, filepath, use_flat_setting):
		
		mye = FlatExporter()
		mye.open(filepath)
		rootnodes = []
		
		for object in bpy.context.scene.objects:
				if not object.name in mye.scenegraph:
						mye.scenegraph[object.name] = []
				if object.parent:
						if not object.parent.name in mye.scenegraph:
								mye.scenegraph[object.parent.name] = []
						mye.scenegraph[object.parent.name].append( object )
				else:
						rootnodes.append(object)

		mye.begin("actions")
		for action in bpy.data.actions:
				mye.write_action(action)
		mye.end()

		mye.begin("materials")
		for material in bpy.data.materials:
				mye.write_material(material)
		mye.end()

		mye.begin("assets")
		for curve in bpy.data.curves:
				mye.write_curve(curve)
		for mesh in bpy.data.meshes:
				mye.write_mesh(mesh)
		for camera in bpy.data.cameras:
				mye.write_camera(camera)
		mye.end()

		mye.write(f"fps {bpy.context.scene.render.fps}\n")

		mye.begin("scene",[("name",bpy.context.scene.name)])
		for object in rootnodes:
				mye.write_object(object)
		mye.end()
		mye.close()
		
		return {'FINISHED'}


# ExportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator


class ExportFlatData(Operator, ExportHelper):
		"""Export scene as flat file"""
		bl_idname = "export_flat.flat"  # important since its how bpy.ops.import_test.some_data is constructed
		bl_label = "Export Flat"

		# ExportHelper mixin class uses this
		filename_ext = ".flat"

		filter_glob: StringProperty(
				default="*.flat",
				options={'HIDDEN'},
				maxlen=255,  # Max internal buffer length, longer would be clamped.
		)

		# List of operator properties, the attributes will be assigned
		# to the class instance from the operator settings before calling.
		use_setting: BoolProperty(
				name="Example Boolean",
				description="Example Tooltip",
				default=True,
		)

		type: EnumProperty(
				name="Example Enum",
				description="Choose between two items",
				items=(
						('OPT_A', "First Option", "Description one"),
						('OPT_B', "Second Option", "Description two"),
				),
				default='OPT_A',
		)

		def execute(self, context):
				return write_flat(context, self.filepath, self.use_setting)


# Only needed if you want to add into a dynamic menu
def menu_func_export(self, context):
		self.layout.operator(ExportFlatData.bl_idname, text="Flat Export Operator")


# Register and add to the "file selector" menu (required to use F3 search "Text Export Operator" for quick access).
def register():
		bpy.utils.register_class(ExportFlatData)
		bpy.types.TOPBAR_MT_file_export.append(menu_func_export)


def unregister():
		bpy.utils.unregister_class(ExportFlatData)
		bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)


if __name__ == "__main__":
		register()

		# test call
#		bpy.ops.export_flat.flat('INVOKE_DEFAULT')
