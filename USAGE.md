# Flat Exporter for Blender

## Introduction

Flat Exporter is a Python script designed for exporting 3D models from Blender to be used in custom 3D engines. It supports tri/quad meshes with either no texture or exactly one texture map. Each mesh must have only one material, with supported base color and alpha. The tool also supports backface culling, with blend modes restricted to opaque or alpha blend. Camera settings including near/far clipping, FOV, and aspect ratio are exported, as well as the scene graph. Note that having no material is considered an error.

## Installation

1. **Download the Script:**
   Download `flatexporter.py` from the provided source.

2. **Open Blender:**
   Launch Blender and open the project you wish to export.

3. **Load the Script:**
   - Go to the `Scripting` tab in Blender.
   - Use the `Open` option to load `flatexporter.py`.
   - Once loaded, you can view the script in the text editor within Blender.

## Usage

1. **Execute the Script:**
   - With `flatexporter.py` open in the Blender text editor, press `Run Script` to execute it.
   - The script integrates itself into the Blender environment, ready for exporting your models.

2. **Exporting Models:**
   - Select the model you wish to export.
   - Follow the prompts or use the export option provided by the script.

## Features

- **Supported Meshes:** Tri/Quad meshes with one texture map.
- **Material Support:** Each mesh must have one material, supporting base color and alpha.
- **Backface Culling:** Option to enable or disable backface culling.
- **Blend Modes:** Supports opaque and alpha blend modes.
- **Camera Export:** Exports camera settings like near/far clipping, FOV, and aspect ratio.
- **Scene Graph:** Exports the scene graph of the project.

## Troubleshooting

- **No Material Error:** Ensure every mesh has at least one material assigned.
- **Texture Limitations:** Only one texture map per mesh is supported.

## Conclusion

Flat Exporter is a specialized tool for efficiently preparing Blender models for use in specific 3D engines. By adhering to the guidelines and limitations outlined, users can ensure a smooth export process.
