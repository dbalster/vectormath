/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef VECTORMATH_H
#define VECTORMATH_H

#include <algorithm>
#include <cmath>
#include <exception>
#include <iomanip>
#include <limits>
#include <numeric>

// introducing the following types and classes

class V2;	   // vector [x y]
class V3;	   // vector [x y z]
class V4;	   // vector [x y z w]
class M3;	   // 3x3 matrix
class M4;	   // 4x4 matrix
class M43;	   // 4x4 matrix but fourth row is [0 0 0 1] and storage omitted
class Q;	   // quaternion [x y z w]
class Plane;   // plane equation ax + by + cz + d
class Frustum; // convenience type: extracts 6 planes from ViewProjection and offers "inside" method
class AABB3;   // axis aligned bounding box
class Capsule;
class Sphere;

// with three rotation axes you have 8 variants in which you can apply them
// ORDER_XYZ means: create a rotation matrix that merges Rot(X) * Rot(Y) *
// Rot(Z)
enum rotation_order_type
{
	XYZ,
	XZY,
	YXZ,
	YZX,
	ZXY,
	ZYX
};

// see Plane::extract
enum plane_order_type
{
	LEFT,
	RIGHT,
	BOTTOM,
	TOP,
	NEAR,
	FAR
};

constexpr inline auto rad2deg(float _rad) -> float
{
	return _rad * 57.295779513082320876798154814105F;
}

constexpr inline auto deg2rad(float _deg) -> float
{
	return _deg * 0.017453292519943295769236907684886F;
}

template <class T> constexpr inline auto lerp(T _value1, T _value2, float _t) -> T
{
	return _value1 + (_value2 - _value1) * _t;
}

template <class T> constexpr inline auto distance(const T &_lhs, const T &_rhs) -> float
{
	return (_rhs - _lhs).length();
}

auto bezier_decasteljau(float _t, float _t0, float _c0, float _c1, float _t1, int _iterations) -> float;

class NonInvertibleException : public std::exception
{
};

class P2
{
public:
	int x, y;
};

/*
  ⎧ x ⎫
  ⎩ y ⎭
*/
class V2
{
public:
	float x, y;

	constexpr V2()
	  : x(0)
	  , y(0)
	{
	}

	constexpr V2(float _x, float _y)
	  : x(_x)
	  , y(_y)
	{
	}

	inline auto orient2d(const V2 &b, const V2 &c) const noexcept -> float
	{
		return (b.x - x) * (c.y - y) - (b.y - y) * (c.x - x);
	}

	constexpr inline auto operator-() const noexcept -> V2
	{
		return {-x, -y};
	}

	constexpr inline auto operator+(const V2 &_rhs) const noexcept -> V2
	{
		return {x + _rhs.x, y + _rhs.y};
	}

	constexpr inline auto operator+=(const V2 &_rhs) noexcept -> V2 &
	{
		x += _rhs.x;
		y += _rhs.y;
		return *this;
	}

	constexpr inline auto operator-(const V2 &_rhs) const noexcept -> V2
	{
		return {x - _rhs.x, y - _rhs.y};
	}

	constexpr inline auto operator-=(const V2 &_rhs) noexcept -> V2 &
	{
		x -= _rhs.x;
		y -= _rhs.y;
		return *this;
	}

	constexpr inline auto operator*(float _rhs) const noexcept -> V2
	{
		return {x * _rhs, y * _rhs};
	}

	constexpr inline auto operator*=(float _rhs) noexcept -> V2 &
	{
		x *= _rhs;
		y *= _rhs;
		return *this;
	}

	constexpr inline auto dot(const V2 &_rhs) const noexcept -> float
	{
		return x * _rhs.x + y * _rhs.y;
	}

	constexpr inline auto operator*(const V2 &_rhs) const noexcept -> float
	{
		return dot(_rhs);
	}

	constexpr inline auto operator|(const V2 &_rhs) const noexcept -> float
	{
		return dot(_rhs);
	}

	constexpr inline auto norm() const noexcept -> float
	{
		return x * x + y * y;
	}

	constexpr inline auto length() const -> float
	{
		return std::sqrt(norm());
	}

	constexpr auto normalize() -> V2 &
	{
		float len = length();
		if (len == 0.0f)
			return *this;
		float const invlen = 1.0F / len;

		x *= invlen;
		y *= invlen;
		return *this;
	}

	constexpr auto normalized() const -> V2
	{
		float len = length();
		if (len == 0.0f)
			return *this;
		float const invlen = 1.0F / len;
		return {x * invlen, y * invlen};
	}

	constexpr static auto distance(const V2 &_lhs, const V2 &_rhs) -> float
	{
		return (_rhs - _lhs).length();
	}

	friend auto operator<<(std::ostream &_out, const V2 &_v) -> std::ostream &;
	friend auto operator>>(std::istream &_in, V2 &_v) -> std::istream &;
};

/*
  ⎧ x ⎫
  ⎪ y ⎪
  ⎩ z ⎭
*/
class V3
{
public:
	float x, y, z;

	constexpr V3()
	  : x(0)
	  , y(0)
	  , z(0)
	{
	}

	constexpr V3(float _x, float _y, float _z)
	  : x(_x)
	  , y(_y)
	  , z(_z)
	{
	}

	constexpr V3(const V4 &_rhs);

	auto operator==(const V3 &_rhs) const -> bool;

	constexpr inline auto operator-() const noexcept -> V3
	{
		return {-x, -y, -z};
	}

	constexpr inline auto operator+(const V3 &_rhs) const noexcept -> V3
	{
		return {x + _rhs.x, y + _rhs.y, z + _rhs.z};
	}

	constexpr inline auto operator+=(const V3 &_rhs) noexcept -> V3 &
	{
		x += _rhs.x;
		y += _rhs.y;
		z += _rhs.z;
		return *this;
	}

	constexpr inline auto operator-(const V3 &_rhs) const noexcept -> V3
	{
		return {x - _rhs.x, y - _rhs.y, z - _rhs.z};
	}

	constexpr inline auto operator-=(const V3 &_rhs) noexcept -> V3 &
	{
		x -= _rhs.x;
		y -= _rhs.y;
		z -= _rhs.z;
		return *this;
	}

	constexpr inline auto operator*(float _rhs) const noexcept -> V3
	{
		return {x * _rhs, y * _rhs, z * _rhs};
	}

	constexpr inline auto operator*=(float _rhs) noexcept -> V3 &
	{
		x *= _rhs;
		y *= _rhs;
		z *= _rhs;
		return *this;
	}

	constexpr inline auto dot(const V3 &_rhs) const noexcept -> float
	{
		return x * _rhs.x + y * _rhs.y + z * _rhs.z;
	}

	constexpr inline auto dot(float _x, float _y, float _z) const noexcept -> float
	{
		return x * _x + y * _y + z * _z;
	}

	constexpr inline auto operator*(const V3 &_rhs) const noexcept -> float
	{
		return dot(_rhs);
	}

	constexpr inline auto operator|(const V3 &_rhs) const noexcept -> float
	{
		return dot(_rhs);
	}

	constexpr inline auto cross(const V3 &_rhs) const noexcept -> V3
	{
		return {y * _rhs.z - z * _rhs.y, z * _rhs.x - x * _rhs.z, x * _rhs.y - y * _rhs.x};
	}

	constexpr inline auto operator^(const V3 &_rhs) const noexcept -> V3
	{
		return cross(_rhs);
	}

	constexpr inline auto norm() const noexcept -> float
	{
		return x * x + y * y + z * z;
	}

	constexpr inline auto length() const -> float
	{
		return std::sqrt(norm());
	}

	auto orient2d(const V3 &a, const V3 &b) const -> float
	{
		return (x - a.x) * (b.y - a.y) - (y - a.y) * (b.x - a.x);
	}

	constexpr auto normalize() -> V3 &
	{
		float len = length();
		if (len == 0.0f)
			return *this;
		float const invlen = 1.0F / len;
		x *= invlen;
		y *= invlen;
		z *= invlen;
		return *this;
	}

	constexpr auto normalized() const -> V3
	{
		float len = length();
		if (len == 0.0f)
			return *this;
		float const invlen = 1.0F / len;
		return {x * invlen, y * invlen, z * invlen};
	}

	constexpr static auto distance(const V3 &_lhs, const V3 &_rhs) -> float
	{
		return (_rhs - _lhs).length();
	}

	friend auto operator<<(std::ostream &_out, const V3 &_v) -> std::ostream &;
	friend auto operator>>(std::istream &_in, V3 &_v) -> std::istream &;
};

/*
  ⎧ x ⎫
  ⎪ y ⎪
  ⎪ z ⎪
  ⎩ w ⎭
*/

class V4
{
public:
	float x, y, z, w;

	constexpr V4()
	  : x(0)
	  , y(0)
	  , z(0)
	  , w(0)
	{
	}

	// extend
	constexpr V4(const V3 &_v3, float _w)
	  : x(_v3.x)
	  , y(_v3.y)
	  , z(_v3.z)
	  , w(_w)
	{
	}

	constexpr V4(float _x, float _y, float _z, float _w)
	  : x(_x)
	  , y(_y)
	  , z(_z)
	  , w(_w)
	{
	}

	constexpr inline auto operator-() const noexcept -> V4
	{
		return {-x, -y, -z, -w};
	}

	constexpr auto operator+(const V4 &_rhs) const noexcept -> V4
	{
		return {x + _rhs.x, y + _rhs.y, z + _rhs.z, w + _rhs.w};
	}

	constexpr auto operator+=(const V4 &_rhs) noexcept -> V4 &
	{
		x += _rhs.x;
		y += _rhs.y;
		z += _rhs.z;
		w += _rhs.w;
		return *this;
	}

	constexpr auto operator-(const V4 &_rhs) const noexcept -> V4
	{
		return {x - _rhs.x, y - _rhs.y, z - _rhs.z, w - _rhs.w};
	}

	constexpr auto operator-=(const V4 &_rhs) noexcept -> V4 &
	{
		x -= _rhs.x;
		y -= _rhs.y;
		z -= _rhs.z;
		w -= _rhs.w;
		return *this;
	}

	constexpr auto operator*(float _rhs) const noexcept -> V4
	{
		return {x * _rhs, y * _rhs, z * _rhs, w * _rhs};
	}

	constexpr auto operator*=(float _rhs) noexcept -> V4 &
	{
		x *= _rhs;
		y *= _rhs;
		z *= _rhs;
		w *= _rhs;
		return *this;
	}

	constexpr inline auto homogeneous() const -> V3
	{
		return {x / w, y / w, z / w};
	}

	constexpr auto dot(const V4 &_rhs) const noexcept -> float
	{
		return x * _rhs.x + y * _rhs.y + z * _rhs.z + w * _rhs.w;
	}

	constexpr auto operator*(const V4 &_rhs) const noexcept -> float
	{
		return dot(_rhs);
	}

	constexpr auto operator|(const V4 &_rhs) const noexcept -> float
	{
		return dot(_rhs);
	}

	constexpr auto cross(const V4 &_rhs) const noexcept -> V4
	{
		return {y * _rhs.z - z * _rhs.y, z * _rhs.x - x * _rhs.z, x * _rhs.y - y * _rhs.x, 1.0F};
	}

	constexpr auto operator^(const V4 &_rhs) const noexcept -> V4
	{
		return cross(_rhs);
	}

	constexpr auto norm() const noexcept -> float
	{
		return x * x + y * y + z * z + w * w;
	}

	constexpr auto length() const -> float
	{
		return std::sqrt(norm());
	}

	constexpr auto normalize() -> V4 &
	{
		float len = length();
		if (len == 0.0f)
			return *this;
		float const invlen = 1.0F / len;
		x *= invlen;
		y *= invlen;
		z *= invlen;
		w *= invlen;
		return *this;
	}

	constexpr auto normalized() const -> V4
	{
		float len = length();
		if (len == 0.0f)
			return *this;
		float const invlen = 1.0F / len;
		return {x * invlen, y * invlen, z * invlen, w * invlen};
	}

	constexpr static auto distance(const V4 &_lhs, const V4 &_rhs) -> float
	{
		return (_rhs - _lhs).length();
	}

	friend auto operator<<(std::ostream &_out, const V4 &_v) -> std::ostream &;
	friend auto operator>>(std::istream &_in, V4 &_v) -> std::istream &;
};

constexpr V3::V3(const V4 &_rhs)
  : x(_rhs.x / _rhs.w)
  , y(_rhs.y / _rhs.w)
  , z(_rhs.z / _rhs.w)
{
}

/*
  column-major 4x4 matrix

  ⎧ a₀₀ a₀₁ a₀₂ a₀₃ ⎫
  ⎪ a₁₀ a₁₁ a₁₂ a₁₃ ⎪
  ⎪ a₂₀ a₂₁ a₂₂ a₂₃ ⎪
  ⎩ a₃₀ a₃₁ a₃₂ a₃₃ ⎭
*/
class M4
{
public:
	float m[4][4];

	constexpr M4()
	  : M4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)
	{
	}

	// upgrade M43 to full M4
	explicit M4(const M43 &_m43);
	// upgrade M3 to full M4
	explicit M4(const M3 &_m3);

	/*
	  named constructor: create a translation matrix from vector

	  ⎧ 1  0  0  0 ⎫
	  ⎪ 0  1  0  0 ⎪
	  ⎪ 0  0  1  0 ⎪
	  ⎩ x  y  z  1 ⎭ <- translation is here
	*/
	constexpr static auto translate(const V3 &_v) -> M4
	{
		return {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, _v.x, _v.y, _v.z, 1};
	}

	/*
	  named constructor: create a translation matrix from scalars

	  ⎧ 1  0  0  0 ⎫
	  ⎪ 0  1  0  0 ⎪
	  ⎪ 0  0  1  0 ⎪
	  ⎩ x  y  z  1 ⎭ <- translation is here
	*/
	constexpr static auto translate(float _x, float _y, float _z) -> M4
	{
		return {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, _x, _y, _z, 1};
	}

	/*
	  named constructor: create a scale matrix from vector

	  ⎧ x  0  0  0 ⎫
	  ⎪ 0  y  0  0 ⎪
	  ⎪ 0  0  z  0 ⎪
	  ⎩ 0  0  0  1 ⎭
	*/
	constexpr static auto scale(const V3 &_v) -> M4
	{
		return {_v.x, 0, 0, 0, 0, _v.y, 0, 0, 0, 0, _v.z, 0, 0, 0, 0, 1};
	}

	/*
	  named constructor: create a scale matrix from scalars

	  ⎧ x  0  0  0 ⎫
	  ⎪ 0  y  0  0 ⎪
	  ⎪ 0  0  z  0 ⎪
	  ⎩ 0  0  0  1 ⎭
	*/
	constexpr static auto scale(float _x, float _y, float _z) -> M4
	{
		return {_x, 0, 0, 0, 0, _y, 0, 0, 0, 0, _z, 0, 0, 0, 0, 1};
	}

	/*
	  named constructor: create a uniform scale matrix from scalar

	  ⎧ u  0  0  0 ⎫
	  ⎪ 0  u  0  0 ⎪
	  ⎪ 0  0  u  0 ⎪
	  ⎩ 0  0  0  1 ⎭
	*/
	constexpr static auto scale(float _u) -> M4
	{
		return {_u, 0, 0, 0, 0, _u, 0, 0, 0, 0, _u, 0, 0, 0, 0, 1};
	}

	// named constructor: create a rotation matrix from axis and angle
	constexpr static auto rotate(const V3 &_axis, float _angleInDegree) -> M4
	{
		float const angleInRad = deg2rad(std::fmod(_angleInDegree, 360.0F));

		float const s = std::sin(angleInRad);
		float const c = std::cos(angleInRad);
		float const t = 1.0F - c;

		V3 const p = _axis.normalized();

		return {(t * p.x * p.x) + (c),
				(t * p.x * p.y) + (s * p.z),
				(t * p.x * p.z) - (s * p.y),
				0.0F,
				(t * p.x * p.y) - (s * p.z),
				(t * p.y * p.y) + (c),
				(t * p.y * p.z) + (s * p.x),
				0.0F,
				(t * p.x * p.z) + (s * p.y),
				(t * p.y * p.z) - (s * p.x),
				(t * p.z * p.z) + (c),
				0.0F,
				0.0F,
				0.0F,
				0.0F,
				1.0F};
	}

	constexpr static auto rotate(const V4 &_axis_angle_w) -> M4
	{
		return rotate(V3(_axis_angle_w.x, _axis_angle_w.y, _axis_angle_w.z), _axis_angle_w.w);
	}

	// named constructor: create a rotation matrix from euler angles and given
	// order
	/*
	  named constructor: create a uniform scale matrix from scalar

	  XYZ
	  ⎧ 1    0      0    0 ⎫⎧ cos(β) 0 -sin(β) 0 ⎫⎧ cos(ɣ) -sin(ɣ)  0  0 ⎫
	  ⎪ 0  cos(ɑ) sin(ɑ) 0 ⎪⎪   0    1    0    0 ⎪⎪ sin(ɣ)  cos(ɣ)  0  0 ⎪
	  ⎪ 0 -sin(ɑ) cos(ɑ) 0 ⎪⎪ sin(β) 0  cos(β) 0 ⎪⎪   0       0     1  0 ⎪
	  ⎩ 0    0      0    1 ⎭⎩   0    0    0    1 ⎭⎩   0       0     0  1 ⎭
	*/
	static auto euler(rotation_order_type _order, const V3 &_v) -> M4;

	static auto quaternion(const Q &_q) -> M4;

	constexpr M4(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13, float m20, float m21, float m22, float m23, float m30,
				 float m31, float m32, float m33)
	  : m{{m00, m01, m02, m03}, {m10, m11, m12, m13}, {m20, m21, m22, m23}, {m30, m31, m32, m33}}
	{
	}

	constexpr M4(const V4 &_r0, const V4 &_r1, const V4 &_r2, const V4 &_r3)
	  : m{
		  {_r0.x, _r0.y, _r0.z, _r0.w},
		  {_r1.x, _r1.y, _r1.z, _r1.w},
		  {_r2.x, _r2.y, _r2.z, _r2.w},
		  {_r3.x, _r3.y, _r3.z, _r3.w},
		}
	{
	}

	// multiply matrices
	constexpr auto operator*(const M4 &_rhs) const -> M4
	{
		return {
		  m[0][0] * _rhs.m[0][0] + m[1][0] * _rhs.m[0][1] + m[2][0] * _rhs.m[0][2] + m[3][0] * _rhs.m[0][3], //
		  m[0][1] * _rhs.m[0][0] + m[1][1] * _rhs.m[0][1] + m[2][1] * _rhs.m[0][2] + m[3][1] * _rhs.m[0][3], //
		  m[0][2] * _rhs.m[0][0] + m[1][2] * _rhs.m[0][1] + m[2][2] * _rhs.m[0][2] + m[3][2] * _rhs.m[0][3], //
		  m[0][3] * _rhs.m[0][0] + m[1][3] * _rhs.m[0][1] + m[2][3] * _rhs.m[0][2] + m[3][3] * _rhs.m[0][3], //
		  m[0][0] * _rhs.m[1][0] + m[1][0] * _rhs.m[1][1] + m[2][0] * _rhs.m[1][2] + m[3][0] * _rhs.m[1][3], //
		  m[0][1] * _rhs.m[1][0] + m[1][1] * _rhs.m[1][1] + m[2][1] * _rhs.m[1][2] + m[3][1] * _rhs.m[1][3], //
		  m[0][2] * _rhs.m[1][0] + m[1][2] * _rhs.m[1][1] + m[2][2] * _rhs.m[1][2] + m[3][2] * _rhs.m[1][3], //
		  m[0][3] * _rhs.m[1][0] + m[1][3] * _rhs.m[1][1] + m[2][3] * _rhs.m[1][2] + m[3][3] * _rhs.m[1][3], //
		  m[0][0] * _rhs.m[2][0] + m[1][0] * _rhs.m[2][1] + m[2][0] * _rhs.m[2][2] + m[3][0] * _rhs.m[2][3], //
		  m[0][1] * _rhs.m[2][0] + m[1][1] * _rhs.m[2][1] + m[2][1] * _rhs.m[2][2] + m[3][1] * _rhs.m[2][3], //
		  m[0][2] * _rhs.m[2][0] + m[1][2] * _rhs.m[2][1] + m[2][2] * _rhs.m[2][2] + m[3][2] * _rhs.m[2][3], //
		  m[0][3] * _rhs.m[2][0] + m[1][3] * _rhs.m[2][1] + m[2][3] * _rhs.m[2][2] + m[3][3] * _rhs.m[2][3], //
		  m[0][0] * _rhs.m[3][0] + m[1][0] * _rhs.m[3][1] + m[2][0] * _rhs.m[3][2] + m[3][0] * _rhs.m[3][3], //
		  m[0][1] * _rhs.m[3][0] + m[1][1] * _rhs.m[3][1] + m[2][1] * _rhs.m[3][2] + m[3][1] * _rhs.m[3][3], //
		  m[0][2] * _rhs.m[3][0] + m[1][2] * _rhs.m[3][1] + m[2][2] * _rhs.m[3][2] + m[3][2] * _rhs.m[3][3], //
		  m[0][3] * _rhs.m[3][0] + m[1][3] * _rhs.m[3][1] + m[2][3] * _rhs.m[3][2] + m[3][3] * _rhs.m[3][3]	 //
		};
	}

	auto operator*(const M43 &_rhs) const -> M4;

	/* transform a vector v3

	⎧ a₀₀ a₀₁ a₀₂ a₀₃ ⎫   ⎧x⎫   ⎧a₀₀x + a₁₀y + a₂₀z + a₃₀1⎫
	⎪ a₁₀ a₁₁ a₁₂ a₁₃ ⎪ * ⎪y⎪ = ⎪a₀₁x + a₁₁y + a₂₁z + a₃₁1⎪
	⎪ a₂₀ a₂₁ a₂₂ a₂₃ ⎪   ⎩z⎭   ⎩a₀₂x + a₁₂y + a₂₂z + a₃₂1⎭
	⎩ a₃₀ a₃₁ a₃₂ a₃₃ ⎭    1
	*/
	constexpr auto operator*(const V3 &_v) const noexcept -> V3
	{
		return {
		  m[0][0] * _v.x + m[1][0] * _v.y + m[2][0] * _v.z + m[3][0], //
		  m[0][1] * _v.x + m[1][1] * _v.y + m[2][1] * _v.z + m[3][1], //
		  m[0][2] * _v.x + m[1][2] * _v.y + m[2][2] * _v.z + m[3][2]  //
		};
	}

	/* transform a vector v4

	⎧ a₀₀ a₀₁ a₀₂ a₀₃ ⎫   ⎧x⎫   ⎧a₀₀x + a₁₀y + a₂₀z + a₃₀w⎫
	⎪ a₁₀ a₁₁ a₁₂ a₁₃ ⎪ * ⎪y⎪ = ⎪a₀₁x + a₁₁y + a₂₁z + a₃₁w⎪
	⎪ a₂₀ a₂₁ a₂₂ a₂₃ ⎪   ⎪z⎪   ⎪a₀₂x + a₁₂y + a₂₂z + a₃₂w⎪
	⎩ a₃₀ a₃₁ a₃₂ a₃₃ ⎭   ⎩w⎭   ⎩a₀₃x + a₁₃y + a₂₃z + a₃₃w⎭
	*/
	constexpr auto operator*(const V4 &_v) const noexcept -> V4
	{
		return {
		  m[0][0] * _v.x + m[1][0] * _v.y + m[2][0] * _v.z + m[3][0] * _v.w, //
		  m[0][1] * _v.x + m[1][1] * _v.y + m[2][1] * _v.z + m[3][1] * _v.w, //
		  m[0][2] * _v.x + m[1][2] * _v.y + m[2][2] * _v.z + m[3][2] * _v.w, //
		  m[0][3] * _v.x + m[1][3] * _v.y + m[2][3] * _v.z + m[3][3] * _v.w	 //
		};
	}

	// V3 -> V4 !
	/* transform a vector v4

	⎧ a₀₀ a₀₁ a₀₂ a₀₃ ⎫   ⎧x⎫   ⎧a₀₀x + a₁₀y + a₂₀z + a₃₀1⎫
	⎪ a₁₀ a₁₁ a₁₂ a₁₃ ⎪ * ⎪y⎪ = ⎪a₀₁x + a₁₁y + a₂₁z + a₃₁1⎪
	⎪ a₂₀ a₂₁ a₂₂ a₂₃ ⎪   ⎩z⎭   ⎪a₀₂x + a₁₂y + a₂₂z + a₃₂1⎪
	⎩ a₃₀ a₃₁ a₃₂ a₃₃ ⎭    1    ⎩a₀₃x + a₁₃y + a₂₃z + a₃₃1⎭
	*/
	constexpr auto transform(const V3 &_v) const noexcept -> V4
	{
		return {
		  m[0][0] * _v.x + m[1][0] * _v.y + m[2][0] * _v.z + m[3][0], //
		  m[0][1] * _v.x + m[1][1] * _v.y + m[2][1] * _v.z + m[3][1], //
		  m[0][2] * _v.x + m[1][2] * _v.y + m[2][2] * _v.z + m[3][2], //
		  m[0][3] * _v.x + m[1][3] * _v.y + m[2][3] * _v.z + m[3][3]  //
		};
	}

	/*

	  ⎧ xₛ 0  0  0 ⎫  xₛ = cot(yFov/2)
	  ⎪ 0  yₛ 0  0 ⎪  yₛ = cot(yFov/2) * aspect
	  ⎪ 0  0  ɑ  β ⎪  ɑ  =         far  / (far-near)
	  ⎩ 0  0  ɣ  1 ⎭  β  = - (near*far) / (far-near)
					  ɣ  = +1   "left-handed" orientation

	  maps z into [0...1]

	*/
	auto perspectiveFovLH(float _yFov, float _aspect, float _near, float _far) -> M4 &;
	/*

	  ⎧ xₛ 0  0  0 ⎫  xₛ = cot(yFov/2)
	  ⎪ 0  yₛ 0  0 ⎪  yₛ = cot(yFov/2) * aspect
	  ⎪ 0  0  ɑ  β ⎪  ɑ  =       far  / (near-far)
	  ⎩ 0  0  ɣ  1 ⎭  β  = (near*far) / (near-far)
					  ɣ  = -1   "left-handed" orientation

	  maps z into [0...1]

	*/
	auto perspectiveFovRH(float _yFov, float _aspect, float _near, float _far) -> M4 &;
	auto perspectiveLH(float _w, float _h, float _near, float _far) -> M4 &;
	auto perspectiveRH(float _w, float _h, float _near, float _far) -> M4 &;
	// these are the classic opengl ones, z -> [-1...+1]
	auto ortho(float _left, float _right, float _top, float _bottom, float _near, float _far) -> M4 &;
	auto persp(float _y_fov, float _aspect, float zNear, float zFar) -> M4 &;
	auto frustum(float _left, float _right, float _bottom, float _top, float _near, float _far) -> M4 &;

	// invert the matrix
	auto invert() -> M4 &;
	// return an inverted copy
	auto inverted() const -> M4
	{
		return M4(*this).invert();
	}
	constexpr auto transpose() -> M4 &
	{
		std::swap(m[0][1], m[1][0]); // A
		std::swap(m[0][2], m[2][0]); // B
		std::swap(m[0][3], m[3][0]); // C
		std::swap(m[1][2], m[2][1]); // D
		std::swap(m[1][3], m[3][1]); // E
		std::swap(m[2][3], m[3][2]); // F
		return *this;
	}

	constexpr auto transposed() const -> M4
	{
		return M4(*this).transpose();
	}

	friend auto operator<<(std::ostream &_out, const M4 &_m) -> std::ostream &;
};

/*
  column-major 3x3 matrix

  ⎧ a₀₀ a₀₁ a₀₂ ⎫
  ⎪ a₁₀ a₁₁ a₁₂ ⎪
  ⎩ a₂₀ a₂₁ a₂₂ ⎭
*/
class M3
{
public:
	float m[3][3];

	constexpr M3()
	  : M3(1, 0, 0, 0, 1, 0, 0, 0, 1)
	{
	}

	constexpr M3(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22)
	  : m{{m00, m01, m02}, {m10, m11, m12}, {m20, m21, m22}}
	{
	}

	constexpr inline M3(const M43 &_m);

	constexpr auto operator*(const M3 &_rhs) const -> M3
	{
		return {
		  m[0][0] * _rhs.m[0][0] + m[1][0] * _rhs.m[0][1] + m[2][0] * _rhs.m[0][2], //
		  m[0][1] * _rhs.m[0][0] + m[1][1] * _rhs.m[0][1] + m[2][1] * _rhs.m[0][2], //
		  m[0][2] * _rhs.m[0][0] + m[1][2] * _rhs.m[0][1] + m[2][2] * _rhs.m[0][2], //
		  m[0][0] * _rhs.m[1][0] + m[1][0] * _rhs.m[1][1] + m[2][0] * _rhs.m[1][2], //
		  m[0][1] * _rhs.m[1][0] + m[1][1] * _rhs.m[1][1] + m[2][1] * _rhs.m[1][2], //
		  m[0][2] * _rhs.m[1][0] + m[1][2] * _rhs.m[1][1] + m[2][2] * _rhs.m[1][2], //
		  m[0][0] * _rhs.m[2][0] + m[1][0] * _rhs.m[2][1] + m[2][0] * _rhs.m[2][2], //
		  m[0][1] * _rhs.m[2][0] + m[1][1] * _rhs.m[2][1] + m[2][1] * _rhs.m[2][2], //
		  m[0][2] * _rhs.m[2][0] + m[1][2] * _rhs.m[2][1] + m[2][2] * _rhs.m[2][2]	//
		};
	}

	constexpr auto operator*(const V3 &_v) const -> V3
	{
		return transform(_v);
	}

	constexpr auto transform(const V3 &_v) const -> V3
	{
		return {m[0][0] * _v.x + m[1][0] * _v.y + m[2][0] * _v.z, m[0][1] * _v.x + m[1][1] * _v.y + m[2][1] * _v.z,
				m[0][2] * _v.x + m[1][2] * _v.y + m[2][2] * _v.z};
	}

	auto determinant() const -> float;

	auto inverted() const -> M3;

	auto invert() -> M3 &
	{
		*this = inverted();
		return *this;
	}

	constexpr auto transpose() -> M3 &
	{
		std::swap(m[0][1], m[1][0]);
		std::swap(m[0][2], m[2][0]);
		std::swap(m[1][2], m[2][1]);
		return *this;
	}

	constexpr auto transposed() const -> M3
	{
		return M3(*this).transpose();
	}

	friend auto operator<<(std::ostream &_out, const M3 &_m) -> std::ostream &;
};
/*
  ⎧ a₀₀ a₀₁ ⎫	<- 2x2 rotation kernel
  ⎪ a₁₀ a₁₁ ⎪
  ⎩ a₂₀ a₂₁ ⎭	<- translation is here
*/
class M32
{
public:
	float m[3][2];

	constexpr inline M32()
	  : M32(1, 0, 0, 1, 0, 0)
	{
	}

	constexpr inline M32(float m00, float m01, float m10, float m11, float m20, float m21)
	  : m{{m00, m01}, {m10, m11}, {m20, m21}}
	{
	}

	constexpr inline M32(const M3 &_m);

	constexpr static auto translate(const V2 &_v) -> M32
	{
		return {1.0F, 0.0F, 0.0F, 1.0F, _v.x, _v.y};
	}

	constexpr static auto translate(float _x, float _y) -> M32
	{
		return {1.0F, 0.0F, 0.0F, 1.0F, _x, _y};
	}

	constexpr static auto scale(const V2 &_v) -> M32
	{
		return {_v.x, 0.0F, 0.0F, _v.y, 0.0F, 0.0F};
	}

	constexpr static auto scale(float _sx, float _sy) -> M32
	{
		return {_sx, 0.0F, 0.0F, _sy, 0.0F, 0.0F};
	}

	constexpr static auto scale(float _u) -> M32
	{
		return {_u, 0.0F, 0.0F, _u, 0.0F, 0.0F};
	}

	constexpr static auto rotate(float _angleInDegree) -> M32
	{
		float c = std::cos(deg2rad(_angleInDegree));
		float s = std::sin(deg2rad(_angleInDegree));
		return {c, -s, s, c, 0.0F, 0.0F};
	}

	/*
	  ⎧ a₀₀ a₀₁ 0 ⎫⎧ b₀₀ b₀₁ 0 ⎫⎧ a₀₀b₀₀+a₀₁b₁₀     a₀₀b₀₁+a₀₁b₁₁     0 ⎫
	  ⎪ a₁₀ a₁₁ 0 ⎪⎪ b₁₀ b₁₁ 0 ⎪⎪ a₁₀b₀₀+a₁₁b₁₀     a₁₀b₀₁+a₁₁b₁₁     0 ⎪
	  ⎩ a₂₀ a₂₁ 1 ⎭⎩ b₂₀ b₂₁ 1 ⎭⎩ a₂₀b₀₀+a₂₁b₁₀+b₂₀ a₂₀b₀₁+a₂₁b₁₁+b₂₁ 1 ⎭
	*/

	constexpr auto operator*(const M32 &_rhs) const -> M32
	{
		return {
		  m[0][0] * _rhs.m[0][0] + m[1][0] * _rhs.m[0][1],			 //
		  m[0][1] * _rhs.m[0][0] + m[1][1] * _rhs.m[0][1],			 //
		  m[0][0] * _rhs.m[1][0] + m[1][0] * _rhs.m[1][1],			 //
		  m[0][1] * _rhs.m[1][0] + m[1][1] * _rhs.m[1][1],			 //
		  m[0][0] * _rhs.m[2][0] + m[1][0] * _rhs.m[2][1] + m[2][0], //
		  m[0][1] * _rhs.m[2][0] + m[1][1] * _rhs.m[2][1] + m[2][1]	 //
		};
	}

	constexpr auto operator*(const V2 &_v) const -> V2
	{
		return transform(_v);
	}

	constexpr auto transform(const V2 &_v) const -> V2
	{
		return {m[0][0] * _v.x + m[1][0] * _v.y + m[2][0], m[0][1] * _v.x + m[1][1] * _v.y + m[2][1]};
	}

	constexpr inline auto determinant() const noexcept -> float
	{
		return m[0][0] * m[1][1] - m[0][1] * m[1][0];
	}

	constexpr auto inverted() const -> M32
	{
		float d = determinant();

		if (d < 0.0f || d > 0.0f)
		{
			d = 1.0f / d;
			return {
			  d * m[1][1], d * -m[0][1], d * -m[1][0], d * m[0][0], d * (m[1][0] * m[2][1] - m[1][1] * m[2][0]), d * (m[0][1] * m[2][0] - m[0][0] * m[2][1])};
		}
		return *this;
	}

	constexpr auto invert() -> M32 &
	{
		*this = inverted();
		return *this;
	}

	constexpr auto transpose() -> M32 &
	{
		std::swap(m[0][1], m[1][0]);
		return *this;
	}

	constexpr auto transposed() const -> M32
	{
		return M32(*this).transpose();
	}

	friend auto operator<<(std::ostream &_out, const M32 &_m) -> std::ostream &;
};

/*
  ⎧ x ⎫
  ⎪ y ⎪
  ⎪ z ⎪
  ⎩ w ⎭
*/

class Q
{
public:
	float x, y, z, w;

	constexpr Q()
	  : x(0)
	  , y(0)
	  , z(0)
	  , w(1)
	{
	}

	constexpr Q(const V3 &_v)
	  : x(_v.x)
	  , y(_v.y)
	  , z(_v.z)
	  , w(1)
	{
	}

	constexpr Q(float _x, float _y, float _z, float _w)
	  : x(_x)
	  , y(_y)
	  , z(_z)
	  , w(_w)
	{
	}

	static auto from_axis_angle(const V3 &_axis, float _angle) -> Q;
	static auto from_euler(const V3 &_eulers) -> Q;
	static auto from_m4(const M4 &_m) -> Q;

	constexpr inline auto norm() const -> float
	{
		return x * x + y * y + z * z + w * w;
	}

	constexpr auto length() const -> float
	{
		return std::sqrt(norm());
	}

	auto normalize() -> Q &;
	auto normalized() const -> Q;
	auto inverse() -> Q &;
	auto inversed() const -> Q;

	constexpr inline auto operator-() const noexcept -> Q
	{
		return {-x, -y, -z, -w};
	}

	constexpr inline auto operator+(const Q &_rhs) const noexcept -> Q
	{
		return {x + _rhs.x, y + _rhs.y, z + _rhs.z, w + _rhs.w};
	}

	constexpr inline auto operator+=(const Q &_rhs) noexcept -> Q &
	{
		x += _rhs.x;
		y += _rhs.y;
		z += _rhs.z;
		w += _rhs.w;
		return *this;
	}

	constexpr inline auto operator-(const Q &_rhs) const noexcept -> Q
	{
		return {x - _rhs.x, y - _rhs.y, z - _rhs.z, w - _rhs.w};
	}

	constexpr inline auto operator-=(const Q &_rhs) noexcept -> Q &
	{
		x -= _rhs.x;
		y -= _rhs.y;
		z -= _rhs.z;
		w -= _rhs.w;
		return *this;
	}

	constexpr inline auto operator*(float _rhs) const noexcept -> Q
	{
		return {x * _rhs, y * _rhs, z * _rhs, w * _rhs};
	}

	constexpr inline auto operator*=(float _rhs) noexcept -> Q &
	{
		x *= _rhs;
		y *= _rhs;
		z *= _rhs;
		w *= _rhs;
		return *this;
	}

	constexpr inline auto dot(const Q &_rhs) const -> float
	{
		return x * _rhs.x + y * _rhs.y + z * _rhs.z + w * _rhs.w;
	}

	constexpr inline auto operator*(const Q &_rhs) const -> float
	{
		return dot(_rhs);
	}

	constexpr inline auto operator|(const Q &_rhs) const -> float
	{
		return dot(_rhs);
	}

	inline auto operator*(const Q &_rhs) -> Q;

	static auto slerp(const Q &_a, const Q &_b, float _t) -> Q;

	friend auto operator<<(std::ostream &_out, const Q &_v) -> std::ostream &;
};

/*
  constant column-major 4x4 matrix

  ⎧ a₀₀ a₀₁ a₀₂ ⎫	<- 3x3 rotation kernel
  ⎪ a₁₀ a₁₁ a₁₂ ⎪
  ⎪ a₂₀ a₂₁ a₂₂ ⎪
  ⎩ 0   0   1   ⎭	<- translation is here

  this matrix keeps the fourth column constant, which is sufficient for affine
  transformations
*/
class M43
{
public:
	float m[4][3];

	constexpr M43()
	  : M43(1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0)
	{
	}

	constexpr static auto translate(const V3 &_v) -> M43
	{
		return {1, 0, 0, 0, 1, 0, 0, 0, 1, _v.x, _v.y, _v.z};
	}

	constexpr static auto translate(float _x, float _y, float _z) -> M43
	{
		return {1, 0, 0, 0, 1, 0, 0, 0, 1, _x, _y, _z};
	}

	constexpr static auto scale(const V3 &_v) -> M43
	{
		return {_v.x, 0, 0, 0, _v.y, 0, 0, 0, _v.z, 0, 0, 0};
	}

	constexpr static auto scale(float _x, float _y, float _z) -> M43
	{
		return {_x, 0, 0, 0, _y, 0, 0, 0, _z, 0, 0, 0};
	}

	constexpr static auto scale(float _u) -> M43
	{
		return {_u, 0, 0, 0, _u, 0, 0, 0, _u, 0, 0, 0};
	}

	static auto rotate(const V3 &_axis, float _angleInDegree) -> M43;

	static auto euler(rotation_order_type _order, const V3 &_v) -> M43;

	constexpr M43(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22, float m30, float m31, float m32)
	  : m{{m00, m01, m02}, {m10, m11, m12}, {m20, m21, m22}, {m30, m31, m32}}
	{
	}

	constexpr M43(const V3 &_r0, const V3 &_r1, const V3 &_r2, const V3 &_r3)
	  : m{
		  {_r0.x, _r0.y, _r0.z},
		  {_r1.x, _r1.y, _r1.z},
		  {_r2.x, _r2.y, _r2.z},
		  {_r3.x, _r3.y, _r3.z},
		}
	{
	}

	constexpr M43(const V4 &_r0, const V4 &_r1, const V4 &_r2)
	  : m{
		  {_r0.x, _r1.x, _r2.x},
		  {_r0.y, _r1.y, _r2.y},
		  {_r0.z, _r1.z, _r2.z},
		  {_r0.w, _r1.w, _r2.w},
		}
	{
	}

	auto operator*(const M43 &_rhs) const -> M43;

	auto operator*(const M4 &_rhs) const -> M4;

	auto invert() -> M43 &;

	auto inverted() const -> M43
	{
		return M43(*this).invert();
	}

	static auto quaternion(const Q &_q) -> M43;

	constexpr auto transform(const V3 &_v) const -> V3
	{
		return {
		  m[0][0] * _v.x + m[1][0] * _v.y + m[2][0] * _v.z + m[3][0], //
		  m[0][1] * _v.x + m[1][1] * _v.y + m[2][1] * _v.z + m[3][1], //
		  m[0][2] * _v.x + m[1][2] * _v.y + m[2][2] * _v.z + m[3][2]  //
		};
	}

	constexpr auto operator*(const V3 &_v) const -> V3
	{
		return transform(_v);
	}

	constexpr auto transpose() -> M43 &
	{
		std::swap(m[0][1], m[1][0]); // A
		std::swap(m[0][2], m[2][0]); // B
		std::swap(m[1][2], m[2][1]); // D
		return *this;
	}

	constexpr auto transposed() const -> M43
	{
		return M43(*this).transpose();
	}

	friend auto operator<<(std::ostream &_out, const M43 &_m) -> std::ostream &;
};

/*
	axis-aligned bounding box
*/
class AABB3
{
public:
	float center[3]{0, 0, 0};
	float radius[3]{0, 0, 0};

	AABB3()
	{
	}

	auto intersects(const AABB3 &_rhs) const -> bool
	{
		unsigned int r = (radius[0] + _rhs.radius[0]);
		if ((unsigned int)(center[0] - _rhs.center[0] + r) > r + r)
			return false;
		r = radius[1] + _rhs.radius[1];
		if ((unsigned int)(center[1] - _rhs.center[1] + r) > r + r)
			return false;
		r = radius[2] + _rhs.radius[2];
		if ((unsigned int)(center[2] - _rhs.center[2] + r) > r + r)
			return false;
		return true;
	}

	template <typename range, typename extractor> void build(range &&_range, extractor extract)
	{
		V3 vmin{+std::numeric_limits<float>::max(), +std::numeric_limits<float>::max(), +std::numeric_limits<float>::max()};
		V3 vmax{-std::numeric_limits<float>::max(), -std::numeric_limits<float>::max(), -std::numeric_limits<float>::max()};

		for (auto &iter : _range)
		{
			auto v = extract(iter);
			{
				if (v.x < vmin.x)
					vmin.x = v.x;
				if (v.x > vmax.x)
					vmax.x = v.x;
				if (v.y < vmin.y)
					vmin.y = v.y;
				if (v.y > vmax.y)
					vmax.y = v.y;
				if (v.z < vmin.z)
					vmin.z = v.z;
				if (v.z > vmax.z)
					vmax.z = v.z;
			}
		}
		radius[0] = (vmax.x - vmin.x) / 2.0F;
		radius[1] = (vmax.y - vmin.y) / 2.0F;
		radius[2] = (vmax.z - vmin.z) / 2.0F;
		center[0] = vmin.x + radius[0];
		center[1] = vmin.y + radius[1];
		center[2] = vmin.z + radius[2];
	}

	auto merge(const AABB3 &_rhs) -> AABB3 &
	{
		for (int i = 0; i < 3; ++i)
		{
			float const l = std::min(center[i] - radius[i], _rhs.center[i] - _rhs.radius[i]);
			float const h = std::max(center[i] + radius[i], _rhs.center[i] + _rhs.radius[i]);
			radius[i] = (h - l) / 2.0F;
			center[i] = l + radius[i];
		}
		return *this;
	}

	// transform
	auto operator*(const M43 &_trfm) const -> AABB3
	{
		AABB3 out;
		for (int i = 0; i < 3; ++i)
		{
			out.center[i] = _trfm.m[3][i];
			out.radius[i] = 0.0F;
			for (int j = 0; j < 3; ++j)
			{
				out.center[i] += _trfm.m[j][i] * center[j];
				out.radius[i] += std::abs(_trfm.m[j][i]) * radius[j];
			}
		}
		return out;
	}

	void clear()
	{
		radius[0] = 0.0F;
		radius[1] = 0.0F;
		radius[2] = 0.0F;
		center[0] = 0.0F;
		center[1] = 0.0F;
		center[2] = 0.0F;
	}
};

/*
  plane equation

  ⎧ a ⎫
  ⎪ b ⎪ -> ax + by + cz + d = 0
  ⎪ c ⎪
  ⎩ d ⎭

*/

class Plane
{
public:
	float a, b, c, d;

	Plane()
	  : a(0)
	  , b(0)
	  , c(0)
	  , d(0)
	{
	}

	// from Plane equation
	Plane(float _a, float _b, float _c, float _d)
	  : a(_a)
	  , b(_b)
	  , c(_c)
	  , d(_d)
	{
	}

	// span Plane from two vectors
	Plane(const V3 &_v1, const V3 &_v2);

	// span Plane from three points
	Plane(const V3 &_p1, const V3 &_p2, const V3 &_p3);

	// insert point into Plane equation: 0==on Plane, < or > means above/below
	auto operator*(const V3 &_p) const -> float;

	// distance of point projected to Plane

	inline auto distance(const V3 &_p) const -> float
	{
		return d + V3(a, b, c).dot(_p);
	}
	inline auto distance(float _x, float _y, float _z) const -> float
	{
		return d + V3(a, b, c).dot(_x, _y, _z);
	}

	auto normalize() -> Plane &;
};

/*
  utility to extract 6 planes from clipspace transform

*/

class Frustum
{
public:
	Plane planes[6];
	Frustum() = default;
	Frustum(const M4 &_m);

	// is a point inside the Frustum?
	auto inside(const V3 &_p) const -> bool;
	auto intersects(const AABB3 &_aabb) const -> bool;
};

constexpr inline M3::M3(const M43 &_m)
  : M3(_m.m[0][0], _m.m[0][1], _m.m[0][2], _m.m[1][0], _m.m[1][1], _m.m[1][2], _m.m[2][0], _m.m[2][1], _m.m[2][2])
{
}

constexpr inline M32::M32(const M3 &_m)
  : M32(_m.m[0][0], _m.m[0][1], _m.m[1][0], _m.m[1][1], _m.m[2][0], _m.m[2][1])
{
}

#endif
