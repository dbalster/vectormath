/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "vectormath.h"

#include <float.h>

static const float fuzzy_epsilon = 0.01F;

auto bezier_decasteljau(float _t, float _t0, float _c0, float _c1, float _t1, int _iterations) -> float
{
	int i = 0;
	float u = 0.0f;
	float v = 1.0f;

	while (i < _iterations)
	{
		float a = (_t0 + _c0) * 0.5f;
		float b = (_c0 + _c1) * 0.5f;
		float c = (_c1 + _t1) * 0.5f;
		float d = (a + b) * 0.5f;
		float e = (b + c) * 0.5f;
		float t = (d + e) * 0.5f;

		if (std::abs(t - _t) < 0.001f)
		{
			return std::clamp((u + v) * 0.5f, 0.0f, 1.0f);
		}

		if (t < _t)
		{
			_t0 = t;
			_c0 = e;
			_c1 = c;
			u = (u + v) * 0.5f;
		}
		else
		{
			_c0 = a;
			_c1 = d;
			_t1 = t;
			v = (u + v) * 0.5f;
		}

		i++;
	}
	return std::clamp((u + v) * 0.5f, 0.0f, 1.0f);
}

Plane &Plane::normalize()
{
	float const len = 1.0F / std::sqrt(a * a + b * b + c * c);
	a *= len;
	b *= len;
	c *= len;
	d *= len;
	return *this;
}

std::istream &operator>>(std::istream &_in, V4 &_v)
{
	_in >> _v.x >> _v.y >> _v.z >> _v.w;
	return _in;
}

std::istream &operator>>(std::istream &_in, V3 &_v)
{
	_in >> _v.x >> _v.y >> _v.z;
	return _in;
}

std::istream &operator>>(std::istream &_in, V2 &_v)
{
	_in >> _v.x >> _v.y;
	return _in;
}

std::ostream &operator<<(std::ostream &_out, const V2 &_v)
{
	return _out << std::fixed << std::setprecision(2) << std::right << "⎧" << std::setw(6) << _v.x << " ⎫" << std::endl
				<< "⎩" << std::setw(6) << _v.y << " ⎭" << std::endl;
}

std::ostream &operator<<(std::ostream &_out, const V3 &_v)
{
	return _out << std::fixed << std::setprecision(2) << std::right << "⎧" << std::setw(6) << _v.x << " ⎫" << std::endl
				<< "⎪" << std::setw(6) << _v.y << " ⎪" << std::endl
				<< "⎩" << std::setw(6) << _v.z << " ⎭" << std::endl;
}

std::ostream &operator<<(std::ostream &_out, const V4 &_v)
{
	return _out << std::fixed << std::right << std::setprecision(2) << "⎧" << std::setw(6) << _v.x << " ⎫" << std::endl
				<< "⎪" << std::setw(6) << _v.y << " ⎪" << std::endl
				<< "⎪" << std::setw(6) << _v.z << " ⎪" << std::endl
				<< "⎩" << std::setw(6) << _v.w << " ⎭" << std::endl;
}

std::ostream &operator<<(std::ostream &_out, const Q &_v)
{
	return _out << std::fixed << std::right << std::setprecision(2) << "⎧" << std::setw(6) << _v.x << "ᵢ ⎫" << std::endl
				<< "⎪" << std::setw(6) << _v.y << "ⱼ ⎪" << std::endl
				<< "⎪" << std::setw(6) << _v.z << "ₖ ⎪" << std::endl
				<< "⎩" << std::setw(6) << _v.w << "  ⎭" << std::endl;
}

std::ostream &operator<<(std::ostream &_out, const M4 &_m)
{
	return _out << std::fixed << std::right << std::setprecision(2) << "⎧" << std::setw(6) << _m.m[0][0] << " " << std::setw(6) << _m.m[0][1] << " "
				<< std::setw(6) << _m.m[0][2] << " " << std::setw(6) << _m.m[0][3] << " ⎫" << std::endl
				<< "⎪" << std::setw(6) << _m.m[1][0] << " " << std::setw(6) << _m.m[1][1] << " " << std::setw(6) << _m.m[1][2] << " " << std::setw(6)
				<< _m.m[1][3] << " ⎪" << std::endl
				<< "⎪" << std::setw(6) << _m.m[2][0] << " " << std::setw(6) << _m.m[2][1] << " " << std::setw(6) << _m.m[2][2] << " " << std::setw(6)
				<< _m.m[2][3] << " ⎪" << std::endl
				<< "⎩" << std::setw(6) << _m.m[3][0] << " " << std::setw(6) << _m.m[3][1] << " " << std::setw(6) << _m.m[3][2] << " " << std::setw(6)
				<< _m.m[3][3] << " ⎭" << std::endl;
}

std::ostream &operator<<(std::ostream &_out, const M32 &_m)
{
	return _out << std::fixed << std::right << std::setprecision(2) << "⎧" << std::setw(6) << _m.m[0][0] << " " << std::setw(6) << _m.m[0][1] << " ⎫"
				<< std::endl
				<< "⎪" << std::setw(6) << _m.m[1][0] << " " << std::setw(6) << _m.m[1][1] << " ⎪" << std::endl
				<< "⎩" << std::setw(6) << _m.m[2][0] << " " << std::setw(6) << _m.m[2][1] << " ⎭" << std::endl;
}

std::ostream &operator<<(std::ostream &_out, const M3 &_m)
{
	return _out << std::fixed << std::right << std::setprecision(2) << "⎧" << std::setw(6) << _m.m[0][0] << " " << std::setw(6) << _m.m[0][1] << " "
				<< std::setw(6) << _m.m[0][2] << " ⎫" << std::endl
				<< "⎪" << std::setw(6) << _m.m[1][0] << " " << std::setw(6) << _m.m[1][1] << " " << std::setw(6) << _m.m[1][2] << " ⎪" << std::endl
				<< "⎩" << std::setw(6) << _m.m[2][0] << " " << std::setw(6) << _m.m[2][1] << " " << std::setw(6) << _m.m[2][2] << " ⎭" << std::endl;
}

std::ostream &operator<<(std::ostream &_out, const M43 &_m)
{
	return _out << std::fixed << std::right << std::setprecision(2) << "⎧" << std::setw(6) << _m.m[0][0] << " " << std::setw(6) << _m.m[0][1] << " "
				<< std::setw(6) << _m.m[0][2] << " ⎫" << std::endl
				<< "⎪" << std::setw(6) << _m.m[1][0] << " " << std::setw(6) << _m.m[1][1] << " " << std::setw(6) << _m.m[1][2] << " ⎪" << std::endl
				<< "⎪" << std::setw(6) << _m.m[2][0] << " " << std::setw(6) << _m.m[2][1] << " " << std::setw(6) << _m.m[2][2] << " ⎪" << std::endl
				<< "⎩" << std::setw(6) << _m.m[3][0] << " " << std::setw(6) << _m.m[3][1] << " " << std::setw(6) << _m.m[3][2] << " ⎭" << std::endl;
}

M4 M4::euler(rotation_order_type _order, const V3 &_v)
{
	float a = std::cos(_v.x), b = std::sin(_v.x);
	float c = std::cos(_v.y), d = std::sin(_v.y);
	float e = std::cos(_v.z), f = std::sin(_v.z);

	switch (_order)
	{
	case YXZ: {
		float ce = c * e, cf = c * f, de = d * e, df = d * f;
		return {ce + df * b, de * b - cf, a * d, 0.0F, a * f, a * e, -b, 0.0F, cf * b - de, df + ce * b, a * c, 0.0F, 0, 0, 0, 1};
	}
	break;

	case ZXY: {
		float ce = c * e, cf = c * f, de = d * e, df = d * f;
		return {ce - df * b, -a * f, de + cf * b, 0, cf + de * b, a * e, df - ce * b, 0, -a * d, b, a * c, 0, 0, 0, 0, 1};
	}
	break;

	case ZYX: {
		float ae = a * e, af = a * f, be = b * e, bf = b * f;
		return {c * e, be * d - af, ae * d + bf, 0, c * f, bf * d + ae, af * d - be, 0, -d, b * c, a * c, 0, 0, 0, 0, 1};
	}
	break;

	case YZX: {
		float ac = a * c, ad = a * d, bc = b * c, bd = b * d;
		return {c * e, bd - ac * f, bc * f + ad, 0, f, a * e, -b * e, 0, -d * e, ad * f + bc, ac - bd * f, 0, 0, 0, 0, 1};
	}
	break;

	case XZY: {
		float ac = a * c, ad = a * d, bc = b * c, bd = b * d;
		return {c * e, -f, d * e, 0, ac * f + bd, a * e, ad * f - bc, 0, bc * f - ad, b * e, bd * f + ac, 0, 0, 0, 0, 1};
	}
	break;

	default: // XYZ
	{
		float ae = a * e, af = a * f, be = b * e, bf = b * f;
		return {c * e, -c * f, d, 0, af + be * d, ae - bf * d, -b * c, 0, bf - ae * d, be + af * d, a * c, 0, 0, 0, 0, 1};
	}
	break;
	}
}

M4 M4::operator*(const M43 &_rhs) const
{
	return {
	  m[0][0] * _rhs.m[0][0] + m[1][0] * _rhs.m[0][1] + m[2][0] * _rhs.m[0][2],
	  m[0][1] * _rhs.m[0][0] + m[1][1] * _rhs.m[0][1] + m[2][1] * _rhs.m[0][2],
	  m[0][2] * _rhs.m[0][0] + m[1][2] * _rhs.m[0][1] + m[2][2] * _rhs.m[0][2],
	  m[0][3] * _rhs.m[0][0] + m[1][3] * _rhs.m[0][1] + m[2][3] * _rhs.m[0][2],
	  m[0][0] * _rhs.m[1][0] + m[1][0] * _rhs.m[1][1] + m[2][0] * _rhs.m[1][2],
	  m[0][1] * _rhs.m[1][0] + m[1][1] * _rhs.m[1][1] + m[2][1] * _rhs.m[1][2],
	  m[0][2] * _rhs.m[1][0] + m[1][2] * _rhs.m[1][1] + m[2][2] * _rhs.m[1][2],
	  m[0][3] * _rhs.m[1][0] + m[1][3] * _rhs.m[1][1] + m[2][3] * _rhs.m[1][2],
	  m[0][0] * _rhs.m[2][0] + m[1][0] * _rhs.m[2][1] + m[2][0] * _rhs.m[2][2],
	  m[0][1] * _rhs.m[2][0] + m[1][1] * _rhs.m[2][1] + m[2][1] * _rhs.m[2][2],
	  m[0][2] * _rhs.m[2][0] + m[1][2] * _rhs.m[2][1] + m[2][2] * _rhs.m[2][2],
	  m[0][3] * _rhs.m[2][0] + m[1][3] * _rhs.m[2][1] + m[2][3] * _rhs.m[2][2],
	  m[0][0] * _rhs.m[3][0] + m[1][0] * _rhs.m[3][1] + m[2][0] * _rhs.m[3][2] + m[3][0],
	  m[0][1] * _rhs.m[3][0] + m[1][1] * _rhs.m[3][1] + m[2][1] * _rhs.m[3][2] + m[3][1],
	  m[0][2] * _rhs.m[3][0] + m[1][2] * _rhs.m[3][1] + m[2][2] * _rhs.m[3][2] + m[3][2],
	  m[0][3] * _rhs.m[3][0] + m[1][3] * _rhs.m[3][1] + m[2][3] * _rhs.m[3][2] + m[3][3] //
	};
}

M4 M43::operator*(const M4 &_rhs) const
{
	return {
	  m[0][0] * _rhs.m[0][0] + m[1][0] * _rhs.m[0][1] + m[2][0] * _rhs.m[0][2] + m[3][0] * _rhs.m[0][3],
	  m[0][1] * _rhs.m[0][0] + m[1][1] * _rhs.m[0][1] + m[2][1] * _rhs.m[0][2] + m[3][1] * _rhs.m[0][3],
	  m[0][2] * _rhs.m[0][0] + m[1][2] * _rhs.m[0][1] + m[2][2] * _rhs.m[0][2] + m[3][2] * _rhs.m[0][3],
	  _rhs.m[0][3],
	  m[0][0] * _rhs.m[1][0] + m[1][0] * _rhs.m[1][1] + m[2][0] * _rhs.m[1][2] + m[3][0] * _rhs.m[1][3],
	  m[0][1] * _rhs.m[1][0] + m[1][1] * _rhs.m[1][1] + m[2][1] * _rhs.m[1][2] + m[3][1] * _rhs.m[1][3],
	  m[0][2] * _rhs.m[1][0] + m[1][2] * _rhs.m[1][1] + m[2][2] * _rhs.m[1][2] + m[3][2] * _rhs.m[1][3],
	  _rhs.m[1][3],
	  m[0][0] * _rhs.m[2][0] + m[1][0] * _rhs.m[2][1] + m[2][0] * _rhs.m[2][2] + m[3][0] * _rhs.m[2][3],
	  m[0][1] * _rhs.m[2][0] + m[1][1] * _rhs.m[2][1] + m[2][1] * _rhs.m[2][2] + m[3][1] * _rhs.m[2][3],
	  m[0][2] * _rhs.m[2][0] + m[1][2] * _rhs.m[2][1] + m[2][2] * _rhs.m[2][2] + m[3][2] * _rhs.m[2][3],
	  _rhs.m[2][3],
	  m[0][0] * _rhs.m[3][0] + m[1][0] * _rhs.m[3][1] + m[2][0] * _rhs.m[3][2] + m[3][0] * _rhs.m[3][3],
	  m[0][1] * _rhs.m[3][0] + m[1][1] * _rhs.m[3][1] + m[2][1] * _rhs.m[3][2] + m[3][1] * _rhs.m[3][3],
	  m[0][2] * _rhs.m[3][0] + m[1][2] * _rhs.m[3][1] + m[2][2] * _rhs.m[3][2] + m[3][2] * _rhs.m[3][3],
	  _rhs.m[3][3] //
	};
}

M43 M43::operator*(const M43 &_rhs) const
{
	return {
	  m[0][0] * _rhs.m[0][0] + m[1][0] * _rhs.m[0][1] + m[2][0] * _rhs.m[0][2],
	  m[0][1] * _rhs.m[0][0] + m[1][1] * _rhs.m[0][1] + m[2][1] * _rhs.m[0][2],
	  m[0][2] * _rhs.m[0][0] + m[1][2] * _rhs.m[0][1] + m[2][2] * _rhs.m[0][2],
	  m[0][0] * _rhs.m[1][0] + m[1][0] * _rhs.m[1][1] + m[2][0] * _rhs.m[1][2],
	  m[0][1] * _rhs.m[1][0] + m[1][1] * _rhs.m[1][1] + m[2][1] * _rhs.m[1][2],
	  m[0][2] * _rhs.m[1][0] + m[1][2] * _rhs.m[1][1] + m[2][2] * _rhs.m[1][2],
	  m[0][0] * _rhs.m[2][0] + m[1][0] * _rhs.m[2][1] + m[2][0] * _rhs.m[2][2],
	  m[0][1] * _rhs.m[2][0] + m[1][1] * _rhs.m[2][1] + m[2][1] * _rhs.m[2][2],
	  m[0][2] * _rhs.m[2][0] + m[1][2] * _rhs.m[2][1] + m[2][2] * _rhs.m[2][2],
	  m[0][0] * _rhs.m[3][0] + m[1][0] * _rhs.m[3][1] + m[2][0] * _rhs.m[3][2] + m[3][0],
	  m[0][1] * _rhs.m[3][0] + m[1][1] * _rhs.m[3][1] + m[2][1] * _rhs.m[3][2] + m[3][1],
	  m[0][2] * _rhs.m[3][0] + m[1][2] * _rhs.m[3][1] + m[2][2] * _rhs.m[3][2] + m[3][2] //
	};
}

float M3::determinant() const
{
	return m[0][0] * m[1][1] * m[2][2] + m[0][1] * m[1][2] * m[2][0] + m[0][2] * m[1][0] * m[2][1] - m[0][0] * m[1][2] * m[2][1] - m[0][1] * m[1][0] * m[2][2] -
		   m[0][2] * m[1][1] * m[2][0];
}

M4 &M4::perspectiveFovLH(float _yFov, float _aspect, float _near, float _far)
{
	float const ys = 1.0F / tanf(deg2rad(_yFov * 0.5F));

	m[0][0] = ys / _aspect;
	m[0][1] = 0.0F;
	m[0][2] = 0.0F;
	m[0][3] = 0.0F;

	m[1][0] = 0.0F;
	m[1][1] = ys;
	m[1][2] = 0.0F;
	m[1][3] = 0.0F;

	m[2][0] = 0.0F;
	m[2][1] = 0.0F;
	m[2][2] = _far / (_far - _near);
	m[2][3] = +1.0F;

	m[3][0] = 0.0F;
	m[3][1] = 0.0F;
	m[3][2] = -(_near * _far) / (_far - _near);
	m[3][3] = 0.0F;
	return *this;
}

M4 &M4::perspectiveFovRH(float _yFov, float _aspect, float _near, float _far)
{
	float const ys = 1.0F / tanf(deg2rad(_yFov * 0.5F));

	m[0][0] = ys / _aspect;
	m[0][1] = 0.0F;
	m[0][2] = 0.0F;
	m[0][3] = 0.0F;

	m[1][0] = 0.0F;
	m[1][1] = ys;
	m[1][2] = 0.0F;
	m[1][3] = 0.0F;

	m[2][0] = 0.0F;
	m[2][1] = 0.0F;
	m[2][2] = (_far) / (_near - _far);
	m[2][3] = -1.0F;

	m[3][0] = 0.0F;
	m[3][1] = 0.0F;
	m[3][2] = (_near * _far) / (_near - _far);
	m[3][3] = 0.0F;
	return *this;
}

M4 &M4::perspectiveLH(float _w, float _h, float _near, float _far)
{
	m[0][0] = (2.0F * _near) / (_w);
	m[1][0] = 0.0F;
	m[2][0] = 0.0F;
	m[3][0] = 0.0F;

	m[0][1] = 0.0F;
	m[1][1] = (2.0F * _near) / (_h);
	m[2][1] = 0.0F;
	m[3][1] = 0.0F;

	m[0][2] = 0.0F;
	m[1][2] = 0.0F;
	m[2][2] = _far / (_far - _near);
	m[3][2] = +1.0F;

	m[0][3] = 0.0F;
	m[1][3] = 0.0F;
	m[2][3] = (_near * _far) / (_near - _far);
	m[3][3] = 0.0F;
	return *this;
}

M4 &M4::perspectiveRH(float _w, float _h, float _near, float _far)
{
	m[0][0] = (2.0F * _near) / (_w);
	m[0][1] = 0.0F;
	m[0][2] = 0.0F;
	m[0][3] = 0.0F;
	m[1][0] = 0.0F;
	m[1][1] = (2.0F * _near) / (_h);
	m[1][2] = 0.0F;
	m[1][3] = 0.0F;
	m[2][0] = 0.0F;
	m[2][1] = 0.0F;
	m[2][2] = (_near * _far) / (_near - _far);
	m[2][3] = -1.0F;
	m[3][0] = 0.0F;
	m[3][1] = 0.0F;
	m[3][2] = _far / (_near - _far);
	m[3][3] = 0.0F;
	return *this;
}

M4 &M4::ortho(float _left, float _right, float _top, float _bottom, float _near, float _far)
{
	m[0][0] = 2.0F / (_right - _left);
	m[0][1] = 0.0F;
	m[0][2] = 0.0F;
	m[0][3] = 0.0F;
	m[1][0] = 0.0F;
	m[1][1] = 2.0F / (_top - _bottom);
	m[1][2] = 0.0F;
	m[1][3] = 0.0F;
	m[2][0] = 0.0F;
	m[2][1] = 0.0F;
	m[2][2] = -2.0F / (_far - _near);
	m[2][3] = 0.0F;
	m[3][0] = -(_right + _left) / (_right - _left);
	m[3][1] = -(_top + _bottom) / (_top - _bottom);
	m[3][2] = -(_far + _near) / (_far - _near);
	m[3][3] = 1.0F;
	return *this;
}

M4 &M4::frustum(float _left, float _right, float _bottom, float _top, float _near, float _far)
{
	m[0][0] = (2.0F * _near) / (_right - _left);
	m[0][1] = 0.0F;
	m[0][2] = 0.0F;
	m[0][3] = 0.0F;
	m[1][0] = 0.0F;
	m[1][1] = (2.0F * _near) / (_top - _bottom);
	m[1][2] = 0.0F;
	m[1][3] = 0.0F;
	m[2][0] = (_right + _left) / (_right - _left);
	m[2][1] = (_top + _bottom) / (_top - _bottom);
	m[2][2] = -(_far + _near) / (_far - _near);
	m[2][3] = -1.0F;
	m[3][0] = 0.0F;
	m[3][1] = 0.0F;
	m[3][2] = -(2.0F * _near * _far) / (_far - _near);
	m[3][3] = 0.0F;
	return *this;
}

M4 &M4::persp(float _y_fov, float _aspect, float zNear, float zFar)
{
	float const s = std::tan(deg2rad(_y_fov * 0.5F)) * zNear;
	float r = _aspect * s, l = -r;
	float t = s, b = -t;
	return frustum(l, r, b, t, zNear, zFar);
}

M4 &M4::invert()
{
	throw "broken, please check";
	float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2], m03 = m[0][3];
	float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2], m13 = m[1][3];
	float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2], m23 = m[2][3];
	float m30 = m[3][0], m31 = m[3][1], m32 = m[3][2], m33 = m[3][3];

	float v0 = m20 * m31 - m21 * m30;
	float v1 = m20 * m32 - m22 * m30;
	float v2 = m20 * m33 - m23 * m30;
	float v3 = m21 * m32 - m22 * m31;
	float v4 = m21 * m33 - m23 * m31;
	float v5 = m22 * m33 - m23 * m32;

	float const t00 = +(v5 * m11 - v4 * m12 + v3 * m13);
	float const t10 = -(v5 * m10 - v2 * m12 + v1 * m13);
	float const t20 = +(v4 * m10 - v2 * m11 + v0 * m13);
	float const t30 = -(v3 * m10 - v1 * m11 + v0 * m12);

	float const det = (t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03);

	if (fabs(det) < FLT_EPSILON)
		throw NonInvertibleException();
	float const invDet = 1.0F / det;

	m[0][0] = t00 * invDet;
	m[1][0] = t10 * invDet;
	m[2][0] = t20 * invDet;
	m[3][0] = t30 * invDet;

	m[0][1] = -(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
	m[1][1] = +(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
	m[2][1] = -(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
	m[3][1] = +(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

	v0 = m10 * m31 - m11 * m30;
	v1 = m10 * m32 - m12 * m30;
	v2 = m10 * m33 - m13 * m30;
	v3 = m11 * m32 - m12 * m31;
	v4 = m11 * m33 - m13 * m31;
	v5 = m12 * m33 - m13 * m32;

	m[0][2] = +(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
	m[1][2] = -(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
	m[2][2] = +(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
	m[3][2] = -(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

	v0 = m21 * m10 - m20 * m11;
	v1 = m22 * m10 - m20 * m12;
	v2 = m23 * m10 - m20 * m13;
	v3 = m22 * m11 - m21 * m12;
	v4 = m23 * m11 - m21 * m13;
	v5 = m23 * m12 - m22 * m13;

	m[0][3] = -(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
	m[1][3] = +(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
	m[2][3] = -(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
	m[3][3] = +(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

	return *this;
}

M43 &M43::invert()
{
	float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2];
	float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2];
	float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2];
	float m30 = m[3][0], m31 = m[3][1], m32 = m[3][2];

	float det33 = m00 * (m11 * m22 - m12 * m21) - m01 * (m10 * m22 - m12 * m20) + m02 * (m10 * m21 - m11 * m20);

	if (det33 == 0)
		throw NonInvertibleException();

	float invDet33 = 1.0f / det33;

	m[0][0] = (m11 * m22 - m21 * m12) * invDet33;
	m[0][1] = -(m01 * m22 - m21 * m02) * invDet33;
	m[0][2] = (m01 * m12 - m11 * m02) * invDet33;
	m[1][0] = -(m10 * m22 - m20 * m12) * invDet33;
	m[1][1] = (m00 * m22 - m20 * m02) * invDet33;
	m[1][2] = -(m00 * m12 - m10 * m02) * invDet33;
	m[2][0] = (m10 * m21 - m20 * m11) * invDet33;
	m[2][1] = -(m00 * m21 - m20 * m01) * invDet33;
	m[2][2] = (m00 * m11 - m10 * m01) * invDet33;

	m[3][0] = -(m30 * m[0][0] + m31 * m[1][0] + m32 * m[2][0]);
	m[3][1] = -(m30 * m[0][1] + m31 * m[1][1] + m32 * m[2][1]);
	m[3][2] = -(m30 * m[0][2] + m31 * m[1][2] + m32 * m[2][2]);

	return *this;
}

M4::M4(const M43 &_m43)
  : M4(_m43.m[0][0], _m43.m[0][1], _m43.m[0][2], 0, _m43.m[1][0], _m43.m[1][1], _m43.m[1][2], 0, _m43.m[2][0], _m43.m[2][1], _m43.m[2][2], 0, _m43.m[3][0],
	   _m43.m[3][1], _m43.m[3][2], 1)
{
}

M4::M4(const M3 &_m3)
  : M4(_m3.m[0][0], _m3.m[0][1], _m3.m[0][2], 0, _m3.m[1][0], _m3.m[1][1], _m3.m[1][2], 0, _m3.m[2][0], _m3.m[2][1], _m3.m[2][2], 0, 0, 0, 0, 1)
{
}

auto Plane::operator*(const V3 &_p) const -> float
{
	return a * _p.x + b * _p.y + c * _p.z + d;
}

// span Plane from two vectors
Plane::Plane(const V3 &_v1, const V3 &_v2)
{
	auto v = (_v1.cross(_v2)).normalize();
	a = v.x;
	b = v.y;
	c = v.z;
	d = -v.dot(_v1);
}

// span Plane from three points
Plane::Plane(const V3 &_p1, const V3 &_p2, const V3 &_p3)
{
	V3 const e1 = _p2 - _p1;
	V3 const e2 = _p3 - _p1;
	V3 const n = e1.cross(e2).normalize();
	d = -n.dot(_p1);
}

// is a point inside the Frustum?
bool Frustum::inside(const V3 &_p) const
{
	return (planes[LEFT] * _p) >= 0 && (planes[RIGHT] * _p) <= 0 && (planes[TOP] * _p) >= 0 && (planes[BOTTOM] * _p) <= 0 && (planes[NEAR] * _p) >= 0 &&
		   (planes[FAR] * _p) <= 0;
}

Frustum::Frustum(const M4 &_m)
{
	planes[LEFT].a = _m.m[0][3] + _m.m[0][0];
	planes[LEFT].b = _m.m[1][3] + _m.m[1][0];
	planes[LEFT].c = _m.m[2][3] + _m.m[2][0];
	planes[LEFT].d = _m.m[3][3] + _m.m[3][0];
	planes[RIGHT].a = _m.m[0][3] - _m.m[0][0];
	planes[RIGHT].b = _m.m[1][3] - _m.m[1][0];
	planes[RIGHT].c = _m.m[2][3] - _m.m[2][0];
	planes[RIGHT].d = _m.m[3][3] - _m.m[3][0];
	planes[BOTTOM].a = _m.m[0][3] + _m.m[0][1];
	planes[BOTTOM].b = _m.m[1][3] + _m.m[1][1];
	planes[BOTTOM].c = _m.m[2][3] + _m.m[2][1];
	planes[BOTTOM].d = _m.m[3][3] + _m.m[3][1];
	planes[TOP].a = _m.m[0][3] - _m.m[0][1];
	planes[TOP].b = _m.m[1][3] - _m.m[1][1];
	planes[TOP].c = _m.m[2][3] - _m.m[2][1];
	planes[TOP].d = _m.m[3][3] - _m.m[3][1];
	planes[NEAR].a = _m.m[0][3] + _m.m[0][2];
	planes[NEAR].b = _m.m[1][3] + _m.m[1][2];
	planes[NEAR].c = _m.m[2][3] + _m.m[2][2];
	planes[NEAR].d = _m.m[3][3] + _m.m[3][2];
	planes[FAR].a = _m.m[0][3] - _m.m[0][2];
	planes[FAR].b = _m.m[1][3] - _m.m[1][2];
	planes[FAR].c = _m.m[2][3] - _m.m[2][2];
	planes[FAR].d = _m.m[3][3] - _m.m[3][2];

	for (auto &plane : planes)
	{
		plane.normalize();
	}
}

bool V3::operator==(const V3 &_rhs) const
{
	return std::abs(_rhs.x - x) < fuzzy_epsilon && std::abs(_rhs.y - y) < fuzzy_epsilon && std::abs(_rhs.z - z) < fuzzy_epsilon;
}

M3 M3::inverted() const
{
	float det = determinant();
	if (det > -FLT_EPSILON && det < +FLT_EPSILON)
		return *this;
	det = 1.0F / det;
	return {(m[1][1] * m[2][2] - m[1][2] * m[2][1]) * det, (m[0][2] * m[2][1] - m[0][1] * m[2][2]) * det, (m[0][1] * m[1][2] - m[0][2] * m[1][1]) * det,
			(m[1][2] * m[2][0] - m[1][0] * m[2][2]) * det, (m[0][0] * m[2][2] - m[0][2] * m[2][0]) * det, (m[0][2] * m[1][0] - m[0][0] * m[1][2]) * det,
			(m[1][0] * m[2][1] - m[1][1] * m[2][0]) * det, (m[0][1] * m[2][0] - m[0][0] * m[2][1]) * det, (m[0][0] * m[1][1] - m[0][1] * m[1][0]) * det};
}

M43 M43::rotate(const V3 &_axis, float angleInRad)
{
	float const s = std::sin(angleInRad);
	float const c = std::cos(angleInRad);
	float const t = 1.0F - c;

	V3 const p = _axis.normalized();

	return {(t * p.x * p.x) + (c),
			(t * p.x * p.y) + (s * p.z),
			(t * p.x * p.z) - (s * p.y),
			(t * p.x * p.y) - (s * p.z),
			(t * p.y * p.y) + (c),
			(t * p.y * p.z) + (s * p.x),
			(t * p.x * p.z) + (s * p.y),
			(t * p.y * p.z) - (s * p.x),
			(t * p.z * p.z) + (c),
			0.0F,
			0.0F,
			0.0F};
}

M43 M43::euler(rotation_order_type _order, const V3 &_v)
{
	float a = std::cos(deg2rad(_v.x)), b = std::sin(deg2rad(_v.x));
	float c = std::cos(deg2rad(_v.y)), d = std::sin(deg2rad(_v.y));
	float e = std::cos(deg2rad(_v.z)), f = std::sin(deg2rad(_v.z));

	switch (_order)
	{
	case YXZ: {
		float ce = c * e, cf = c * f, de = d * e, df = d * f;
		return {ce + df * b, de * b - cf, a * d, a * f, a * e, -b, cf * b - de, df + ce * b, a * c, 0, 0, 0};
	}
	break;

	case ZXY: {
		float ce = c * e, cf = c * f, de = d * e, df = d * f;
		return {ce - df * b, -a * f, de + cf * b, cf + de * b, a * e, df - ce * b, -a * d, b, a * c, 0, 0, 0};
	}
	break;

	case ZYX: {
		float ae = a * e, af = a * f, be = b * e, bf = b * f;
		return {c * e, be * d - af, ae * d + bf, c * f, bf * d + ae, af * d - be, -d, b * c, a * c, 0, 0, 0};
	}
	break;

	case YZX: {
		float ac = a * c, ad = a * d, bc = b * c, bd = b * d;
		return {c * e, bd - ac * f, bc * f + ad, f, a * e, -b * e, -d * e, ad * f + bc, ac - bd * f, 0, 0, 0};
	}
	break;

	case XZY: {
		float ac = a * c, ad = a * d, bc = b * c, bd = b * d;
		return {c * e, -f, d * e, ac * f + bd, a * e, ad * f - bc, bc * f - ad, b * e, bd * f + ac, 0, 0, 0};
	}
	break;

	default: // XYZ
	{
		float ae = a * e, af = a * f, be = b * e, bf = b * f;
		return {c * e, -c * f, d, af + be * d, ae - bf * d, -b * c, bf - ae * d, be + af * d, a * c, 0, 0, 0};
	}
	break;
	}
}

Q Q::from_axis_angle(const V3 &_axis, float _angle)
{
	Q q(_axis.normalized());
	float const angle = deg2rad(_angle * 0.5F);
	float const s = std::sin(angle);
	q.x *= s;
	q.y *= s;
	q.z *= s;
	q.w = std::cos(angle);
	return q;
}

// http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToquat/index.htm
Q Q::from_euler(const V3 &_eulers)
{
	float const a = _eulers.x * 0.5F; // heading
	float const b = _eulers.y * 0.5F; // attitude
	float const c = _eulers.z * 0.5F; // bank
	float const c1 = std::cos(a);
	float const c2 = std::cos(b);
	float const c3 = std::cos(c);
	float const s1 = std::sin(a);
	float const s2 = std::sin(b);
	float const s3 = std::sin(c);
	return {s1 * s2 * c3 + c1 * c2 * s3, s1 * c2 * c3 + c1 * s2 * s3, c1 * s2 * c3 - s1 * c2 * s3, c1 * c2 * c3 - s1 * s2 * s3};
}

Q Q::from_m4(const M4 &_m)
{
	float const diag = _m.m[0][0] + _m.m[1][1] + _m.m[2][2] + 1;
	Q q;

	if (diag > 0.0F)
	{
		const float scale = std::sqrt(diag) * 2.0F; // get scale from diagonal

		// TODO: speed this up
		q.x = (_m.m[1][2] - _m.m[2][1]) / scale;
		q.y = (_m.m[2][0] - _m.m[0][2]) / scale;
		q.z = (_m.m[0][1] - _m.m[1][0]) / scale;
		q.w = 0.25F * scale;
	}
	else
	{
		if (_m.m[0][0] > _m.m[1][1] && _m.m[0][0] > _m.m[2][2])
		{
			// 1st element of diag is greatest value
			// find scale according to 1st element, and double it
			const float scale = std::sqrt(1.0F + _m.m[0][0] - _m.m[1][1] - _m.m[2][2]) * 2.0F;

			// TODO: speed this up
			q.x = 0.25F * scale;
			q.y = (_m.m[1][0] + _m.m[0][1]) / scale;
			q.z = (_m.m[0][2] + _m.m[2][0]) / scale;
			q.w = (_m.m[1][2] - _m.m[2][1]) / scale;
		}
		else if (_m.m[1][1] > _m.m[2][2])
		{
			// 2nd element of diag is greatest value
			// find scale according to 2nd element, and double it
			const float scale = std::sqrt(1.0F + _m.m[1][1] - _m.m[0][0] - _m.m[2][2]) * 2.0F;

			// TODO: speed this up
			q.x = (_m.m[1][0] + _m.m[0][1]) / scale;
			q.y = 0.25F * scale;
			q.z = (_m.m[2][1] + _m.m[1][2]) / scale;
			q.w = (_m.m[2][0] - _m.m[0][2]) / scale;
		}
		else
		{
			// 3rd element of diag is greatest value
			// find scale according to 3rd element, and double it
			const float scale = sqrtf(1.0F + _m.m[2][2] - _m.m[0][0] - _m.m[1][1]) * 2.0F;

			// TODO: speed this up
			q.x = (_m.m[2][0] + _m.m[0][2]) / scale;
			q.y = (_m.m[2][1] + _m.m[1][2]) / scale;
			q.z = 0.25F * scale;
			q.w = (_m.m[0][1] - _m.m[1][0]) / scale;
		}
	}

	return q.normalize();
}

Q &Q::normalize()
{
	float const invlen = 1.0F / length();
	x *= invlen;
	y *= invlen;
	z *= invlen;
	w *= invlen;
	return *this;
}

Q Q::normalized() const
{
	return Q(*this).normalize();
}

Q &Q::inverse()
{
	float const l = 1.0F / x * x + y * y + z * z + w * w;
	x *= -l;
	y *= -l;
	z *= -l;
	w *= l;
	return *this;
}

Q Q::inversed() const
{
	return Q(*this).inverse();
}

Q Q::operator*(const Q &_rhs)
{
	return {x * _rhs.w + y * _rhs.z + w * _rhs.x - z * _rhs.y, y * _rhs.w + z * _rhs.x + w * _rhs.y - x * _rhs.z,
			x * _rhs.y + z * _rhs.w + w * _rhs.z - y * _rhs.x, w * _rhs.w - x * _rhs.x - y * _rhs.y - z * _rhs.z};
}
Q slerp(const Q &_a, const Q &_b, float _t)
{
	float cosVal = (_a.x * _b.x) + (_a.y * _b.y) + (_a.z * _b.z) + (_a.w * _b.w);
	if (cosVal > 1.0F || cosVal < -1.0F)
	{
		return _b;
	}

	float factor = 1.0F;
	if (cosVal < 0.0F)
	{
		factor = -1.0F;
		cosVal = -cosVal;
	}

	float const angle = std::acos(cosVal);
	if (angle <= 0.0001F)
	{
		return _b;
	}

	float const invSinVal = 1.0F / std::sin(angle);
	float const c0 = std::sin((1.0F - _t) * angle) * invSinVal;
	float const c1 = factor * std::sin(_t * angle) * invSinVal;

	return {_a.x * c0 + _b.x * c1, _a.y * c0 + _b.y * c1, _a.z * c0 + _b.z * c1, _a.w * c0 + _b.w * c1};
}

M4 M4::quaternion(const Q &_q)
{
	return {1.0F - 2.0F * (_q.y * _q.y + _q.z * _q.z),
			2.0F * (_q.x * _q.y + _q.z * _q.w),
			2.0F * (_q.z * _q.x - _q.y * _q.w),
			0.0F,
			2.0F * (_q.x * _q.y - _q.z * _q.w),
			1.0F - 2.0F * (_q.z * _q.z + _q.x * _q.x),
			2.0F * (_q.y * _q.z + _q.x * _q.w),
			0.0F,
			2.0F * (_q.z * _q.x + _q.y * _q.w),
			2.0F * (_q.y * _q.z - _q.x * _q.w),
			1.0F - 2.0F * (_q.y * _q.y + _q.x * _q.x),
			0.0F,
			0.0F,
			0.0F,
			0.0F,
			1.0F};
}

M43 M43::quaternion(const Q &_q)
{
	return {1.0F - 2.0F * (_q.y * _q.y + _q.z * _q.z), 2.0F * (_q.x * _q.y + _q.z * _q.w),		  2.0F * (_q.z * _q.x - _q.y * _q.w),		 0.0F,
			2.0F * (_q.x * _q.y - _q.z * _q.w),		   1.0F - 2.0F * (_q.z * _q.z + _q.x * _q.x), 2.0F * (_q.y * _q.z + _q.x * _q.w),		 0.0F,
			2.0F * (_q.z * _q.x + _q.y * _q.w),		   2.0F * (_q.y * _q.z - _q.x * _q.w),		  1.0F - 2.0F * (_q.y * _q.y + _q.x * _q.x), 0.0F};
}
// this is just a basic working version.
// http://fgiesen.wordpress.com/2010/10/17/view-frustum-culling
bool Frustum::intersects(const AABB3 &_aabb) const
{
	float const cx = _aabb.center[0];
	float const cy = _aabb.center[1];
	float const cz = _aabb.center[2];
	float const rx = _aabb.radius[0];
	float const ry = _aabb.radius[1];
	float const rz = _aabb.radius[2];

	for (int i = 0; i < 6; ++i)
	{
		const Plane &p = planes[i];
		if (																	//
		  ((p.a * (cx - rx) + p.b * (cy - ry) + p.c * (cz - rz) + p.d) <= 0) && //
		  ((p.a * (cx + rx) + p.b * (cy - ry) + p.c * (cz - rz) + p.d) <= 0) && //
		  ((p.a * (cx - rx) + p.b * (cy + ry) + p.c * (cz - rz) + p.d) <= 0) && //
		  ((p.a * (cx + rx) + p.b * (cy + ry) + p.c * (cz - rz) + p.d) <= 0) && //
		  ((p.a * (cx - rx) + p.b * (cy - ry) + p.c * (cz + rz) + p.d) <= 0) && //
		  ((p.a * (cx + rx) + p.b * (cy - ry) + p.c * (cz + rz) + p.d) <= 0) && //
		  ((p.a * (cx - rx) + p.b * (cy + ry) + p.c * (cz + rz) + p.d) <= 0) && //
		  ((p.a * (cx + rx) + p.b * (cy + ry) + p.c * (cz + rz) + p.d) <= 0))
		{
			return false;
		}
	}

	return true;
}
