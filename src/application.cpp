#include "application.h"

#include "camera.h"
#include "color.h"
#include "flatimporter.h"
#include "material.h"
#include "node.h"
#include "rasterizer.h"
#include "terminal.h"
#include "vectormath.h"

#if defined(USE_CHAFA)
#include <chafa.h>
#endif

#include <chrono>
#include <sstream>
#include <string>
#include <vector>

using std::string;
using std::stringstream;
using std::vector;
using namespace std::chrono;
using namespace std::chrono_literals;

class Blur : public PostprocessingFilter
{
	std::vector<Color> buffer;
	int width, height;

public:
	void resize(int _width, int _height) override
	{
		width = _width;
		height = _height;
		buffer.resize(_width * _height);
	}

	std::array<float, 3 * 3> kernel{0.5F, 0.5F, 0.5F, 0.5F, 4.0F, 0.5F, 0.5F, 0.5F, 0.5F};

	std::vector<Color> &process(std::vector<Color> &_color) override
	{
		for (int y = 0; y < height; ++y)
		{
			for (int x = 0; x < width; ++x)
			{
				float sumR = 0, sumG = 0, sumB = 0;
				float div = 0;
				for (int iy = 0; iy <= 2; ++iy)
				{
					int dy = y + iy - 1;
					if (dy < 0 || dy >= height)
						continue;
					for (int ix = 0; ix <= 2; ++ix)
					{
						int dx = x + ix - 1;
						if (dx < 0 || dx >= width)
							continue;

						float weight = kernel[iy * 3 + ix];

						Color color = _color[dy * width + dx];
						sumR += color.red * weight;
						sumG += color.green * weight;
						sumB += color.blue * weight;
						div++;
					}
				}
				buffer[y * width + x] = Color(sumR / div, sumG / div, sumB / div, 0);
			}
		}

		return buffer;
	}
};

class KittyEncoder
{
public:
	static std::string Encode(const std::vector<Color> &input, int width, int height)
	{
		static const std::string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

		std::string output;
		output.reserve(((input.size() * 4) / 3) + (input.size() % 3 ? 4 : 0) + 100); // Extra Platz für ESCAPE-Sequenzen

		// ESCAPE-Sequenzen für das Kitty Graphics Protocol
		output += "\x1b_G"; // Start der Grafikausgabe
		output += "f=24,";	// 24-Bit-Farben
		output += "s=" + std::to_string(width) + ",";
		output += "v=" + std::to_string(height) + ";";

		int i = 0;
		unsigned char char_array_3[3];
		unsigned char char_array_4[4];

		for (const auto &_color : input)
		{
			auto color = (r8g8b8_t)_color;
			char_array_3[i++] = color.red;
			processColorComponent(i, char_array_3, char_array_4, base64_chars, output);

			char_array_3[i++] = color.green;
			processColorComponent(i, char_array_3, char_array_4, base64_chars, output);

			char_array_3[i++] = color.blue;
			processColorComponent(i, char_array_3, char_array_4, base64_chars, output);
		}

		if (i)
		{
			for (int j = i; j < 3; j++)
				char_array_3[j] = '\0';

			encode3to4(char_array_3, char_array_4, base64_chars, output);

			while ((i++ < 3))
				output += '=';
		}

		output += "\x1b\\\x1b\\\x1b\\"; // Ende der Grafikausgabe
		return output;
	}

private:
	static void processColorComponent(int &i, unsigned char *char_array_3, unsigned char *char_array_4, const std::string &base64_chars, std::string &output)
	{
		if (i == 3)
		{
			encode3to4(char_array_3, char_array_4, base64_chars, output);
			i = 0;
		}
	}

	static void encode3to4(const unsigned char *char_array_3, unsigned char *char_array_4, const std::string &base64_chars, std::string &output)
	{
		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (int i = 0; i < 4; i++)
			output += base64_chars[char_array_4[i]];
	}
};

#if defined(USE_CHAFA)
class Display : public PostprocessingFilter
{
	int width, height;
	std::shared_ptr<Terminal> terminal;

	ChafaSymbolMap *symbol_map;
	ChafaCanvasConfig *config;
	ChafaCanvas *canvas;
	ChafaTermInfo *terminfo;
	std::vector<r8g8b8a8_t> buffer;

public:
	int scaleX{4};
	int scaleY{6};
	Display(std::shared_ptr<Terminal> _terminal)
	  : terminal(_terminal)
	{
		auto termdb = chafa_term_db_get_default();
		terminfo = chafa_term_db_detect(termdb, g_get_environ());
		symbol_map = chafa_symbol_map_new();
		chafa_symbol_map_add_by_tags(symbol_map, (ChafaSymbolTags)(CHAFA_SYMBOL_TAG_BLOCK)
									 // CHAFA_SYMBOL_TAG_SEXTANT
									 // CHAFA_SYMBOL_TAG_QUAD | CHAFA_SYMBOL_TAG_DIAGONAL));
									 // CHAFA_SYMBOL_TAG_BLOCK
									 // CHAFA_SYMBOL_TAG_ALL
		);
	}
	~Display() override
	{
		chafa_term_info_unref(terminfo);
		chafa_symbol_map_unref(symbol_map);
		chafa_canvas_unref(canvas);
		chafa_canvas_config_unref(config);
	}

	void resize(int _width, int _height) override
	{
		width = _width / scaleX;
		height = _height / scaleY;
		if (config)
		{
			chafa_canvas_config_unref(config);
			config = 0;
		}
		if (canvas)
		{
			chafa_canvas_unref(canvas);
			canvas = 0;
		}
		config = chafa_canvas_config_new();
		chafa_canvas_config_set_geometry(config, width, height);
		chafa_canvas_config_set_symbol_map(config, symbol_map);
		canvas = chafa_canvas_new(config);
	}
	std::vector<Color> &process(std::vector<Color> &_color) override
	{
		buffer.clear();
		// buffer.resize(_color.size());
		for (auto &c : _color)
		{
			buffer.push_back((r8g8b8a8_t)c);
		}

		guint8 *pixels = reinterpret_cast<guint8 *>(buffer.data());
		chafa_canvas_draw_all_pixels(canvas, CHAFA_PIXEL_RGBA8_PREMULTIPLIED, pixels, width * scaleX, height * scaleY, width * scaleX * 4);
		GString *gs = chafa_canvas_print(canvas, terminfo);
		terminal->write(gs->str, gs->len);
		g_string_free(gs, TRUE);

		return _color;
	}
};
#else

class Display : public PostprocessingFilter
{
	int width, height;
	std::shared_ptr<Terminal> terminal;

public:
	int scaleX{4};
	int scaleY{6};

	Display(std::shared_ptr<Terminal> _terminal)
	  : terminal(_terminal)
	{
	}

	void resize(int _width, int _height) override
	{
		width = _width;
		height = _height;
	}
	std::vector<Color> &process(std::vector<Color> &_color) override
	{
		auto result = KittyEncoder::Encode(_color, width, height);
		terminal->setCursor(0, 0);
		terminal->write(result.data(), result.size());
		return _color;
	}
};
#if 0
class DisplayX : public PostprocessingFilter
{
	int width, height;
	std::shared_ptr<Terminal> terminal;

public:
	int scaleX{2};
	int scaleY{2};

	DisplayX(std::shared_ptr<Terminal> _terminal)
	  : terminal(_terminal)
	{
	}

	void resize(int _width, int _height) override
	{
		width = _width;
		height = _height;
	}
	std::vector<Color> &process(std::vector<Color> &_color) override
	{
		Color c0old;
		Color c1old;

		auto c0 = _color.begin();
		auto c1 = c0 + width;
		c0old = c0->value + 1;
		c1old = c1->value + 1;
		for (int y = 0; y < height / scaleY; ++y)
		{
			terminal->setCursor(0, y);

			for (int x = 0; x < width / scaleX; ++x)
			{
				if (c0->value != c0old.value)
				{
					c0old.value = c0->value;
					terminal->printf("\x1b[38;2;%d;%d;%dm", c0->red(), c0->green(), c0->blue());
				}
				if (c1->value != c1old.value)
				{
					c1old.value = c1->value;
					terminal->printf("\x1b[48;2;%d;%d;%dm", c1->red(), c1->green(), c1->blue());
				}
				terminal->write("▀");

				c0++;
				c1++;
			}
			c0 += width;
			c1 += width;
		}
		return _color;
	}
};
#endif

#endif

void Application::signal_handler(const int sig)
{
	if (self == nullptr)
		return;
	switch (sig)
	{
#if defined(HAS_TERMIOS)
	case SIGWINCH:
		self->terminal->querySize();
		break;
#endif
	default:
		if (self->terminal)
			self->terminal->leaveRawMode();
		_exit(0);
		break;
	}
}

Application *Application::self{nullptr};

Application::Application()
{
	terminal = std::make_shared<Terminal>(8 * 1024 * 1024);
	display = std::make_shared<Display>(terminal);

	self = this;
	terminal->sizeChanged = [&](int _width, int _height) { sizeChanged(_width, _height); };

	rasterizer.filters.push_back(display);

	signal(SIGINT, signal_handler);
#if defined(HAS_TERMIOS)
	signal(SIGTSTP, signal_handler);
	signal(SIGCONT, signal_handler);
	signal(SIGWINCH, signal_handler);
#endif
}

void Application::sizeChanged(int _width, int _height)
{
	rasterizer.resize(_width * display->scaleX, _height * display->scaleY);
}

Application::~Application()
{
	self = nullptr;
}

void Application::init()
{
	terminal->enterRawMode();
	terminal->querySize();
	terminal->flush();
	terminal->init();
	terminal->read(5);
}

void Application::exit()
{
	terminal->leaveRawMode();
	scene->clear();
	scene.reset();
}

void Application::setScene(std::shared_ptr<Node> _scene)
{
	if (scene)
	{
		scene->clear();
	}
	camera.reset();
	scene.reset();

	scene = _scene;
	elapsedTimeInSeconds = 0.0f;
}

void Application::onMouse(int _x, int _y, int _button)
{
}

void Application::onKey(int _key)
{
}

auto Application::run() -> int
{
	init();

	duration<double> time_to_render(1s); // forces first frame
	duration<double> time_to_update(0);
	duration<double> time_to_input(0);

	terminal->write("\x1b[?2026h");

	//	std::shared_ptr<Node> cam, fpc, light;

	// bool changed = true;
	time_point<high_resolution_clock> last = high_resolution_clock::now();
	running = true;
	while (running)
	{
		auto now = high_resolution_clock::now();

		duration<double> delta = now - last;
		last = now;

		time_to_render += delta;
		time_to_update += delta;
		time_to_input += delta;

		if (time_to_input >= milliseconds(10)) // 100 hz
		{
			int key = terminal->read(0);
			if (key != -1)
			{
				if (terminal->isMouse)
				{
					onMouse(terminal->mouseX, terminal->mouseY, key);
				}
				else
				{
					onKey(key);
				}
			}
			time_to_input -= milliseconds(10);
		}
		if (time_to_update >= milliseconds(1000 / 100)) // 100 Hz
		{
			update();
			time_to_update -= milliseconds(1000 / 100);
		}
		auto dt = milliseconds(1000 / 60);
		if (time_to_render >= dt)
		{
			time_to_render = 0s;
			rasterizer.clear();
			elapsedTimeInSeconds += dt.count() / 1000.0f;

			if (scene != nullptr)
			{
				preprocess();
				render();
				postprocess();
			}
		}
	}

	exit();
	return 0;
}

void Application::preprocess()
{
	float animationTime = elapsedTimeInSeconds * animationSpeedInFramesPerSecond;

	Material::for_each([&](std::shared_ptr<Material> m) {
		if (m->action)
			m->action->animate(reinterpret_cast<Bindable *>(m.get()), animationTime);
	});

	scene->animate(animationTime);
	scene->transform();
	scene->updateAABB();

	if (auto cam = camera.lock())
	{
		rasterizer.uniform.view = cam->world.inverted();

		std::shared_ptr<Camera> cameraData = std::static_pointer_cast<Camera>(cam->asset);
		if (cameraData)
		{ //
			rasterizer.uniform.projection.perspectiveFovRH(cameraData->yFov, cameraData->aspectRatio, cameraData->near, cameraData->far);

			//   world to clip space transform
			rasterizer.uniform.viewProjection = rasterizer.uniform.projection * rasterizer.uniform.view;

			rasterizer.uniform.normalSpace = cam->world.transposed();

			// frustum represents a volume to cull objects
			rasterizer.frustum = Frustum(rasterizer.uniform.viewProjection);
		}
	}
}

void Application::postprocess()
{
	terminal->flush();
	fflush(stdout);
}

void Application::render()
{
	culledObjects.clear();
	scene->cull(rasterizer.frustum, culledObjects);

	std::ranges::sort(/*std::execution::par_unseq,*/ culledObjects, [&](const auto &cc1, const auto &cc2) -> bool {
		if (cc1.pass < cc2.pass)
			return true;
		if (cc1.pass > cc2.pass)
			return false;
		if (cc1.pass == PASS_OPAQUE)
			return (cc1.distanceFromCamera < cc2.distanceFromCamera);
		return (cc1.distanceFromCamera > cc2.distanceFromCamera);
	});

	// process and optimize draw calls, then draw
	for (auto &cc : culledObjects)
	{
		cc.node->render(rasterizer);
	}

	terminal->setCursor(0, 0);
	rasterizer.process();
}

void Application::update()
{
}
