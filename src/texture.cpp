/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../stb/stb_image.h"

#include "color.h"

Texture2D::~Texture2D()
{
	free(data); // !! do not delete []
}

void Texture2D::load(const std::string &_filename)
{
	data = (rgba32_t *)stbi_load(_filename.c_str(), &width, &height, &channels, 4);
}

Color Texture2D::sample(float _u, float _v)
{
	if (data == nullptr)
		return Color();
	int x = modulo(_u) * width;
	int y = modulo(-_v) * height;

	if (sampleMode == SAMPLE_CONSTANT)
	{
		return Color(data[(y * width) + x].r8g8b8a8);
	}

	float red = 0;
	float green = 0;
	float blue = 0;
	float alpha = 0;
	float weight = 0;

	for (int i = -1; i <= 1; ++i)
	{
		int x_ = x + i;
		if (x_ < 0 || x_ >= width)
			continue;
		for (int j = -1; j <= 1; ++j)
		{
			int y_ = y + j;
			if (y_ < 0 || y_ >= height)
				continue;
			auto color = data[(y_ * width) + x_].r8g8b8a8;
			red += color.red;
			green += color.green;
			blue += color.blue;
			alpha += color.alpha;
			weight++;
		}
	}

	red /= weight;
	green /= weight;
	blue /= weight;
	alpha /= weight;

	return Color(red, green, blue, alpha);
}
