/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef TERMINALRENDERER_H
#define TERMINALRENDERER_H

#include <functional>
#include <list>
#include <vector>

#include "color.h"
#include "mesh.h"
#include "terminal.h"
#include "vectormath.h"

class PostprocessingFilter
{
public:
	virtual ~PostprocessingFilter() = default;
	virtual void resize(int _width, int _height) = 0;
	virtual std::vector<Color> &process(std::vector<Color> &_color) = 0;
};

enum depth_test_mode
{
	DEPTH_TEST_NEVER,
	DEPTH_TEST_LESS,
	DEPTH_TEST_EQUAL,
	DEPTH_TEST_LESS_OR_EQUAL,
	DEPTH_TEST_GREATER,
	DEPTH_TEST_NOT_EQUAL,
	DEPTH_TEST_GREATER_OR_EQUAL,
	DEPTH_TEST_ALWAYS,
};

enum blend_mode
{
	BLEND_MODE_OPAQUE,
	BLEND_MODE_TRANSPARENT,
};

enum render_pass
{
	PASS_EARLY_Z,
	PASS_OPAQUE,
	PASS_TRANSPARENT,
	PASS_GIZMOS,
};

struct PerVertex
{
	// V3 position; // in world
	V3 view;
	V4 clip;
	V3 ndc;
	V3 normal; // in view space
	V4 color;
	V2 raster;
	V2 uv;
	V3 light;
};

struct PerPixel
{
	V3 position; // in view space
	V3 normal;
	V4 color;
	V2 uv;
	float z;
};

struct PerTriangle
{
	M4 world2clip;
	M43 world2view;
	V3 normalCS;
	V3 normalVS;
	float dotScreenDir;
};

struct Uniform
{
	M43 view;
	M3 normalSpace;
	M4 projection;
	M4 viewProjection;
	V3 lightPosition; // in view space
	M3 normalSpaceModel;
	std::shared_ptr<Material> material;
};

class IPixelShader
{
public:
	virtual void shade(const PerTriangle &, const PerPixel &, Color &) = 0;
};

class IVertexShader
{
public:
	virtual void shade(const PerTriangle &, const Vertex &, PerVertex &) = 0;
};

class Rasterizer
{
	std::vector<Color> colorBuffer;
	std::vector<float> depthBuffer;

	// std::array<Color>

	Rasterizer(const Rasterizer &) = delete;
	Rasterizer(Rasterizer &&) = delete;
	auto operator=(const Rasterizer &) -> Rasterizer & = delete;
	auto operator=(Rasterizer &&) -> Rasterizer & = delete;

	int scaleX{1}, scaleY{1};

public:
	int width{0};
	int height{0};
	std::list<std::shared_ptr<PostprocessingFilter>> filters;

	Frustum frustum;
	// varyings

	std::vector<Color>::const_iterator begin() const
	{
		return colorBuffer.begin();
	}

	auto operator[](size_t index) -> const Color &
	{
		return colorBuffer[index];
	}

	PerVertex perVertex[3];

	Uniform uniform;
	int pixelsWritten{0};
	int zClipped{0};
	int culledTriangles{0};
	int renderedTriangles{0};
	int sentTriangles{0};
	float minz[1000];
	float maxz[1000];

	depth_test_mode depthTestMode{DEPTH_TEST_GREATER};
	bool enableDepthWrites{true};
	bool enableColorWrites{true};
	bool opaque{true}; // TODO: full support or blendfuncs
	bool cullClockwise{true};
	render_pass pass{PASS_OPAQUE};

	std::function<Color(const Uniform &_uniform, const PerTriangle &_perTriangle, const PerPixel &_perPixel)> pixelShader;
	std::function<void(const Uniform &_uniform, const PerTriangle &_perTriangle, const Vertex &_vertex, PerVertex &_perVertex)> vertexShader;

	void resize(int _width, int _height);

	Rasterizer() = default;
	~Rasterizer();

	void clear();
	void process();

	inline auto orient2d(const P2 &a, const P2 &b, const P2 &c) -> int
	{
		return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
	}

	enum Interpolations : int
	{
		IPOL_NONE = 0,
		IPOL_UV = 1,
		IPOL_NORM = 2,
		IPOL_POS = 4,
		IPOL_COL = 8,
		IPOL_ALL = ~0,
	};
	int interpolations{IPOL_ALL};

	void drawIndexedTriangles(const M43 &_model, const std::vector<Vertex> &_vertices, const std::vector<int> &_indices, int _count, int _offset);

private:
	auto rasterizeTriangle(const PerTriangle &perTriangle, const P2 &p0, const P2 &p1, const P2 &p2) -> int;
};

#endif
