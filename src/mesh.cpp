/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "mesh.h"
#include "rasterizer.h"
#include "material.h"

void Mesh::render(Rasterizer &_rasterizer, const M43 &_model) // draw one frame
{
	_rasterizer.uniform.normalSpaceModel = _rasterizer.uniform.normalSpace * _model;

	int offset = 0;
	for (auto i = 0U; i < batches.size(); ++i)
	{
		int count = batches[i];
		_rasterizer.uniform.material = materials[i];
		if (_rasterizer.uniform.material->texture == nullptr)
		{
			if (smooth)
			{
				// no texture but per pixel lighting

				_rasterizer.interpolations = Rasterizer::IPOL_NORM | Rasterizer::IPOL_POS;

				_rasterizer.pixelShader = [](auto &_uniform, auto &_perTriangle, auto &_perPixel) -> Color {
					V3 lightDir = -(_uniform.lightPosition - _perPixel.position).normalize();
					float diffuse = std::min(std::max(_perPixel.normal.dot(lightDir), 0.1F), 1.0F);
					return Color(_uniform.material->diffuse.x * diffuse, _uniform.material->diffuse.y * diffuse, _uniform.material->diffuse.z * diffuse,
								 _uniform.material->alpha);
				};

				_rasterizer.vertexShader = [](auto &_uniform, auto &_perTriangle, auto &_vertex, auto &_perVertex) {
					_perVertex.normal = _uniform.normalSpaceModel.transform(_vertex.normal);
				};
			}
			else
			{
				// no texture and per face lighting

				_rasterizer.interpolations = vertexcolors ? Rasterizer::IPOL_COL : Rasterizer::IPOL_NONE;
				_rasterizer.interpolations |= Rasterizer::IPOL_POS;

				if (vertexcolors)
				{
					_rasterizer.pixelShader = [](auto &_uniform, auto &_perTriangle, auto &_perPixel) -> Color {
						V4 diffuse = _perPixel.color;
						V4 diff = diffuse; //(_rasterizer.cullClockwise) ? diffuse : diffuse * 0.1F;
						V3 lightDir = (_uniform.lightPosition - _perPixel.position).normalize();
						float dot = std::min(std::max(_perTriangle.normalVS.dot(lightDir), 0.0F), 1.0F);
						return Color(diff.x * dot, diff.y * dot, diff.z * dot, _uniform.material->alpha);
					};
				}
				else
				{
					_rasterizer.pixelShader = [](auto &_uniform, auto &_perTriangle, auto &_perPixel) -> Color {
						V4 diffuse = _uniform.material->diffuse;
						V4 diff = diffuse; //(_rasterizer.cullClockwise) ? diffuse : diffuse * 0.1F;
						V3 lightDir = -(_uniform.lightPosition - _perPixel.position).normalize();
						float dot = std::min(std::max(_perTriangle.normalVS.dot(lightDir), 0.0F), 1.0F);
						return Color(diff.x * dot, diff.y * dot, diff.z * dot, _uniform.material->alpha);
					};
				}

				_rasterizer.vertexShader = [](auto &_uniform, auto &_perTriangle, auto &_vertex, auto &_perVertex) {};
			}
		}
		else
		{
			if (smooth)
			{
				_rasterizer.interpolations = Rasterizer::IPOL_ALL;

				_rasterizer.pixelShader = [](auto &_uniform, auto &_perTriangle, auto &_perPixel) -> Color {
					V3 lightDir = -(_uniform.lightPosition - _perPixel.position).normalize();
					float diffuse = std::min(std::max(_perPixel.normal.dot(lightDir), 0.1F), 1.0F);
					return _uniform.material->texture->sample(_perPixel.uv.x, _perPixel.uv.y) * diffuse;
				};

				_rasterizer.vertexShader = [](auto &_uniform, const auto &_perTriangle, auto &_vertex, auto &_perVertex) {
					_perVertex.normal = _uniform.normalSpaceModel.transform(_vertex.normal);
					_perVertex.uv = _vertex.uv; // todo: apply uv transform                      // stays in uv space
				};
			}
			else
			{
				_rasterizer.interpolations = vertexcolors ? Rasterizer::IPOL_COL : Rasterizer::IPOL_NONE;
				_rasterizer.interpolations |= Rasterizer::IPOL_UV;
				_rasterizer.interpolations |= Rasterizer::IPOL_POS;

				_rasterizer.pixelShader = [](auto &_uniform, auto &_perTriangle, auto &_perPixel) -> Color {
					V4 diffuse = _uniform.material->diffuse;

					V3 lightDir = -(_uniform.lightPosition - _perPixel.position).normalize();
					float dot = std::min(std::max(_perTriangle.normalVS.dot(lightDir), 0.0F), 1.0F);

					diffuse *= dot;
					diffuse.w = 1.0f;
					return _uniform.material->texture->sample(_perPixel.uv.x, _perPixel.uv.y) * diffuse;
				};

				_rasterizer.vertexShader = [](auto &_uniform, auto &_perTriangle, auto &_vertex, auto &_perVertex) {
					_perVertex.uv = _vertex.uv; // todo: apply uv transform                      // stays in uv space
				};
			}
		}

		// set blend operation. currently it's only
		_rasterizer.opaque = _rasterizer.uniform.material->opaque;

		_rasterizer.depthTestMode = DEPTH_TEST_LESS_OR_EQUAL;
		if (_rasterizer.uniform.material->useBackfaceCulling)
		{
			_rasterizer.cullClockwise = true;
			_rasterizer.drawIndexedTriangles(_model, vertices, indices, count, offset);
		}
		else
		{
			_rasterizer.cullClockwise = false;
			_rasterizer.depthTestMode = DEPTH_TEST_LESS;
			_rasterizer.drawIndexedTriangles(_model, vertices, indices, count, offset);
			_rasterizer.cullClockwise = true;
			_rasterizer.drawIndexedTriangles(_model, vertices, indices, count, offset);
		}
		_rasterizer.uniform.material = nullptr;
		offset += count;
	}
}
