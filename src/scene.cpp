/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "scene.h"
#include "vectormath.h"

#include <limits>

void Action::animate(Bindable *_owner, float _time)
{
	if ((end - start) == 0.0F)
	{
		start = std::numeric_limits<float>::max();
		end = std::numeric_limits<float>::min();

		for (auto &fcurve : curves)
		{
			if (fcurve.keyframes.front().value.x < start)
				start = fcurve.keyframes.front().value.x;
			if (fcurve.keyframes.back().value.x > end)
				end = fcurve.keyframes.back().value.x;
		}
	}

	float time = _time - start;
	float duration = end - start;

	if (time < 0.0F)
	{
		return;
	}

	if (time > duration)
	{
		if (cyclic)
		{
			time = fmod(time, duration);
		}
		else
		{
			time = duration;
		}
	}

	for (auto &fcurve : curves)
	{
		fcurve.animate(_owner, time);
	}
}

void FCurve::animate(Bindable *_owner, float _time)
{
	auto set = _owner->resolve(datapath);
	if (!set)
	{
		throw NotResolvableException();
	}

	// binary search for current sampler input
	unsigned int low = 0;
	unsigned int high = keyframes.size() - 1;
	unsigned int i = 0;
	while (low <= high)
	{
		i = (low + high) / 2;
		if (_time < keyframes[i].value.x && i > 0)
		{
			high = i - 1;
		}
		else if (_time > keyframes[i + 1].value.x && i < high)
		{
			low = i + 1;
		}
		else
		{
			break;
		}
	}

	switch (keyframes[i].type)
	{
	case KEYFRAME_NONE:
		break;
	case KEYFRAME_CONSTANT: {
		float t = (_time - keyframes[i].value.x) / (keyframes[i + 1].value.x - keyframes[i].value.x);
		set((t < 0.5f) ? keyframes[i].value.y : keyframes[i + 1].value.y);
	}
	break;
	case KEYFRAME_LINEAR: {
		float t = (_time - keyframes[i].value.x) / (keyframes[i + 1].value.x - keyframes[i].value.x);
		auto val = lerp(keyframes[i].value.y, keyframes[i + 1].value.y, t);
		set(val);
	}
	break;
	case KEYFRAME_BEZIER: {
		/*
			beziersplines can be expressed in matrix notation with the coefficients of the hermite polynom like this:
														⎧ 2 -3  0  1 ⎫⎧ p₀ ⎫	where p are the positions and t the tangents
				P(t) = [t³ t² t 1]  ⎪-2  3  0  0 ⎪⎪ p₁ ⎪
														⎪ 1 -2  1  0 ⎪⎪ t₀ ⎪
														⎩ 1 -1  0  0 ⎭⎩ t₁ ⎭

				this can surely be optimized by inlining (lots of zeros and ones) but this is more readable.
		*/

		int j = i + 1;
		float t = bezier_decasteljau(_time, keyframes[i].value.x, keyframes[i].right.x, keyframes[j].left.x, keyframes[j].value.x, 10);

		M4 hermite{{2, -3, 0, 1}, {-2, 3, 0, 0}, {1, -2, 1, 0}, {1, -1, 0, 0}};
		V4 T{t * t * t, t * t, t, 1};
		V4 P{keyframes[i].value.y, keyframes[j].value.y, keyframes[i].right.y, keyframes[j].left.y};
		set(T * (hermite * P));
	}
	break;
	}
}
