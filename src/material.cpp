/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "material.h"

auto Material::resolve(const std::string &_datapath) -> setter
{
	auto binding = bindings.get(_datapath);
	if (!binding)
	{
		if (_datapath == "diffuse.X")
			binding = {[this](float v) { diffuse.x = v; }};
		else if (_datapath == "diffuse.Y")
			binding = {[this](float v) { diffuse.y = v; }};
		else if (_datapath == "diffuse.Z")
			binding = {[this](float v) { diffuse.z = v; }};
		else if (_datapath == "diffuse.W")
			binding = {[this](float v) { diffuse.w = v; }};
		else if (_datapath == "alpha.X")
			binding = {[this](float v) { alpha = v; }};
		if (binding)
		{
			bindings.set(_datapath, binding);
		}
	}
	return binding;
}
