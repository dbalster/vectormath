/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef SCENE_H
#define SCENE_H

#include <chrono>
#include <cstdint>
#include <list>
#include <string>

#include "pool.h"

#include "asset.h"
#include "color.h"
#include "vectormath.h"
#include <map>

class NotResolvableException : public std::exception
{
};

template <typename TYPE> class Bindings
{
public:
	typedef std::function<void(TYPE)> setter_type;
	typedef std::function<TYPE()> getter_type;

public:
	auto has(const std::string &_path) -> bool
	{
		return bindings.end() != bindings.find(_path);
	}

	auto get(const std::string &_path) -> setter_type
	{
		return bindings[_path];
	}

	auto set(const std::string &_path, setter_type _binding) -> void
	{
		bindings[_path] = _binding;
	}

private:
	std::map<std::string, setter_type> bindings;
};

class Bindable
{
protected:
	Bindings<float> bindings;

public:
	typedef typename Bindings<float>::setter_type setter;
	// somehow this should be cached
	virtual auto resolve(const std::string &_datapath) -> setter = 0;
};

enum keyframe_type
{
	KEYFRAME_NONE,
	KEYFRAME_CONSTANT, // full steps
	KEYFRAME_LINEAR,
	KEYFRAME_BEZIER,
};
struct Keyframe
{
	keyframe_type type;
	V2 value;
	V2 left;
	V2 right;
};

class FCurve
{
public:
	std::string datapath; // ie "rotation_euler.X"
	std::vector<Keyframe> keyframes;
	void animate(Bindable *_owner, float _elapsedTimeInSeconds) noexcept(false);
};

class Action : public Pool<Action>
{
public:
	std::string name;
	std::vector<FCurve> curves;

	float start; // start time in seconds
	float end;	 // end time in seconds
	bool cyclic;
	// EasingFunctionType ease;
	//  TODO: markers? groups? channels?

	// requires an owning object
	void animate(Bindable *_owner, float _elapsedTimeInSeconds) noexcept(false);
};

#endif
