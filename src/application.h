/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include <vector>

#include "node.h"
#include "rasterizer.h"

class Display;
class Terminal;

class Application
{
	static Application *self;
	static void signal_handler(const int sig);

public:
	typedef Application base;
	Application(const Application &) = delete;
	Application(Node &&) = delete;
	Application &operator=(const Application &) = delete;
	Application &operator=(Application &&) = delete;

	Application();
	virtual ~Application();

	virtual int run();

protected:
	std::vector<Node::CullContext> culledObjects;
	std::shared_ptr<Terminal> terminal;
	std::shared_ptr<Display> display;

	std::shared_ptr<Node> scene;
	std::weak_ptr<Node> camera; // current camera (node, asset is camera data)
	Rasterizer rasterizer;
	float elapsedTimeInSeconds{0.0f};
	float animationSpeedInFramesPerSecond{24.0f};
	bool running{false};

	virtual void setScene(std::shared_ptr<Node> _scene);
	virtual void init();
	virtual void exit();
	virtual void update();
	virtual void preprocess();
	virtual void render();
	virtual void postprocess();
	virtual void sizeChanged(int _width, int _height);
	virtual void onMouse(int _x, int _y, int _button);
	virtual void onKey(int _key);
};

#endif
