#include "spline.h"
#include "vectormath.h"
#include <algorithm>

// https://en.wikipedia.org/wiki/B%C3%A9zier_curve
// https://en.wikipedia.org/wiki/De_Casteljau%27s_algorithm

// split split split until epsilon stops it

template <typename T> auto recursive_length_with_decasteljau(T p0, T c0, T c1, T p1, float epsilon) -> float
{
	float controlnetlen = distance(p0, c0) + distance(c0, c1) + distance(c1, p1);
	float chordlen = distance(p0, p1);

	if (controlnetlen - chordlen < epsilon)
	{
		return chordlen;
	}
	else
	{
		V3 mid_c0 = lerp(p0, c0, 0.5f);
		V3 mid_c1 = lerp(c0, c1, 0.5f);
		V3 mid_c2 = lerp(c1, p1, 0.5f);

		V3 mid_c0_c1 = lerp(mid_c0, mid_c1, 0.5f);
		V3 mid_c1_c2 = lerp(mid_c1, mid_c2, 0.5f);

		V3 new_p0 = p0;
		V3 new_c0 = mid_c0;
		V3 new_c1 = mid_c0_c1;
		V3 new_p1 = lerp(mid_c0_c1, mid_c1_c2, 0.5f);

		float length1 = recursive_length_with_decasteljau(new_p0, new_c0, new_c1, new_p1, epsilon);
		V3 new_p0_2 = new_p1;
		V3 new_c0_2 = mid_c1_c2;
		V3 new_c1_2 = mid_c2;
		V3 new_p1_2 = p1;
		float length2 = recursive_length_with_decasteljau(new_p0_2, new_c0_2, new_c1_2, new_p1_2, epsilon);

		return length1 + length2;
	}
}

void Spline::add(const V3 &p, const V3 &left, const V3 &right)
{
	BezierPoint bp;
	bp.value = p;
	bp.left = left;
	bp.right = right;
	bp.offset = 0.0f;
	points.push_back(bp);

	float offset = 0.0f;
	for (size_t i = 0; i < points.size() - 1; ++i)
	{
		auto length = recursive_length_with_decasteljau<V3>(points[i].value, points[i].right, points[i + 1].left, points[i + 1].value, 0.1f);
		points[i].length = length;
		points[i].offset = offset;
		offset += length;
	}
	points.back().length = 0;
	points.back().offset = offset;
}

void Spline::clear()
{
	points.clear();
}

auto Spline::length() const -> float
{
	float length = 0.0f;

	for (size_t i = 0; i < points.size() - 1; ++i)
	{
		length += recursive_length_with_decasteljau<V3>(points[i].value, points[i].right, points[i + 1].left, points[i + 1].value, 0.1f);
	}

	return length;
}

// interpolate between t=[ 0,length() )
auto Spline::interpolate(float _t) const -> V3
{
	// binary search for current sampler input
	size_t low = 0;
	size_t high = points.size() - 1;
	size_t i = 0;
	while (low <= high)
	{
		i = (low + high) / 2;
		if (_t < points[i].offset && i > 0)
		{
			high = i - 1;
		}
		else if (_t > points[i + 1].offset && i < high)
		{
			low = i + 1;
		}
		else
		{
			break;
		}
	}

	float t = (_t - points[i].offset) / points[i].length;

	const V3 &p0 = points[i].value;
	const V3 &c0 = points[i].right;
	const V3 &c1 = points[i + 1].left;
	const V3 &p1 = points[i + 1].value;

#if 1
	// solve hermite polynom

	float s = (1.0f - t);
	float s2 = s * s;
	float s3 = s2 * s;
	float t2 = t * t;
	float t3 = t2 * t;
	return p0 * s3 /* * t0=1 */ + c0 * 3.0f * t * s2 + c1 * 3.0f * t2 * s + p1 * t3 /* * s0=1 */;

#else
	// or "trilinear" bezier subdivision

	V3 mid_c0 = lerp(p0, c0, t);
	V3 mid_c1 = lerp(c0, c1, t);
	V3 mid_c2 = lerp(c1, p1, t);

	V3 mid_c0_c1 = lerp(mid_c0, mid_c1, t);
	V3 mid_c1_c2 = lerp(mid_c1, mid_c2, t);

	return lerp(mid_c0_c1, mid_c1_c2, t);
#endif
}
