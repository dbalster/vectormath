/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "flatimporter.h"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

#include "commodities.h"
#include "scene.h"

#include "camera.h"
#include "curve.h"
#include "light.h"
#include "material.h"
#include "mesh.h"
#include "node.h"
#include "texture.h"
#include "transform.h"

using std::string;

namespace fs = std::filesystem;

string ensureExists(const string &path)
{
	for (const auto &entry : fs::recursive_directory_iterator(fs::current_path()))
	{
		if (entry.is_regular_file() && entry.path().filename() == fs::path(path).filename())
		{
			return entry.path().string();
		}
	}
	return path;
}

void FlatImporter::parseFCurve(std::istream &file, FCurve &fcurve)
{
	string word;
	string line;

	while ((file >> word))
	{
		if (word == "endfcurve")
		{
			break;
		}
		else if (word == "datapath")
		{
			getline(file, fcurve.datapath);
			trim(fcurve.datapath);
		}
		else if (word == "keyframe")
		{
			std::string type;
			V2 value, left, right;
			if (file >> type >> value.x >> value.y >> left.x >> left.y >> right.x >> right.y)
			{
				Keyframe k;
				k.value = value;
				k.left = left;
				k.right = right;
				if (type == "BEZIER")
					k.type = KEYFRAME_BEZIER;
				else if (type == "CONSTANT")
					k.type = KEYFRAME_CONSTANT;
				else if (type == "LINEAR")
					k.type = KEYFRAME_LINEAR;
				fcurve.keyframes.emplace_back(k);
			}
		}
	}
}

void FlatImporter::parseFCurves(std::istream &file, std::shared_ptr<Action> _action)
{
	string word;
	while ((file >> word))
	{
		if (word == "endfcurves")
			break;
		else if (word == "start")
			file >> _action->start;
		else if (word == "end")
			file >> _action->end;
		else if (word == "fcurve")
		{
			FCurve c;
			parseFCurve(file, c);
			_action->curves.emplace_back(c);
		}
	}
}

void FlatImporter::parseAction(std::istream &file)
{
	string word;
	string line;
	std::shared_ptr<Action> action = std::make_shared<Action>();
	while ((file >> word))
	{
		if (word == "endaction")
			break;
		else if (word == "fcurves")
			parseFCurves(file, action);
		else if (word == "start")
			file >> action->start;
		else if (word == "end")
			file >> action->end;
		else if (word == "cyclic")
		{
			file >> word;
			action->cyclic = word == "True";
		}
		else if (word == "name")
		{
			getline(file, word); // name!
			trim(word);
			action->name = word;
			Action::add(action->name, action);
		}
	}
}

void FlatImporter::parseActions(std::istream &file)
{
	string word;
	while ((file >> word))
	{
		if (word == "endactions")
			break;
		else if (word == "action")
			parseAction(file);
	}
}

void FlatImporter::parseSpline(std::istream &file, Spline &spline)
{
	string word;
	string line;

	while ((file >> word))
	{
		if (word == "endspline")
			break;
		else if (word == "type")
		{
			getline(file, word); // name!
			trim(word);
		}
		else if (word == "bezier")
		{
			V3 p, l, r;
			if (file >> p.x >> p.y >> p.z >> l.x >> l.y >> l.z >> r.x >> r.y >> r.z)
			{
				spline.add(p, l, r);
			}
		}
	}
}

void FlatImporter::parseSplines(std::istream &file, std::shared_ptr<Curve> curve)
{
	string word;
	while ((file >> word))
	{
		if (word == "endsplines")
			break;
		else if (word == "spline")
		{
			Spline spline;
			parseSpline(file, spline);
			curve->splines.emplace_back(spline);
		}
	}
}

void FlatImporter::parseCurve(std::istream &file)
{
	string word;
	string line;
	std::shared_ptr<Curve> curve;

	while ((file >> word))
	{
		if (word == "endcurve")
			break;
		else if (word == "name")
		{
			getline(file, word); // name!
			trim(word);
			curve = std::make_shared<Curve>();
			curve->name = word;
			Curve::add(word, curve);
		}
		else if (word == "splines")
			parseSplines(file, curve);
	}
}

void FlatImporter::parseMaterial(std::istream &file)
{
	string word;
	string line;

	V4 diffuse;
	std::string name;
	std::shared_ptr<Action> action;
	std::shared_ptr<Texture2D> texture;
	bool useBackfaceCulling = false;
	bool opaque = true;
	float alpha;

	while ((file >> word))
	{
		if (word == "endmaterial")
		{
			std::shared_ptr<Material> material;

			material = std::make_shared<Material>();
			material->name = name;
			material->action = action;
			material->texture = texture;
			material->useBackfaceCulling = useBackfaceCulling;
			material->opaque = opaque;
			material->alpha = alpha;
			material->diffuse = diffuse;
			Material::add(name, material);
			break;
		}
		else if (word == "action")
		{
			getline(file, line);
			trim(line);
			action = Action::find(line);
		}
		else if (word == "name")
		{
			getline(file, name);
			trim(name);
		}
		else if (word == "image")
		{
			if (getline(file, line))
			{
				trim(line);

				string path = ensureExists(line);
				texture = Texture2D::find(path);
				if (texture == nullptr)
				{
					texture = std::make_shared<Texture2D>();
					Texture2D::add(path, texture);
					texture->load(path);
				}
			}
		}
		else if (word == "use_backface_culling")
		{
			file >> word;
			useBackfaceCulling = word == "True";
		}
		else if (word == "blend_method")
		{
			file >> word;
			if (word == "OPAQUE")
			{
				opaque = true;
			}
			else if (word == "BLEND")
			{
				opaque = false;
			}
		}
		else if (word == "alpha")
		{
			file >> alpha;
		}
		else if (word == "diffuse")
		{
			if (file >> diffuse.x >> diffuse.y >> diffuse.z >> diffuse.w)
			{
			}
		}
	}
}

void FlatImporter::parseMesh(std::istream &file)
{
	string word;
	string line;
	std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();

	while ((file >> word))
	{
		if (word == "endmesh")
		{
			break;
		}
		else if (word == "action")
		{
			getline(file, line);
			trim(line);
			mesh->action = Action::find(line);
		}
		else if (word == "name")
		{
			getline(file, mesh->name);
			trim(mesh->name);
			Asset::add(mesh->name, mesh);
		}
		else if (word == "smooth")
		{
			file >> word;
			mesh->smooth = word == "True";
		}
		else if (word == "vertexcolors")
		{
			file >> word;
			mesh->vertexcolors = word == "True";
		}
		else if (word == "material")
		{
			getline(file, line);
			trim(line);
			auto material = Material::find(line);
			mesh->materials.push_back(material);
		}
		else if (word == "vertices")
		{
			if (getline(file, line))
			{
				std::istringstream tokenizer(line);
				while (tokenizer)
				{
					Vertex v;
					V4 color;
					if (tokenizer >> v.position.x >> v.position.y >> v.position.z >> v.normal.x >> v.normal.y >> v.normal.z >> color.x >> color.y >> color.z >>
						color.w >> v.uv.x >> v.uv.y)
					{
						v.normal.normalize();
						v.color.setf(color.x, color.y, color.z, color.w);
						mesh->vertices.push_back(v);
					}
				}
				mesh->aabb.build(mesh->vertices, [](const Vertex &v) { return v.position; });
			}
		}
		else if (word == "indices")
		{
			if (getline(file, line))
			{
				std::istringstream tokenizer(line);
				while (tokenizer)
				{
					int index;
					if (tokenizer >> index)
					{
						mesh->indices.push_back(index);
					}
				}
			}
		}
		else if (word == "batches")
		{
			if (getline(file, line))
			{
				std::istringstream tokenizer(line);
				while (tokenizer)
				{
					int count;
					if (tokenizer >> count)
					{
						mesh->batches.push_back(count);
					}
				}
			}
		}
	}
}

void FlatImporter::parseCamera(std::istream &file)
{
	string word;
	string line;
	std::shared_ptr<Camera> camera = std::make_shared<Camera>();

	while ((file >> word))
	{
		if (word == "endcamera")
			break;
		else if (word == "action")
		{
			getline(file, line);
			trim(line);
			camera->action = Action::find(line);
		}
		else if (word == "near")
			file >> camera->near;
		else if (word == "far")
			file >> camera->far;
		else if (word == "aspect")
			file >> camera->aspectRatio;
		else if (word == "yfov")
			file >> camera->yFov;
		else if (word == "name")
		{
			getline(file, camera->name);
			trim(camera->name);
			Asset::add(camera->name, camera);
		}
	}
}

void FlatImporter::parseMaterials(std::istream &file)
{
	string word;
	while ((file >> word))
	{
		if (word == "endmaterials")
			break;
		else if (word == "material")
			parseMaterial(file);
	}
}

void FlatImporter::parseAssets(std::istream &file)
{
	string word;
	while ((file >> word))
	{
		if (word == "endassets")
			break;
		else if (word == "mesh")
			parseMesh(file);
		else if (word == "curve")
			parseCurve(file);
		else if (word == "camera")
			parseCamera(file);
	}
}

void FlatImporter::parseModifiers(std::istream &file, std::shared_ptr<Node> node)
{
	string word;
	Node::Modifier modifier;
	bool push = false;
	while ((file >> word))
	{
		if (word == "endmodifiers")
			break;
		else if (word == "type")
		{
			if (push)
				node->modifiers.push_back(modifier);
			modifier = Node::Modifier();
			file >> word;
			push = true;
		}
		else if (word == "count")
		{
			file >> modifier.count;
		}
		else if (word == "relative_offset")
		{
			file >> modifier.relative.x >> modifier.relative.y >> modifier.relative.z;
			modifier.use_relative = true;
		}
		else if (word == "constant_offset")
		{
			file >> modifier.constant.x >> modifier.constant.y >> modifier.constant.z;
			modifier.use_constant = true;
		}
		else if (word == "object_offset")
		{
			getline(file, modifier.offset_object);
			trim(modifier.offset_object);
		}
	}
	if (push)
		node->modifiers.push_back(modifier);
}

void FlatImporter::parseNode(std::shared_ptr<Node> parent, std::istream &file)
{
	string word;
	string dummy;
	std::shared_ptr<Node> object = std::make_shared<Node>();
	parent->add(object);

	while ((file >> word))
	{
		if (word == "endnode")
			break;
		else if (word == "name")
		{
			getline(file, object->name);
			trim(object->name);
		}
		else if (word == "uncullable")
		{
			file >> word;
			object->uncullable = word == "True";
		}
		else if (word == "hidden")
		{
			object->hidden = true;
		}
		else if (word == "modifiers")
		{
			parseModifiers(file, object);
		}
		else if (word == "action")
		{
			getline(file, dummy);
			trim(dummy);
			object->action = Action::find(dummy);
		}
		else if (word == "asset")
		{
			getline(file, dummy);
			trim(dummy);
			object->asset = Asset::find(dummy);
		}
		else if (word == "rotate")
		{
			float a, b, c;
			file >> word; // mode: XYZ
			file >> a >> b >> c;

			Transform *trfm = new RotateEulerTransform("rotation_euler", a, b, c);
			trfm->next = object->transforms;
			object->transforms = trfm;
		}
		else if (word == "location")
		{
			float x, y, z;
			file >> x >> y >> z;

			Transform *trfm = new TranslateTransform("location", x, y, z);
			trfm->next = object->transforms;
			object->transforms = trfm;
		}
		else if (word == "scale")
		{
			float x, y, z;
			file >> x >> y >> z;

			Transform *trfm = new ScaleTransform("scale", x, y, z);
			trfm->next = object->transforms;
			object->transforms = trfm;
		}
		else if (word == "children")
		{
			while ((file >> word))
			{
				if (word == "node")
					parseNode(object, file);
				else if (word == "endchildren")
					break;
			}
		}
	}
}

std::shared_ptr<Node> FlatImporter::load(const std::string &filename)
{
	std::shared_ptr<Node> scene = std::make_shared<Node>();

	std::ifstream file(filename);
	string word;

	getline(file, word);

	while ((file >> word))
	{
		if (word == "actions")
			parseActions(file);
		else if (word == "assets")
			parseAssets(file);
		else if (word == "materials")
			parseMaterials(file);
		else if (word == "fps")
		{
			file >> fps;
		}
		else if (word == "scene")
		{
		}
		else if (word == "name")
		{
			getline(file, scene->name);
			trim(scene->name);
		}
		else if (word == "node")
			parseNode(scene, file);
		else if (word == "endscene")
			break;
	}

	return scene;
}
