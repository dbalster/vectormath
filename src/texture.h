/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef TEXTURE_H
#define TEXTURE_H

#include "pool.h"

#include <algorithm>
#include <string>
//#include <bits>
#include "color.h"
#include <cmath>

enum sample_mode
{
	SAMPLE_CONSTANT,
	SAMPLE_LINEAR,
};

class Texture2D : public Pool<Texture2D>
{
	rgba32_t *data{nullptr};
	int width{0};
	int height{0};
	int channels{0};

public:
	Texture2D()
	{
	}
	~Texture2D();

	sample_mode sampleMode{SAMPLE_CONSTANT};

	Texture2D(const Texture2D &) = delete;
	Texture2D(Texture2D &&) = delete;
	auto operator=(const Texture2D &) const -> Texture2D & = delete;
	auto operator=(Texture2D &&) -> Texture2D & = delete;

	void load(const std::string &_filename);
	inline auto modulo(const float &x) -> float
	{
		return std::clamp(x - std::floor(x), 0.0F, 1.0F);
	}

	auto sample(float _u, float _v) -> Color;
};

#endif
