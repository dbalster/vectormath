/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef COLOR_H
#define COLOR_H

#include "vectormath.h"
#include <algorithm>
#include <cstdint>

struct r8g8b8a8_t
{
	uint32_t red : 8;
	uint32_t green : 8;
	uint32_t blue : 8;
	uint32_t alpha : 8;
};

struct r8g8b8_t
{
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};

struct a8r8g8b8_t
{
	uint32_t alpha : 8;
	uint32_t red : 8;
	uint32_t green : 8;
	uint32_t blue : 8;
};
struct a8b8g8r8_t
{
	uint32_t alpha : 8;
	uint32_t blue : 8;
	uint32_t green : 8;
	uint32_t red : 8;
};

struct b8g8r8a8_t
{
	uint32_t blue : 8;
	uint32_t green : 8;
	uint32_t red : 8;
	uint32_t alpha : 8;
};

union rgba32_t {
	r8g8b8a8_t r8g8b8a8;
	a8r8g8b8_t a8r8g8b8;
	a8b8g8r8_t a8b8g8r8;
	b8g8r8a8_t b8g8r8a8;
	uint32_t value;
};

class Color
{
public:
	float red{0.0F};
	float green{0.0F};
	float blue{0.0F};
	float alpha{1.0F};

	// using constants for repeated values increases readability and may aid optimization
	static constexpr float TO_UINT8 = 255.0f;
	static constexpr float TO_FLOAT = 1.0f / TO_UINT8;

	// maybe better to do std::experimental::simd

	operator r8g8b8_t() const
	{
		return r8g8b8_t{uint8_t(red * TO_UINT8), uint8_t(green * TO_UINT8), uint8_t(blue * TO_UINT8)};
	}

	operator r8g8b8a8_t() const
	{
		return r8g8b8a8_t{uint8_t(red * TO_UINT8), uint8_t(green * TO_UINT8), uint8_t(blue * TO_UINT8), uint8_t(alpha * TO_UINT8)};
	}
	operator a8r8g8b8_t() const
	{
		return a8r8g8b8_t{uint8_t(alpha * TO_UINT8), uint8_t(red * TO_UINT8), uint8_t(green * TO_UINT8), uint8_t(blue * TO_UINT8)};
	}
	operator a8b8g8r8_t() const
	{
		return a8b8g8r8_t{uint8_t(alpha * TO_UINT8), uint8_t(blue * TO_UINT8), uint8_t(green * TO_UINT8), uint8_t(red * TO_UINT8)};
	}
	operator b8g8r8a8_t() const
	{
		return b8g8r8a8_t{uint8_t(blue * TO_UINT8), uint8_t(green * TO_UINT8), uint8_t(red * TO_UINT8), uint8_t(alpha * TO_UINT8)};
	}

	Color() = default;

	Color(float _red, float _green, float _blue, float _alpha)
	  : red(_red)
	  , green(_green)
	  , blue(_blue)
	  , alpha(_alpha)
	{
	}

	Color(const r8g8b8a8_t &_color)
	  : red(_color.red * TO_FLOAT)
	  , green(_color.green * TO_FLOAT)
	  , blue(_color.blue * TO_FLOAT)
	  , alpha(_color.alpha * TO_FLOAT)
	{
	}
	Color(const a8r8g8b8_t &_color)
	  : red(_color.red * TO_FLOAT)
	  , green(_color.green * TO_FLOAT)
	  , blue(_color.blue * TO_FLOAT)
	  , alpha(_color.alpha * TO_FLOAT)
	{
	}
	Color(const a8b8g8r8_t &_color)
	  : red(_color.red * TO_FLOAT)
	  , green(_color.green * TO_FLOAT)
	  , blue(_color.blue * TO_FLOAT)
	  , alpha(_color.alpha * TO_FLOAT)
	{
	}
	Color(const b8g8r8a8_t &_color)
	  : red(_color.red * TO_FLOAT)
	  , green(_color.green * TO_FLOAT)
	  , blue(_color.blue * TO_FLOAT)
	  , alpha(_color.alpha * TO_FLOAT)
	{
	}

	void setf(float _r, float _g, float _b, float _a)
	{
		red = _r;
		green = _g;
		blue = _b;
		alpha = _a;
	}

	explicit operator V4() const
	{
		return V4(red, green, blue, alpha);
	}

	// multiplication operator could potentially benefit from being inlined
	inline Color operator*(float _f) const
	{
		return Color(red * _f, green * _f, blue * _f, alpha * _f);
	}

	inline Color operator*(const V4 &_v) const
	{
		return Color(red * _v.x, green * _v.y, blue * _v.y, alpha * _v.w);
	}

	static Color fromHSB(float hue, float saturation, float brightness)
	{
		saturation = std::clamp(saturation, 0.0f, 1.0f);
		brightness = std::clamp(brightness, 0.0f, 1.0f);

		float r = 0, g = 0, b = 0;
		if (saturation == 0.0f)
		{
			r = g = b = brightness;
		}
		else
		{
			float sector = hue / 60.0f;
			int i = static_cast<int>(floor(sector));
			float f = sector - i;
			float p = brightness * (1 - saturation);
			float q = brightness * (1 - saturation * f);
			float t = brightness * (1 - saturation * (1 - f));

			switch (i)
			{
			case 0:
				r = brightness;
				g = t;
				b = p;
				break;
			case 1:
				r = q;
				g = brightness;
				b = p;
				break;
			case 2:
				r = p;
				g = brightness;
				b = t;
				break;
			case 3:
				r = p;
				g = q;
				b = brightness;
				break;
			case 4:
				r = t;
				g = p;
				b = brightness;
				break;
			default: // case 5:
				r = brightness;
				g = p;
				b = q;
				break;
			}
		}

		return Color(r, g, b, 1.0f);
	}
};

#endif // COLOR_H
