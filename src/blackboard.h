#ifndef BLACKBOARD_H
#define BLACKBOARD_H

#include <any>
#include <memory>
#include <string>
#include <unordered_map>

//#include <nlohmann/json.hpp>
// using namespace nlohmann;

// simple blackboard
/*

	Blackboard bb;

	bb[this]["x"] = 1;
	bb[0]["y"] = true;
	bb["hall"]["z"] = 1.0;
	bb[nullptr]["z"] = 1.0;

*/

class Blackboard
{
public:
	class Scope
	{
	public:
		typedef std::unordered_map<std::string, std::any> container_type;
		container_type variables;

		inline bool contains(const std::string &_key) const
		{
			return variables.contains(_key);
		}

		std::any &operator[](const std::string &_key)
		{
			auto iter = variables.find(_key);
			if (iter != variables.end())
			{
				return iter->second;
			}
			auto [result, success] = variables.emplace(std::make_pair(_key, std::any()));
			return result->second;
		}
	};

private:
	typedef std::unordered_map<uint64_t, Scope> container_type;
	container_type scopes;

public:
	inline bool contains(uint64_t _scope) const
	{
		return scopes.contains(_scope);
	}

	Scope &operator[](uint64_t _scope)
	{
		auto iter = scopes.find(_scope);
		if (iter != scopes.end())
		{
			return iter->second;
		}
		auto [result, success] = scopes.emplace(std::make_pair(_scope, Scope()));
		return result->second;
	}
};

// once allocated, use the memory address as unique hash code
// hence, copies needs to be prevented

class Hashed
{
public:
	Hashed()
	  : hash(std::hash<void *>()(this))
	{
	}
	Hashed(const Hashed &_other) = delete;
	Hashed &operator=(const Hashed &_other) = delete;

	const size_t hash;
};

class ContextWithBlackboard : public Hashed
{
protected:
	std::shared_ptr<Blackboard> blackboard;

public:
	ContextWithBlackboard(std::shared_ptr<Blackboard> _blackboard)
	  : Hashed()
	  , blackboard(_blackboard)
	{
	}

	inline bool contains(size_t _hash) const
	{
		return blackboard->contains(uint64_t(hash << 32) | _hash);
	}

	inline Blackboard::Scope &operator[](size_t _hash)
	{
		return (*blackboard)[uint64_t(hash << 32) | _hash];
	}
};

#endif
