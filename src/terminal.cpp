/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "terminal.h"

#include <cstdarg>
#include <cstdio>
#include <stdio.h>
#if defined(HAS_TERMIOS)
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#else
#include <io.h>
#include <windows.h>
#endif
#include <string.h>

using namespace std;
using namespace std::chrono;
using namespace std::chrono_literals;

Terminal::Terminal(size_t _size)
  : width(0)
  , height(0)
{
	for (int i = 0; i < 6; ++i)
		last_time_point[i] = std::chrono::steady_clock::now();

	size = _size;
	buffer.resize(_size);
	offset = 0;
}

Terminal::~Terminal()
{
}

size_t Terminal::write(const char *_s)
{
	size_t len = 0;
	while (_s[len] != 0)
		++len;
	write(_s, len);
	return len;
}

size_t Terminal::write(const char *_s, size_t _len)
{
	if ((offset + _len) >= size)
		flush();

	for (size_t i = 0; i < _len; ++i)
	{
		buffer[offset] = _s[i];
		offset++;
	}
	buffer[offset] = 0;
	return _len;
}

size_t Terminal::printf(const char *_fmt, ...)
{
	char buf[256];
	va_list ap;
	va_start(ap, _fmt);
	unsigned int len = vsprintf(buf, _fmt, ap);
	va_end(ap);
	write(buf, len);
	return len;
}

void Terminal::flush()
{
	if (offset > 0)
	{
#if defined(HAS_TERMIOS)
		::write(STDOUT_FILENO, buffer.data(), offset);
#else
		_write(1, buffer.data(), offset);
		_flushall();
#endif
	}
	offset = 0;
}

void Terminal::setCursor(int _x, int _y)
{
	printf("\x1b[%d;%dH", _y + 1, _x + 1);
}

void Terminal::setForeColor(uint8_t _r, uint8_t _g, uint8_t _b)
{
	printf("\x1b[38;2;%d;%d;%dm", _r, _g, _b);
}

void Terminal::setForeColor(const std::string &_)
{
	auto p = _.find('#');
	if (p == std::string::npos)
		return;
	std::string s = _.substr(p + 1);
	int color = std::stoi(s, nullptr, 16);
	setForeColor(color >> 16, color >> 8, color);
}

void Terminal::setBackColor(const std::string &_)
{
	auto p = _.find('#');
	if (p == std::string::npos)
		return;
	std::string s = _.substr(p + 1);
	int color = std::stoi(s, nullptr, 16);
	setBackColor(color >> 16, color >> 8, color);
}

void Terminal::setBackColor(uint8_t _r, uint8_t _g, uint8_t _b)
{
	printf("\x1b[48;2;%d;%d;%dm", _r, _g, _b);
}

void Terminal::hideCursor()
{
	write("\x1b[?25l");
}

void Terminal::showCursor()
{
	write("\x1b[?25h");
}

void Terminal::altScreen()
{
	write("\x1b[?1049h");
}

void Terminal::normalScreen()
{
	write("\x1b[?1049l");
}

void Terminal::clear()
{
	write("\x1b[2J\x1b[0;0f");
}

void Terminal::mouseOn()
{
	write("\x1b[?1002h\x1b[?1015h\x1b[?1006h");
}

void Terminal::mouseOff()
{
	write("\x1b[?1002l\x1b[?1015l\x1b[?1006l");
}

void Terminal::reset()
{
	write("\x1b[0m");
}

void Terminal::setTitle(const char *_s)
{
	printf("\x1b]0;%s\007", _s);
}

void Terminal::bold()
{
}

bool Terminal::setEcho(bool _on)
{
#if defined(HAS_TERMIOS)
	struct termios settings;
	if (tcgetattr(STDIN_FILENO, &settings))
		return false;
	if (_on)
		settings.c_lflag |= ECHO;
	else
		settings.c_lflag &= ~(ECHO);
	return 0 == tcsetattr(STDIN_FILENO, TCSANOW, &settings);
#else
	return false;
#endif
}

bool Terminal::linebuffered(bool _on)
{
#if defined(HAS_TERMIOS)
	struct termios settings;
	if (tcgetattr(STDIN_FILENO, &settings))
		return false;
	if (_on)
		settings.c_lflag |= ICANON;
	else
		settings.c_lflag &= ~(ICANON);
	if (tcsetattr(STDIN_FILENO, TCSANOW, &settings))
		return false;
	if (_on)
		setlinebuf(stdin);
	else
		setbuf(stdin, NULL);
#endif
	return true;
}

void Terminal::init()
{
#if !defined(HAS_TERMIOS)
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);

	DWORD fdwMode = ENABLE_EXTENDED_FLAGS | ENABLE_WINDOW_INPUT | ENABLE_PROCESSED_INPUT | ENABLE_VIRTUAL_TERMINAL_INPUT; // | ENABLE_MOUSE_INPUT;
	SetConsoleMode(hStdin, fdwMode);
#else
	// fcntl(0, F_SETFL, O_NONBLOCK);
#endif
}

int Terminal::read(int _timeoutMs)
{
#if defined(HAS_TERMIOS)
	isMouse = false;
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(STDIN_FILENO, &fds);
	timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 1000 * _timeoutMs; // 1000µs or 1ms

	int result = select(1, &fds, nullptr, nullptr, &tv);
	// timeout or error or interrupt (EINTR)
	if (result <= 0)
		return -1;
	if (!FD_ISSET(STDIN_FILENO, &fds))
		return -1;
	int len = 0;
	ioctl(STDIN_FILENO, FIONREAD, &len);
	static char buffer[64];
	if (len > 64)
		return -1;
	if (len != ::read(STDIN_FILENO, buffer, len))
		return -1;
	if (buffer[0] != 27 && len == 1)
		return buffer[0];

	if (buffer[0] == 27 && buffer[1] == '[') // CSI
	{
		char *token;
		char *save;
		sequence.assign(buffer + 2, len - 2);
		// std::cerr << buffer+2 << std::endl;

		// parse mouse
		if (buffer[2] == '<')
		{
			int button = -1;
			// button;row;colm or M
			token = strtok_r(buffer + 3, ";", &save);
			if (token)
			{
				if (0 == strcmp(token, "0"))
					button = MOUSE_0_DOWN;
				else if (0 == strcmp(token, "1"))
					button = MOUSE_1_DOWN;
				else if (0 == strcmp(token, "2"))
					button = MOUSE_2_DOWN;
				else if (0 == strcmp(token, "32"))
					button = MOUSE_0_MOVE;
				else if (0 == strcmp(token, "33"))
					button = MOUSE_1_MOVE;
				else if (0 == strcmp(token, "34"))
					button = MOUSE_2_MOVE;
				else if (0 == strcmp(token, "64"))
					button = MOUSE_WHEEL_UP;
				else if (0 == strcmp(token, "65"))
					button = MOUSE_WHEEL_DOWN;
				else
				{
					return -1;
				}
				isMouse = true;

				token = strtok_r(0, ";", &save);
				if (token)
				{
					mouseX = strtoul(token, 0, 0) - 1;
					token = strtok_r(0, ";", &save);
					if (token)
					{
						int n = strlen(token);
						if (token[n - 1] == 'm')
						{
							button++; // down -> up
						}
						token[n - 1] = 0;
						mouseY = strtoul(token, 0, 0) - 1;
					}
				}
				if (button >= MOUSE_0_DOWN && button <= MOUSE_2_UP)
				{
					int index = button - MOUSE_0_DOWN;
					if (index < 0 || index > 5)
						throw " index out of range";
					auto now = steady_clock::now();
					since_last[index] = duration_cast<milliseconds>(now - last_time_point[index]).count();
					last_time_point[index] = now;
				}
			}
			return button;
		}
		else
		{
			if (0 == sequence.rfind("A", 0))
				return KEY_UP;
			if (0 == sequence.rfind("B", 0))
				return KEY_DOWN;
			if (0 == sequence.rfind("C", 0))
				return KEY_LEFT;
			if (0 == sequence.rfind("D", 0))
				return KEY_RIGHT;
			if (0 == sequence.rfind("1~", 0))
				return KEY_HOME;
			if (0 == sequence.rfind("4~", 0))
				return KEY_END;
			if (0 == sequence.rfind("H", 0))
				return KEY_HOME;
			if (0 == sequence.rfind("F", 0))
				return KEY_END;
			if (0 == sequence.rfind("5~", 0))
				return KEY_PGUP;
			if (0 == sequence.rfind("6~", 0))
				return KEY_PGDOWN;
			if (0 == sequence.rfind("OP", 0))
				return KEY_F1;
			if (0 == sequence.rfind("OQ", 0))
				return KEY_F2;
			if (0 == sequence.rfind("13~", 0))
				return KEY_F3;
			if (0 == sequence.rfind("14~", 0))
				return KEY_F4;
			if (0 == sequence.rfind("15~", 0))
				return KEY_F5;
			if (0 == sequence.rfind("3~", 0))
				return KEY_DELETE;
		}
	}

	return -1;
#else
	DWORD cNumRead = 0;
	INPUT_RECORD irInBuf[128];
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);

	if (GetNumberOfConsoleInputEvents(hStdin, &cNumRead))
	{
		if (cNumRead > 0 && ReadConsoleInput(hStdin,
											 irInBuf,	 // buffer to read into
											 128,		 // size of read buffer
											 &cNumRead)) // number of records read
		{
			for (DWORD i = 0; i < cNumRead; ++i)
			{
				switch (irInBuf[i].EventType)
				{
				case KEY_EVENT:
					isMouse = false;
					switch (irInBuf[i].Event.KeyEvent.uChar.AsciiChar)
					{
					case VK_UP:
						return VK_UP;
					case VK_DOWN:
						return VK_DOWN;
					case VK_LEFT:
						return VK_LEFT;
					case VK_RIGHT:
						return VK_RIGHT;
					default:
						break;
					}
					return irInBuf[i].Event.KeyEvent.uChar.AsciiChar;

					break;
				case MOUSE_EVENT:
					isMouse = true;
					break;
				case WINDOW_BUFFER_SIZE_EVENT:
					width = irInBuf[i].Event.WindowBufferSizeEvent.dwSize.X;
					height = irInBuf[i].Event.WindowBufferSizeEvent.dwSize.Y;
					sizeChanged(width, height);
					break;
				}
			}
			FlushConsoleInputBuffer(hStdin);
		}
	}
	return -1;
#endif
}

void Terminal::querySize()
{
#if defined(HAS_TERMIOS)
	struct winsize w;
	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) < 0)
		return;
	if (width != w.ws_col or height != w.ws_row)
	{
		width = w.ws_col;
		height = w.ws_row;
	}
#else
	CONSOLE_SCREEN_BUFFER_INFOEX csbi;
	if (GetConsoleScreenBufferInfoEx(GetStdHandle(STD_OUTPUT_HANDLE), &csbi))
	{
		width = csbi.srWindow.Right - csbi.srWindow.Left + 1;
		height = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
	}
	else
	{
		width = 80;
		height = 25;
	}
#endif
	sizeChanged(width, height);
}

void Terminal::enterRawMode()
{
#if defined(HAS_TERMIOS)
	tcgetattr(STDIN_FILENO, &originalSettings);
#endif
	setEcho(false);
	linebuffered(false);
	altScreen();
	hideCursor();
	mouseOn();
	querySize();
	flush();
}

void Terminal::leaveRawMode()
{
#if defined(HAS_TERMIOS)
	tcsetattr(STDIN_FILENO, TCSANOW, &originalSettings);
#endif
	mouseOff();
	clear();
	reset();
	normalScreen();
	showCursor();
	flush();
}
