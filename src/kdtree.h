#ifndef KDTREE_H
#define KDTREE_H

#include <iostream>
#include <memory>
#include <vector>

class Point
{
public:
	float x, y;

	Point(float x, float y)
	  : x(x)
	  , y(y)
	{
	}

	bool operator==(const Point &other) const
	{
		return x == other.x && y == other.y;
	}
};

class KDNode
{
public:
	Point point;
	std::unique_ptr<KDNode> left;
	std::unique_ptr<KDNode> right;

	KDNode(Point point)
	  : point(point)
	  , left(nullptr)
	  , right(nullptr)
	{
	}
	~KDNode()
	{
		left.reset();
		right.reset();
	}
};

class KDTree
{
private:
	std::unique_ptr<KDNode> root;

	void insertRec(std::unique_ptr<KDNode> &node, const Point &point, unsigned depth);
	void searchRec(const std::unique_ptr<KDNode> &node, const Point &center, float radius, unsigned depth, std::vector<Point> &foundPoints) const;
	void searchRec(const std::unique_ptr<KDNode> &node, const Point &topleft, const Point &bottomright, unsigned depth, std::vector<Point> &foundPoints) const;
	void collectPoints(std::unique_ptr<KDNode> &node, std::vector<Point> &points);
	void removeRec(std::unique_ptr<KDNode> &node, const Point &topleft, const Point &bottomright, unsigned depth);
	void buildTree(std::unique_ptr<KDNode> &node, std::vector<Point> points, unsigned depth);
	void printRec(const std::unique_ptr<KDNode> &node, std::ostream &os, unsigned depth, const std::string &prefix) const;
	int countNodes(const std::unique_ptr<KDNode> &node) const;
	int maxDepth(const std::unique_ptr<KDNode> &node) const;

public:
	KDTree()
	  : root(nullptr)
	{
	}

	void insert(const Point &point);
	void insert(const std::vector<Point> &points);
	std::vector<Point> search(const Point &topleft, const Point &bottomright) const;
	std::vector<Point> search(const Point &center, float radius) const;
	void remove(const Point &topleft, const Point &bottomright);
	void print(std::ostream &os) const;
	void rebalance();
	bool isBalanced() const;
};

#endif
