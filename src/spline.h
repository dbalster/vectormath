#ifndef SPLINE_H
#define SPLINE_H

#include "vectormath.h"
#include <vector>

class Spline
{
	struct BezierPoint
	{
		V3 value;	  // current point
		V3 left;	  // tangent point to the left
		V3 right;	  // tangent point to the right (ascending index)
		float length; // length of this segment, cached
		float offset; // offset length within the points
	};

	std::vector<BezierPoint> points;

public:
	void add(const V3 &p, const V3 &left, const V3 &right);
	void clear();

	// return length of the bezier spline
	auto length() const -> float;

	// interpolate between t=[ 0,length() ) on the bezier spline
	auto interpolate(float _t) const -> V3;
};

#endif
