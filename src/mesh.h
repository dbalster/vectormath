/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef MESH_H
#define MESH_H

#include <vector>

#include "asset.h"
#include "color.h"
#include "texture.h"
#include "vectormath.h"

class Vertex
{
public:
	V3 position;
	V3 normal;
	Color color;
	V2 uv;
};

class Mesh : public Asset
{
public:
	std::vector<Vertex> vertices;
	std::vector<int> indices;
	std::vector<int> batches;
	std::vector<std::shared_ptr<Material>> materials;

	AABB3 aabb;
	bool smooth{false}; // TODO: split indices into rangs of smooth and flat triangles
	bool vertexcolors{false};

	Mesh()
	{
	}
	~Mesh() override
	{
	}

	auto type() const noexcept -> asset_type override
	{
		return ASSET_MESH;
	}

	void render(Rasterizer &_output, const M43 &_model) override;
};

#endif
