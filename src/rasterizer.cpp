/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "rasterizer.h"

void Rasterizer::resize(int _width, int _height)
{
	width = _width;
	height = _height;

	colorBuffer.resize(width * height);
	depthBuffer.resize(width * height);

	for (auto &filter : filters)
	{
		filter->resize(width, height);
	}
}

#include <fstream>

void Rasterizer::process()
{
	/*
		std::ofstream f("depth.csv");
		if (f.is_open())
		{
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					f << depthBuffer[y * width + x] << ";";
				}
				f << std::endl;
			}
			f.close();
		}
	*/
	auto &buffer = colorBuffer;
	for (auto &filter : filters)
	{
		buffer = filter->process(buffer);
	}
}

void Rasterizer::clear()
{
	std::ranges::fill(depthBuffer, 1.0f);
	Color black;
	std::ranges::fill(colorBuffer, black);
}

void Rasterizer::drawIndexedTriangles(const M43 &_model, const std::vector<Vertex> &_vertices, const std::vector<int> &_indices, int _count, int _offset)
{
	PerTriangle perTriangle;

	perTriangle.world2clip = uniform.viewProjection * _model;
	perTriangle.world2view = uniform.view * _model;

	int v02 = cullClockwise ? 0 : 2;
	int v20 = 2 - v02;

	V3 viewDir = {0, 0, -1};

	for (int i = 0; i < _count; i += 3)
	{
		auto &v0 = _vertices[_indices[_offset + i + v02]];
		auto &v1 = _vertices[_indices[_offset + i + 1]];
		auto &v2 = _vertices[_indices[_offset + i + v20]];

		// local -> world -> view -> projection -> clip space
		perVertex[0].clip = perTriangle.world2clip.transform(v0.position);
		perVertex[1].clip = perTriangle.world2clip.transform(v1.position);
		perVertex[2].clip = perTriangle.world2clip.transform(v2.position);

		if (interpolations & IPOL_COL)
		{
			perVertex[0].color = (V4)v0.color;
			perVertex[1].color = (V4)v1.color;
			perVertex[2].color = (V4)v2.color;
		}

		perVertex[0].ndc = perVertex[0].clip.homogeneous();
		perVertex[1].ndc = perVertex[1].clip.homogeneous();
		perVertex[2].ndc = perVertex[2].clip.homogeneous();

		perTriangle.normalCS = (perVertex[2].ndc - perVertex[0].ndc).cross(perVertex[1].ndc - perVertex[0].ndc);
		perTriangle.normalCS.normalize();

		perTriangle.dotScreenDir = perTriangle.normalCS.dot(viewDir);

		if (perTriangle.dotScreenDir < 0)
		{
			culledTriangles++;

			continue;
		}

		perVertex[0].view = perTriangle.world2view.transform(v0.position);
		perVertex[1].view = perTriangle.world2view.transform(v1.position);
		perVertex[2].view = perTriangle.world2view.transform(v2.position);

		perTriangle.normalVS = (perVertex[2].view - perVertex[0].view).cross(perVertex[1].view - perVertex[0].view);
		perTriangle.normalVS.normalize();
		// perTriangle.dotScreenDir = perTriangle.normalVS.dot(viewDir);
		perTriangle.dotScreenDir = std::max(perTriangle.normalVS.dot(viewDir), 0.0f);

		vertexShader(uniform, perTriangle, v0, perVertex[0]);
		vertexShader(uniform, perTriangle, v1, perVertex[1]);
		vertexShader(uniform, perTriangle, v2, perVertex[2]);

		P2 p0, p1, p2;

		// convert to raster coordinates

		float w = 0.5f * width;
		float h = 0.5f * height;

		p0.x = (perVertex[0].ndc.x + 1.0f) * w;
		p0.y = (1.0f - perVertex[0].ndc.y) * h;

		p1.x = (perVertex[1].ndc.x + 1.0f) * w;
		p1.y = (1.0f - perVertex[1].ndc.y) * h;

		p2.x = (perVertex[2].ndc.x + 1.0f) * w;
		p2.y = (1.0f - perVertex[2].ndc.y) * h;

		rasterizeTriangle(perTriangle, p2, p1, p0);
	}
}

auto Rasterizer::rasterizeTriangle(const PerTriangle &perTriangle, const P2 &p0, const P2 &p1, const P2 &p2) -> int
{
	int minX = std::min({p0.x, p1.x, p2.x});
	int minY = std::min({p0.y, p1.y, p2.y});
	int maxX = std::max({p0.x, p1.x, p2.x});
	int maxY = std::max({p0.y, p1.y, p2.y});

	sentTriangles++;

	// check out of screen
	if (maxX < 0 || minX > width || maxY < 0 || minY > height)
		return 0;

	minX = std::max(minX, 0);
	minY = std::max(minY, 0);
	maxX = std::min(maxX, width - 1);
	maxY = std::min(maxY, height - 1);

	// check too small
	if ((maxX - minX <= 0) || (maxY - minY <= 0))
		return 0;

	int A01 = p0.y - p1.y, B01 = p1.x - p0.x;
	int A12 = p1.y - p2.y, B12 = p2.x - p1.x;
	int A20 = p2.y - p0.y, B20 = p0.x - p2.x;

	P2 p = {minX, minY};
	int w0_row = orient2d(p1, p2, p);
	int w1_row = orient2d(p2, p0, p);
	int w2_row = orient2d(p0, p1, p);

	float area = orient2d(p0, p1, p2);
	if (area <= 0)
		return 0;
	area = 1.0f / area;
	if (area <= 0.000001)
		return 0;

	float csz0 = perVertex[0].clip.z;
	float csz1 = perVertex[1].clip.z;
	float csz2 = perVertex[2].clip.z;

	float rz0 = 1.0f / csz0;
	float rz1 = 1.0f / csz1;
	float rz2 = 1.0f / csz2;

	float ndcz0 = perVertex[0].ndc.z;
	float ndcz1 = perVertex[1].ndc.z;
	float ndcz2 = perVertex[2].ndc.z;

	renderedTriangles++;

	for (p.y = minY; p.y <= maxY; p.y++)
	{
		int w0 = w0_row;
		int w1 = w1_row;
		int w2 = w2_row;

		for (p.x = minX; p.x <= maxX; p.x++)
		{
			if (w0 >= 0 && w1 >= 0 && w2 >= 0)
			{
				// barycentric weights
				float c = w0 * area; // c->b->a, we're calling this p2,p1,p0
				float b = w1 * area;
				float a = 1.0f - b - c;

				// barycentric "z" interpolation
				float ndc_z = a * ndcz0 + b * ndcz1 + c * ndcz2;

				int index = (p.y * width) + p.x;

				bool depth_test_passed = false;
				switch (depthTestMode)
				{
				case DEPTH_TEST_ALWAYS:
					depth_test_passed = true;
					break;
				case DEPTH_TEST_NEVER:
					depth_test_passed = false;
					break;
				case DEPTH_TEST_EQUAL:
					depth_test_passed = ndc_z == depthBuffer[index];
					break;
				case DEPTH_TEST_LESS:
					depth_test_passed = ndc_z < depthBuffer[index];
					break;
				case DEPTH_TEST_NOT_EQUAL:
					depth_test_passed = ndc_z != depthBuffer[index];
					break;
				case DEPTH_TEST_LESS_OR_EQUAL:
					depth_test_passed = ndc_z <= depthBuffer[index];
					break;
				case DEPTH_TEST_GREATER:
					depth_test_passed = ndc_z > depthBuffer[index];
					break;
				case DEPTH_TEST_GREATER_OR_EQUAL:
					depth_test_passed = ndc_z >= depthBuffer[index];
					break;
				default:
					break;
				}

				if (depth_test_passed)
				{
					PerPixel perPixel;
					float ra = a * rz0;
					float rb = b * rz1;
					float rc = c * rz2;

					float clip_z = (1.0f / (ra + rb + rc));

					if (interpolations & IPOL_UV)
					{
						perPixel.uv.x = (ra * perVertex[0].uv.x + rb * perVertex[1].uv.x + rc * perVertex[2].uv.x) * clip_z;
						perPixel.uv.y = (ra * perVertex[0].uv.y + rb * perVertex[1].uv.y + rc * perVertex[2].uv.y) * clip_z;
					}

					if (interpolations & IPOL_NORM)
					{
						perPixel.normal.x = (ra * perVertex[0].normal.x + rb * perVertex[1].normal.x + rc * perVertex[2].normal.x) * clip_z;
						perPixel.normal.y = (ra * perVertex[0].normal.y + rb * perVertex[1].normal.y + rc * perVertex[2].normal.y) * clip_z;
						perPixel.normal.z = (ra * perVertex[0].normal.z + rb * perVertex[1].normal.z + rc * perVertex[2].normal.z) * clip_z;
						perPixel.normal.normalize();
						perPixel.normal = -perPixel.normal;
					}

					if (interpolations & IPOL_COL)
					{
						perPixel.color.x = (ra * perVertex[0].color.x + rb * perVertex[1].color.x + rc * perVertex[2].color.x) * clip_z;
						perPixel.color.y = (ra * perVertex[0].color.y + rb * perVertex[1].color.y + rc * perVertex[2].color.y) * clip_z;
						perPixel.color.z = (ra * perVertex[0].color.z + rb * perVertex[1].color.z + rc * perVertex[2].color.z) * clip_z;
						perPixel.color.w = 1.0f;
					}

					if (interpolations & IPOL_POS)
					{
						perPixel.position.x = (ra * perVertex[0].view.x + rb * perVertex[1].view.x + rc * perVertex[2].view.x) * clip_z;
						perPixel.position.y = (ra * perVertex[0].view.y + rb * perVertex[1].view.y + rc * perVertex[2].view.y) * clip_z;
						perPixel.position.z = (ra * perVertex[0].view.z + rb * perVertex[1].view.z + rc * perVertex[2].view.z) * clip_z;
					}

					perPixel.z = ndc_z;

					if (enableDepthWrites)
						depthBuffer[index] = ndc_z;
					if (enableColorWrites)
					{
						Color s = pixelShader(uniform, perTriangle, perPixel);
						if (!opaque)
						{
							Color d = colorBuffer[index];
							float sr = s.red;
							float sg = s.green;
							float sb = s.blue;
							float sa = s.alpha;
							float dr = d.red;
							float dg = d.green;
							float db = d.blue;
							// float da = d.alpha() / 255.0F;
							dr = dr * (1.0F - sa) + sr * sa;
							dg = dg * (1.0F - sa) + sg * sa;
							db = db * (1.0F - sa) + sb * sa;
							s.setf(dr, dg, db, 1.0F);
						}
						colorBuffer[index] = s;
					}
					pixelsWritten++;
				}
				// else zClipped++;
			}
			w0 += A12;
			w1 += A20;
			w2 += A01;
		}
		w0_row += B12;
		w1_row += B20;
		w2_row += B01;
	}
	return 0;
}

Rasterizer::~Rasterizer()
{
	resize(0, 0);
}
