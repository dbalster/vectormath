#include "kdtree.h"

#include <algorithm>
#include <cmath>

void KDTree::insertRec(std::unique_ptr<KDNode> &node, const Point &point, unsigned depth)
{
	if (!node)
	{
		node = std::make_unique<KDNode>(point);
		return;
	}

	unsigned cd = depth % 2;
	if ((cd == 0 && point.x < node->point.x) || (cd == 1 && point.y < node->point.y))
	{
		insertRec(node->left, point, depth + 1);
	}
	else
	{
		insertRec(node->right, point, depth + 1);
	}
}

void KDTree::searchRec(const std::unique_ptr<KDNode> &node, const Point &center, float radius, unsigned depth, std::vector<Point> &foundPoints) const
{
	if (!node)
		return;

	float dx = center.x - node->point.x;
	float dy = center.y - node->point.y;
	if (dx * dx + dy * dy <= radius * radius)
	{
		foundPoints.push_back(node->point);
	}

	unsigned cd = depth % 2;
	float distance = cd == 0 ? center.x - node->point.x : center.y - node->point.y;
	float distanceSquared = distance * distance;

	if (distance <= radius)
	{
		// Der Kreis könnte beide Unterbäume schneiden
		searchRec(node->left, center, radius, depth + 1, foundPoints);
		searchRec(node->right, center, radius, depth + 1, foundPoints);
	}
	else
	{
		// Der Kreis schneidet nur einen Unterbaum
		if (distance < 0)
		{
			searchRec(node->left, center, radius, depth + 1, foundPoints);
		}
		else
		{
			searchRec(node->right, center, radius, depth + 1, foundPoints);
		}
	}
}

void KDTree::searchRec(const std::unique_ptr<KDNode> &node, const Point &topleft, const Point &bottomright, unsigned depth,
					   std::vector<Point> &foundPoints) const
{
	if (!node)
		return;

	if (node->point.x >= topleft.x && node->point.x <= bottomright.x && node->point.y >= topleft.y && node->point.y <= bottomright.y)
	{
		foundPoints.push_back(node->point);
	}

	unsigned cd = depth % 2;
	if (cd == 0)
	{
		if (topleft.x < node->point.x)
			searchRec(node->left, topleft, bottomright, depth + 1, foundPoints);
		if (bottomright.x > node->point.x)
			searchRec(node->right, topleft, bottomright, depth + 1, foundPoints);
	}
	else
	{
		if (topleft.y < node->point.y)
			searchRec(node->left, topleft, bottomright, depth + 1, foundPoints);
		if (bottomright.y > node->point.y)
			searchRec(node->right, topleft, bottomright, depth + 1, foundPoints);
	}
}

void KDTree::collectPoints(std::unique_ptr<KDNode> &node, std::vector<Point> &points)
{
	if (!node)
		return;

	points.push_back(node->point); // Füge den Punkt des aktuellen Knotens hinzu

	collectPoints(node->left, points);	// Sammle Punkte aus dem linken Unterbaum
	collectPoints(node->right, points); // Sammle Punkte aus dem rechten Unterbaum
}

void KDTree::removeRec(std::unique_ptr<KDNode> &node, const Point &topleft, const Point &bottomright, unsigned depth)
{
	if (!node)
		return;

	// Überprüfen, ob der Punkt des Knotens im Bereich liegt
	if (node->point.x >= topleft.x && node->point.x <= bottomright.x && node->point.y >= topleft.y && node->point.y <= bottomright.y)
	{

		// Entferne den Knoten und sammle alle Punkte in den Unterbäumen
		std::vector<Point> points;
		collectPoints(node->left, points);
		collectPoints(node->right, points);
		node.reset();

		// Füge die gesammelten Punkte wieder ein, außer die im Bereich liegen
		for (const auto &point : points)
		{
			if (!(point.x >= topleft.x && point.x <= bottomright.x && point.y >= topleft.y && point.y <= bottomright.y))
			{
				insert(point);
			}
		}
	}
	else
	{
		// Rekursives Entfernen in Unterbäumen
		unsigned cd = depth % 2;
		if (cd == 0)
		{
			if (topleft.x < node->point.x)
			{
				removeRec(node->left, topleft, bottomright, depth + 1);
			}
			if (bottomright.x > node->point.x)
			{
				removeRec(node->right, topleft, bottomright, depth + 1);
			}
		}
		else
		{
			if (topleft.y < node->point.y)
			{
				removeRec(node->left, topleft, bottomright, depth + 1);
			}
			if (bottomright.y > node->point.y)
			{
				removeRec(node->right, topleft, bottomright, depth + 1);
			}
		}
	}
}

void KDTree::buildTree(std::unique_ptr<KDNode> &node, std::vector<Point> points, unsigned depth)
{
	if (points.empty())
		return;

	size_t n = points.size();
	unsigned cd = depth % 2;

	// Sortiere die Punkte nach der aktuellen Dimension
	std::nth_element(points.begin(), points.begin() + n / 2, points.end(), [cd](const Point &a, const Point &b) { return cd == 0 ? a.x < b.x : a.y < b.y; });

	// Wähle den Medianpunkt als Wurzel für den aktuellen Knoten
	Point medianPoint = points[n / 2];
	node = std::make_unique<KDNode>(medianPoint);

	// Baue die Unterbäume
	std::vector<Point> leftPoints(points.begin(), points.begin() + n / 2);
	std::vector<Point> rightPoints(points.begin() + n / 2 + 1, points.end());

	buildTree(node->left, leftPoints, depth + 1);
	buildTree(node->right, rightPoints, depth + 1);
}

void KDTree::printRec(const std::unique_ptr<KDNode> &node, std::ostream &os, unsigned depth, const std::string &prefix) const
{
	if (!node)
		return;

	os << prefix;
	if (depth != 0)
	{
		// Verwende Unicode-Zeichen für die Baumstruktur
		os << (node->left ? "├── " : "└── ");
	}

	// Drucke den Knotennamen
	os << "(" << node->point.x << ", " << node->point.y << ")" << std::endl;

	// Rekursive Aufrufe für Unterbäume
	printRec(node->left, os, depth + 1, prefix + (node->left ? "│   " : "    "));
	printRec(node->right, os, depth + 1, prefix + (node->right ? "│   " : "    "));
}

int KDTree::countNodes(const std::unique_ptr<KDNode> &node) const
{
	if (!node)
		return 0;
	return 1 + countNodes(node->left) + countNodes(node->right);
}

int KDTree::maxDepth(const std::unique_ptr<KDNode> &node) const
{
	if (!node)
		return 0;
	return 1 + std::max(maxDepth(node->left), maxDepth(node->right));
}

void KDTree::insert(const Point &point)
{
	insertRec(root, point, 0);
}

void KDTree::insert(const std::vector<Point> &points)
{
	root.reset(); // Lösche den aktuellen Baum, falls vorhanden
	buildTree(root, points, 0);
}

std::vector<Point> KDTree::search(const Point &topleft, const Point &bottomright) const
{
	std::vector<Point> foundPoints;
	searchRec(root, topleft, bottomright, 0, foundPoints);
	return foundPoints;
}

std::vector<Point> KDTree::search(const Point &center, float radius) const
{
	std::vector<Point> foundPoints;
	searchRec(root, center, radius, 0, foundPoints);
	return foundPoints;
}

void KDTree::remove(const Point &topleft, const Point &bottomright)
{
	removeRec(root, topleft, bottomright, 0);
}

void KDTree::print(std::ostream &os) const
{
	printRec(root, os, 0, "");
}

void KDTree::rebalance()
{
	std::vector<Point> points;
	collectPoints(root, points);
	root.reset();
	buildTree(root, points, 0);
}

bool KDTree::isBalanced() const
{
	int numNodes = countNodes(root);
	int depth = maxDepth(root);
	return depth <= std::log2(numNodes);
}
