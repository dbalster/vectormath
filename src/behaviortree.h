/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef BEHAVIORTREE_H
#define BEHAVIORTREE_H

#include <catch2/catch.hpp>
#include <chrono>
#include <memory>
#include <vector>

#include "blackboard.h"

namespace Behaviortree
{

enum Status : int
{
	FAILURE,
	SUCCESS,
	RUNNING,
	ERROR
};

constexpr inline const char *to_string(Status _status) noexcept
{
	switch (_status)
	{
	case FAILURE:
		return "FAILURE";
	case SUCCESS:
		return "SUCCESS";
	case RUNNING:
		return "RUNNING";
	case ERROR:
		return "ERROR";
	}
	return "ERROR";
}

class Node : public Hashed
{
public:
	Node();
	virtual ~Node();

	Node(const Node &) = delete;
	Node(Node &&) = delete;
	Node &operator=(const Node &) = delete;

	auto run(ContextWithBlackboard &_context) const -> Status;

	auto status(ContextWithBlackboard &_context) const -> Status
	{
		return std::any_cast<Status>(_context[hash]["$"]);
	}

protected:
	virtual bool open(ContextWithBlackboard &_context) const;
	virtual void close(ContextWithBlackboard &_context) const;
	virtual auto execute(ContextWithBlackboard &_context) const -> Status = 0;
};

class Decorator : public Node
{
public:
	std::shared_ptr<Node> child;
};

class Composite : public Node
{
public:
	std::vector<std::shared_ptr<Node>> children;
};

class Inverter : public Decorator
{
	auto execute(ContextWithBlackboard &_context) const -> Status override;
};

class Failer : public Node
{
	auto execute(ContextWithBlackboard &_context) const -> Status override;
};

class Succeeder : public Node
{
	auto execute(ContextWithBlackboard &_context) const -> Status override;
};

class Waiter : public Node
{
	bool open(ContextWithBlackboard &_context) const override;
	auto execute(ContextWithBlackboard &_context) const -> Status override;

public:
	std::chrono::milliseconds delay{0};
	Waiter(std::chrono::milliseconds _delay)
	  : delay(_delay)
	{
	}
};

class Repeater : public Decorator
{
	bool open(ContextWithBlackboard &_context) const override;
	auto execute(ContextWithBlackboard &_context) const -> Status override;

public:
	int count;
	Repeater(int _count)
	  : count(_count)
	{
	}
};

// ∀ failure -> failure
// simple for loop, will always start over if a subchild is running

class Selector : public Composite
{
	auto execute(ContextWithBlackboard &_context) const -> Status override;
};

// ∀ success -> success
// simple for loop, will always start over if a subchild is running

class Sequence : public Composite
{
	auto execute(ContextWithBlackboard &_context) const -> Status override;
};

// ∀ success -> success
// will exit and continue at a child that is/was running

class MemSequence : public Composite
{
	bool open(ContextWithBlackboard &_context) const override;
	auto execute(ContextWithBlackboard &_context) const -> Status override;
};

// ∀ failure -> failure
// will exit and continue at a child that is/was running

class MemSelector : public Composite
{
	bool open(ContextWithBlackboard &_context) const override;
	auto execute(ContextWithBlackboard &_context) const -> Status override;
};

class Executor : public Composite
{
	auto execute(ContextWithBlackboard &_context) const -> Status override;
};

class Printer : public Node
{
	auto execute(ContextWithBlackboard &_context) const -> Status override;

public:
	std::string text;
	Printer(const std::string &_text)
	  : text(_text)
	{
	}
};

}; // namespace Behaviortree

#endif
