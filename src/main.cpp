/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include <algorithm>
#include <chrono>
#include <cstdio>
#include <filesystem>
#include <iostream>
#include <memory>
#include <random>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include "application.h"
#include "behaviortree.h"
#include "flatimporter.h"
#include "node.h"
#include "rasterizer.h"
#include "transform.h"
#include "vectormath.h"

#include "astar.h"
#include "kdtree.h"

using std::string;
using std::stringstream;
using namespace std::chrono;
using namespace std::chrono_literals;
using std::vector;

struct Position
{
	float x, y, z;
};

struct Velocity
{
	float x, y, z;
};

struct Accelleration
{
};

class Ball
{
public:
	std::shared_ptr<Node> node;
	V3 position;
	V3 previous;
	float radius;
	V4 *rotateX;
	Q orientation;
	V3 rotation;

	void accelerate(V3 _forcefield, float dt)
	{
		position += _forcefield * dt * dt;
	}

	void integrate(float dt)
	{
		V3 p = (position * 2.0) - previous;
		previous = position;
		position = p;
		return;
		/*
		float speed = v3_length(&_ball->rotation);
		if (speed != 0.0)
		{
			vec3 n = v3_normalized(&_ball->rotation);
			quat q, q2;
			quat_from_axis_angle(&q, &n, speed * dt);
			quat_mul(&q2, &_ball->orientation, &q);
			_ball->orientation = q2;

			_ball->rotateX->x = _ball->orientation.x;
			_ball->rotateX->y = _ball->orientation.y;
			_ball->rotateX->z = _ball->orientation.z;
			_ball->rotateX->w = _ball->orientation.w;
		}
		*/
	}

	float collides(const Ball &_other) const
	{
		float distance = V3::distance(position, _other.position);
		float contact_distance = radius + _other.radius;
		return distance - contact_distance;
	}
};

struct Physics
{
	std::shared_ptr<Node> prototype;

	Ball balls[5000];
	int n_balls;

	Ball *ball_add(float x, float y, float z, float r)
	{
		Ball *b = balls + n_balls;
		n_balls++;
		/*
				b->node = sg_node_clone(_phy->prototype, true);
				sg_node_clear_flags(b->node, SG_NODE_INVISIBLE+SG_NODE_INACTIVE);

				b->props = &_phy->props;

				SgTransform *trfm = sg_transform_find_by_id(b->node->transforms,"location");
				if (trfm && trfm->type==SG_TRANSFORM_TRANSLATE)
				{
					b->position = (vec3*)(trfm+1);
				}
				else return 0;

				trfm = sg_transform_find_by_id(b->node->transforms,"rotationX");
				if (trfm && trfm->type==SG_TRANSFORM_ROTATE)
				{
					b->rotateX = (vec4*)(trfm+1);
				}

				trfm = sg_transform_find_by_id(b->node->transforms,"scale");
				if (trfm && trfm->type==SG_TRANSFORM_SCALE)
				{
					vec3 *scale = (vec3*)(trfm+1);
					scale->x = scale->y = scale->z = r;
				}


				sg_node_add(_phy->prototype->parent, b->node);

				b->radius = r;
				b->position->x = x;
				b->position->y = y;
				b->position->z = z;
				b->previous = *b->position;

				quat_id(&b->orientation);
				b->rotation.x = 1;
				b->rotation.y = 0;
				b->rotation.z = 0;
		*/
		return b;
	}
};

class FirstPersonController
{
private:
	V3 position;
	V3 velocity;
	V3 acceleration;
	V3 rotation;

	float gravity = 300.0f; // 9.81f; // Erdgravitation in m/s^2
	float jumpSpeed = 100.0f;
	float moveSpeed = 200.0f; // Geschwindigkeit der Bewegung
	float turnSpeed = 2.0f;	  // Drehgeschwindigkeit
	float damping = 0.99f;	  // Dämpfungsfaktor

	bool isGrounded = true;

public:
	FirstPersonController()
	  : position(0, 0, 0)
	  , velocity(0, 0, 0)
	  , acceleration(0, 0, 0)
	  , rotation(0, 0, 0)
	{
	}

	void Update(float deltaTimeInSeconds, int inputChar)
	{
		// Reset acceleration
		acceleration = V3(0, 0, 0);

		// Drehung
		if (inputChar == 'a')
			rotation.z += turnSpeed * deltaTimeInSeconds;
		if (inputChar == 'd')
			rotation.z -= turnSpeed * deltaTimeInSeconds;

		// Bewegungsrichtung basierend auf Rotation
		V3 forwardDir(sin(rotation.z), -cos(rotation.z), 0);
		V3 strafeDir(cos(rotation.z), sin(rotation.z), 0);

		if (inputChar == 'A')
			acceleration -= strafeDir * moveSpeed;
		if (inputChar == 'D')
			acceleration += strafeDir * moveSpeed;

		// Bewegung
		if (inputChar == 'w')
			acceleration -= forwardDir * moveSpeed;
		if (inputChar == 's')
			acceleration += forwardDir * moveSpeed;

		// Jump
		if (inputChar == ' ' && isGrounded)
		{
			velocity.z += jumpSpeed;
			isGrounded = false;
		}

		// Physik
		velocity += acceleration * deltaTimeInSeconds;
		position += velocity * deltaTimeInSeconds;

		// Erdgravitation
		if (!isGrounded)
		{
			velocity.z -= gravity * deltaTimeInSeconds;
		}

		// Dämpfung
		velocity *= damping;

		// Kollisionen und Bodenkontakt prüfen
		CheckCollisions();

		// Update die Rotation hier, falls benötigt
	}

	void CheckCollisions()
	{
		// Implementiere Kollisionserkennung und setze isGrounded entsprechend
		// ...
		if (position.z <= 0)
		{ // Einfache Bodenkollision
			position.z = 0;
			isGrounded = true;
			velocity.z = 0;
		}
	}

	V3 GetPosition() const
	{
		return position;
	}
	V3 GetRotation() const
	{
		return rotation;
	}

	// Weitere Methoden, um Eigenschaften anzupassen
	void SetGravity(float g)
	{
		gravity = g;
	}
	void SetJumpSpeed(float speed)
	{
		jumpSpeed = speed;
	}
	void SetMoveSpeed(float speed)
	{
		moveSpeed = speed;
	}
	void SetTurnSpeed(float speed)
	{
		turnSpeed = speed;
	}
	void SetDamping(float d)
	{
		damping = d;
	}
};

std::string readAllText(const std::string &filePath)
{
	return (std::stringstream() << std::ifstream(filePath).rdbuf()).str();
}

class MyApplication : public Application
{
	FirstPersonController ego;
	FlatImporter importer;
	vector<string> flatfiles;
	std::vector<std::string>::iterator current;

	std::weak_ptr<Node> fpc;
	std::weak_ptr<Node> light;

public:
	void init() override
	{
		base::init();
		for (const auto &entry : std::filesystem::directory_iterator("data"))
		{
			if (entry.is_regular_file() && entry.path().extension() == ".flat")
			{
				flatfiles.push_back(entry.path());
			}
		}
		std::ranges::sort(flatfiles, [&](auto &a, auto &b) { return a < b; });

		current = flatfiles.begin();
		setScene(importer.load(*current));
		animationSpeedInFramesPerSecond = importer.fps;
	}

	void setScene(std::shared_ptr<Node> _scene) override
	{
		base::setScene(_scene);
		camera = scene->findInChildren("Camera");

		fpc = scene->find("Ego");
		if (auto n = fpc.lock())
		{
			n->clearTransforms();
			n->appendTransform(new TranslateTransform("location", V3()));
			n->appendTransform(new RotateEulerTransform("euler_rotation", 0, 0, 0));
		}

		light = scene->find("Light");
	}

	void render() override
	{
		if (auto l = light.lock())
		{
			auto m = rasterizer.uniform.view * l->world;

			rasterizer.uniform.lightPosition.x = m.m[3][0];
			rasterizer.uniform.lightPosition.y = m.m[3][1];
			rasterizer.uniform.lightPosition.z = m.m[3][2];
		}

		base::render();
	}

	void onMouse(int _x, int _y, int _button) override
	{
		base::onMouse(_x, _y, _button);
	}

	void update() override
	{
		ego.Update(0.01f, -1);

		if (auto n = fpc.lock())
		{
			n->set<TranslateTransform>("location", [&](auto &trfm) { trfm.location = ego.GetPosition(); });
			n->set<RotateEulerTransform>("euler_rotation", [&](auto &trfm) { trfm.euler = ego.GetRotation(); });

			if (auto cam = camera.lock())
			{
				cam->dirty = true;
			}
		}
	}

	void onKey(int _key) override
	{
		base::onKey(_key);
		ego.Update(0.01f, _key);
		switch (_key)
		{
		case 'q':
			running = false;
			break;
		case '+': {
			++current;
			if (current == flatfiles.end())
				current = flatfiles.begin();
			setScene(importer.load(*current));
			animationSpeedInFramesPerSecond = importer.fps;
		}
		break;
		case '-': {
			if (current == flatfiles.begin())
				current = flatfiles.end();
			--current;
			setScene(importer.load(*current));
			animationSpeedInFramesPerSecond = importer.fps;
		}
		break;
		}
	}
};

class Labyrinth
{
public:
	enum Cell
	{
		GROUND,
		WALL,
		OBSTACLE
	};

private:
	std::vector<std::vector<char>> grid;

	struct Edge
	{
		int x1, y1, x2, y2;
	};

	int find(std::vector<int> &parent, int i)
	{
		if (parent[i] == i)
		{
			return i;
		}
		return find(parent, parent[i]);
	}

	void unionSets(std::vector<int> &parent, int x, int y)
	{
		int xroot = find(parent, x);
		int yroot = find(parent, y);
		parent[xroot] = yroot;
	}

public:
	Labyrinth()
	  : width(0)
	  , height(0)
	{
	}
	int width, height;

	void generate(int w, int h)
	{
		width = w;
		height = h;

		// Initialisierung des Gitters
		grid = std::vector<std::vector<char>>(height, std::vector<char>(width, WALL));

		std::vector<Edge> edges;
		std::vector<int> parent(width * height);

		// Kanten und Elterninitialisierung
		for (int y = 0; y < height; ++y)
		{
			for (int x = 0; x < width; ++x)
			{
				if (x < width - 1)
				{
					edges.push_back({x, y, x + 1, y});
				}
				if (y < height - 1)
				{
					edges.push_back({x, y, x, y + 1});
				}
				parent[y * width + x] = y * width + x;
			}
		}

		// Mischen der Kanten
		std::shuffle(edges.begin(), edges.end(), std::mt19937{std::random_device{}()});

		// Kruskals Algorithmus
		for (const auto &edge : edges)
		{
			int x = edge.x1 + edge.x2;
			int y = edge.y1 + edge.y2;
			if (find(parent, edge.y1 * width + edge.x1) != find(parent, edge.y2 * width + edge.x2))
			{
				unionSets(parent, edge.y1 * width + edge.x1, edge.y2 * width + edge.x2);
				grid[y / 2][x / 2] = GROUND;
			}
		}
	}
	void print() const
	{
		for (int x = 0; x < width + 2; ++x)
			std::cout << "█";
		std::cout << "\n";

		for (int y = 0; y < height; ++y)
		{
			std::cout << "█";
			for (int x = 0; x < width; ++x)
			{
				switch (grid[y][x])
				{
				case GROUND:
					std::cout << " ";
					break;
				case WALL:
					std::cout << "█";
					break;
				default:
					std::cout << grid[y][x];
					break;
				}
			}
			std::cout << "█\n";
		}
		for (int x = 0; x < width + 2; ++x)
			std::cout << "█";
	}

	char get(int x, int y) const
	{
		if (x < 0 || x >= width || y < 0 || y >= height)
		{
			throw std::out_of_range("Index außerhalb des Labyrinths");
		}
		return grid[y][x];
	}

	void set(int x, int y, char cell)
	{
		if (x < 0 || x >= width || y < 0 || y >= height)
		{
			throw std::out_of_range("Index außerhalb des Labyrinths");
		}
		grid[y][x] = cell;
	}
};

struct GridNode
{
	int x, y;

	bool operator==(const GridNode &other) const
	{
		return x == other.x && y == other.y;
	}

	struct Hash
	{
		size_t operator()(const GridNode &node) const
		{
			return std::hash<int>()(node.x) ^ (std::hash<int>()(node.y) << 1);
		}
	};
};

Labyrinth laby;

void astartest()
{

	AStar<GridNode, double> astar([](const GridNode &a, const GridNode &b) { return std::hypot(b.x - a.x, b.y - a.y); },
								  [](const GridNode &a, const GridNode &b) { return std::hypot(b.x - a.x, b.y - a.y); },
								  [](std::vector<GridNode> &neighbors, const GridNode &node) {
									  for (int i = -1; i <= 1; ++i)
									  {
										  auto x = node.x + i;
										  if (x < 0 || x >= laby.width)
											  continue;
										  for (int j = -1; j <= 1; ++j)
										  {
											  auto y = node.y + j;
											  if (y < 0 || y >= laby.height)
												  continue;
											  auto c = laby.get(x, y);
											  if (c == Labyrinth::Cell::GROUND)
											  {
												  neighbors.push_back({x, y});
											  }
										  }
									  }
								  });

	GridNode start{1, 1};
	GridNode goal{13, 6};

	astar.begin(start, goal);

	std::cout << "Searching:";
	while (astar.search(10) == SearchState::SEARCHING)
	{
		std::cout << ".";
	}
	std::cout << "\n";
}

template <typename TYPE> class IndexedPool
{
	std::vector<std::shared_ptr<TYPE>> pool;
	std::vector<size_t> free;
	std::vector<size_t> used;

public:
	typedef TYPE value_type;

	IndexedPool(const size_t _size, std::function<std::shared_ptr<value_type>()> _gen)
	{
		for (size_t i = 0; i < _size; ++i)
		{
			pool.push_back(_gen());
			free.push_back(i);
		}
	}

	inline auto get(size_t _index) const -> std::shared_ptr<value_type>
	{
		if (_index >= pool.size())
			return std::shared_ptr<value_type>();
		return pool[_index];
	}

	auto request() -> size_t
	{
		if (free.empty())
			return ~0;
		auto e = free.back();
		used.push_back(e);
		free.pop_back();
		return e;
	}

	void release(size_t _index)
	{
		auto iter = std::remove(used.begin(), used.end(), _index);
		if (iter != used.end())
		{
			used.erase(iter, used.end());
			free.push_back(_index);
		}
	}
};

class AStarManager : public IndexedPool<AStar<GridNode, double>>
{
	static double cost(const GridNode &a, const GridNode &b)
	{
		return std::hypot(b.x - a.x, b.y - a.y);
	}
	static double heuristic(const GridNode &b, const GridNode &a)
	{
		return std::hypot(b.x - a.x, b.y - a.y);
	}
	void nearby(std::vector<GridNode> &neighbors, const GridNode &node)
	{
		for (int i = -1; i <= 1; ++i)
		{
			auto x = node.x + i;
			if (x < 0 || x >= laby.width)
				continue;
			for (int j = -1; j <= 1; ++j)
			{
				auto y = node.y + j;
				if (y < 0 || y >= laby.height)
					continue;
				auto c = laby.get(x, y);
				if (c == Labyrinth::GROUND)
				{
					neighbors.push_back({x, y});
				}
			}
		}
	}

public:
	const Labyrinth &laby;
	AStarManager(const Labyrinth &_laby, size_t _poolSize)
	  : IndexedPool(_poolSize,
					[this]() { return std::make_shared<value_type>(cost, heuristic, [this](std::vector<GridNode> &g, const GridNode &c) { nearby(g, c); }); })
	  , laby(_laby)
	{
	}
};

AStarManager astarmgr(laby, 26);

class Pathfinder : public Behaviortree::Node
{
	bool open(ContextWithBlackboard &_context) const override
	{
		auto index = astarmgr.request();
		_context[hash]["A*"] = index;
		if (index == ~0UL)
		{
			return false;
		}

		auto start_x = std::any_cast<int>(_context[0]["start_x"]);
		auto start_y = std::any_cast<int>(_context[0]["start_y"]);
		auto goal_x = std::any_cast<int>(_context[0]["goal_x"]);
		auto goal_y = std::any_cast<int>(_context[0]["goal_y"]);
		auto astar = astarmgr.get(index);

		GridNode start{start_x, start_y};
		GridNode goal{goal_x, goal_y};

		astar->begin(start, goal);

		return Node::open(_context);
	}

	auto execute(ContextWithBlackboard &_context) const -> Behaviortree::Status override
	{
		auto index = std::any_cast<size_t>(_context[hash]["A*"]);
		if (index == ~0UL)
		{
			return Behaviortree::Status::ERROR;
		}

		auto astar = astarmgr.get(index);

		auto result = astar->search(1);

		switch (result)
		{
		case SEARCHING: {

			for (const auto &node : astar->debug())
			{
				auto s = std::any_cast<char>(_context[123]["id"]);
				laby.set(node.x, node.y, (char)s);
			}
		}
			return Behaviortree::Status::RUNNING;
		case FOUND: {
			auto path = astar->result();
			_context[0]["path"] = path;
		}
			return Behaviortree::Status::SUCCESS;
		case NOT_FOUND:
			return Behaviortree::Status::FAILURE;
		}

		return Behaviortree::Status::ERROR;
	}
	void close(ContextWithBlackboard &_context) const override
	{
		Node::close(_context);

		auto index = std::any_cast<size_t>(_context[hash]["A*"]);
		astarmgr.release(index);
	}

public:
};

class RandomGenerator
{
private:
	std::mt19937 gen;
	std::random_device rd;

public:
	void init()
	{
		gen = std::mt19937(rd());
	}

	int generate(int min, int max)
	{
		if (max <= min)
		{
			throw std::invalid_argument("Max muss größer als Min sein");
		}

		std::uniform_int_distribution<int> dist(min, max - 1);
		return dist(gen);
	}
};

RandomGenerator rng;

class TargetSetter : public Behaviortree::Node
{
	auto execute(ContextWithBlackboard &_context) const -> Behaviortree::Status override
	{

		_context[0]["start_x"] = rng.generate(0, laby.width);
		_context[0]["start_y"] = rng.generate(0, laby.height);
		_context[0]["goal_x"] = rng.generate(0, laby.width);
		_context[0]["goal_y"] = rng.generate(0, laby.height);
		return Behaviortree::Status::SUCCESS;
	}
};

class AnimationPlayer : public Behaviortree::Node
{
	auto execute(ContextWithBlackboard &_context) const -> Behaviortree::Status override
	{
		return Behaviortree::Status::SUCCESS;
	}
};

class Unit : public ContextWithBlackboard
{
public:
	std::shared_ptr<Behaviortree::Node> bt;
	char id;

	Unit(std::shared_ptr<Behaviortree::Node> _bt, std::shared_ptr<Blackboard> _bb, char _id)
	  : ContextWithBlackboard(_bb)
	  , bt(_bt)
	  , id(_id)
	{
		x = rng.generate(0, laby.width);
		y = rng.generate(0, laby.height);

		(*this)[123]["id"] = (char)_id;
	}

	int x, y;
};

class UnitMover : public Behaviortree::Node
{
	// open: receive path via blackboard
	// open: t=0

	auto execute(ContextWithBlackboard &_context) const -> Behaviortree::Status override
	{
		// while t < 1: return RUNNING, else SUCCESS
		// interpolate x,y along path at t
		// t += dt

		return Behaviortree::Status::SUCCESS;
	}
};

/*
	wolf
	- idleing			-> ...ing = laufender vorgang = animation
	- walking
	- running
	- fleeing
	- hunting
	- attacking
	- sleeping
	...

	- hungry
	- damaged


	wolf.bt =
	select:
	- running
	- damaged & attacked? fleeing
	- hasTargetFar? hunting
	- hasTargetNear? attacking
	- tired? sleeping
  - else: walking

-----------------------------------

unit=daniel

goal: satt sein, unverletzt sein, arbeit gemacht haben ..
world/unit state: daniel hat hunger

actions:
- pizza bestellen
  + telefonnummer haben
	+ geld
	=> pizza
	cost: geld=10€ und zeit=10min

- pizza essen
  => satt

- pizza backen
  + zutaten
	=> pizza
	cost: geld=0 und zeit=1std

=> A* !

  pizza bestellen
	pizza essen





*/

void test()
{
	KDTree kdTree;

	std::vector<Point> points;
	points.reserve(1000);
	for (int i = 0; i < 1000; ++i)
	{
		auto x = rng.generate(0, 1000);
		auto y = rng.generate(0, 1000);
		points.push_back(Point(x, y));
	}

	kdTree.insert(points);
	// kdTree.print(std::cout);

	auto foundPoints = kdTree.search(Point(200, 333), Point(480, 555));
	foundPoints = kdTree.search(Point(500, 500), 100.0f);

	for (const auto &point : foundPoints)
	{
		std::cout << "(" << point.x << ", " << point.y << ")" << std::endl;
	}

	// Teste weitere Funktionen...
}

void bttest()
{
	using namespace Behaviortree;

	rng.init();

	auto sequence = std::make_shared<MemSequence>();
	auto waiter = std::make_shared<Waiter>(500ms);
	auto printer = std::make_shared<Printer>("Hallo, Welt");
	auto repeater = std::make_shared<Repeater>(5);
	auto pathfinder = std::make_shared<Pathfinder>();
	auto targetsetter = std::make_shared<TargetSetter>();
	auto unitmover = std::make_shared<UnitMover>();
	auto animplayer = std::make_shared<AnimationPlayer>();

	sequence->children.push_back(targetsetter);
	sequence->children.push_back(animplayer);
	sequence->children.push_back(pathfinder);
	//		sequence->children.push_back(parallel);
	//		parallel->children.push_back(unitmover);
	//		parallel->children.push_back(walkcycler);
	//		parallel->children.push_back(soundplayer);
	sequence->children.push_back(animplayer);

	std::vector<std::shared_ptr<Unit>> units;

	Labyrinth l;
	l.generate(180, 20);
	laby = l;

	auto bb = std::make_shared<Blackboard>();

	for (char id = 'A'; id <= 'Z'; id++)
	{
		units.push_back(std::make_shared<Unit>(sequence, bb, id));
	}

	while (true)
	{
		std::cout << "\x1b[1;1H";
		laby = l;
		for (auto &unit : units)
		{
			unit->bt->run(*unit);
		}
		laby.print();
		std::cout << std::flush;

		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}
#include "goap.h"
using namespace GOAP;

class WolfGoal : public Goal
{
public:
	// Wolf könnte das Ziel haben, nicht hungrig zu sein
	bool isSatisfied(const WorldState &state) const override
	{
		return !state.at("isHungry");
	}

	WorldState getRequiredConditions() const override
	{
		return {{"isHungry", false}};
	}
};

class SheepGoal : public Goal
{
public:
	// Schaf könnte das Ziel haben, in Sicherheit zu sein
	bool isSatisfied(const WorldState &state) const override
	{
		return state.at("isSafe");
	}

	WorldState getRequiredConditions() const override
	{
		return {{"isSafe", true}};
	}
};

class HuntAction : public GOAP::Action
{
public:
	bool checkPreconditions(const WorldState &state) const override
	{
		return state.at("isAlive") && state.at("isHungry");
	}

	void applyEffects(WorldState &state) const override
	{
		state["isHungry"] = false;
		state["isAlive"] = false; // für das Schaf
	}

	void execute() override
	{
		// Logik für die Jagd
	}

	float getCost() const override
	{
		return 10.0f; // Kosten für die Aktion, z.B. Energieverbrauch
	}
};

class FleeAction : public GOAP::Action
{
public:
	bool checkPreconditions(const WorldState &state) const override
	{
		bool fulfilled = false;
		try
		{
			fulfilled = state.at("isAlive") && !state.at("isSafe");
		}
		catch (...)
		{
		}
		return fulfilled;
	}

	void applyEffects(WorldState &state) const override
	{
		state["isSafe"] = true;
	}

	void execute() override
	{
		// Logik für die Flucht
	}

	float getCost() const override
	{
		return 5.0f; // Kosten für die Aktion
	}
};

class Actor
{
public:
	virtual ~Actor() = default;

	// Jeder Akteur hat einen aktuellen WorldState
	WorldState currentState;

	// Methode zur Ausführung einer Aktion
	virtual void performAction(std::shared_ptr<GOAP::Action> action)
	{
		if (action->checkPreconditions(currentState))
		{
			action->applyEffects(currentState);
			action->execute();
		}
	}

	// Abstrakte Methode, die in abgeleiteten Klassen definiert wird
	virtual std::shared_ptr<Goal> getGoal() = 0;

	// Weitere gemeinsame Eigenschaften und Methoden
};

class Wolf : public Actor
{
private:
	std::shared_ptr<Goal> wolfGoal;

public:
	Wolf()
	{
		wolfGoal = std::make_shared<WolfGoal>();
		// Initialisiere den Zustand des Wolfes
		currentState = {{"isHungry", true}, {"isAlive", true}, {"isSafe", true}};
	}

	std::shared_ptr<Goal> getGoal() override
	{
		return wolfGoal;
	}

	// Weitere wolf-spezifische Methoden
};

class Sheep : public Actor
{
private:
	std::shared_ptr<Goal> sheepGoal;

public:
	Sheep()
	{
		sheepGoal = std::make_shared<SheepGoal>();
		// Initialisiere den Zustand des Schafes
		currentState = {{"isSafe", false}, {"isAlive", true}, {"isHungry", true}};
	}

	std::shared_ptr<Goal> getGoal() override
	{
		return sheepGoal;
	}

	// Weitere sheep-spezifische Methoden
};

GOAP::Planner::Planner()
{
}
GOAP::Planner::~Planner()
{
}
std::vector<std::shared_ptr<GOAP::Action>> GOAP::Planner::plan(const WorldState &startState, std::shared_ptr<Goal> goal)
{
	return {};
}

std::vector<std::shared_ptr<GOAP::Action>> allActions;

class MyPlanner : Planner
{
public:
	std::vector<std::shared_ptr<GOAP::Action>> plan(const WorldState &startState, std::shared_ptr<Goal> goal)
	{
		AStar<WorldState, float> astar(
		  // Heuristikfunktion
		  [](const WorldState &current, const WorldState &goal) -> float {
			  float distance = 0.0f;
			  for (const auto &[key, value] : goal)
			  {
				  auto found = current.find(key);
				  if (found == current.end() || found->second != value)
				  {
					  distance += 1.0f; // Jeder Unterschied zählt als 1
				  }
			  }
			  return distance;
		  },
		  // Kostenfunktion
		  [](const WorldState &from, const WorldState &to) -> float {
			  return 1.0f;
			  // Berechne die Kosten für den Übergang von 'from' zu 'to'
		  },
		  // Nachbarfunktion
		  [&](std::vector<WorldState> &neighbors, const WorldState &current) {
			  for (const auto &action : allActions)
			  {
				  if (action->checkPreconditions(current))
				  {
					  WorldState newState = current;
					  action->applyEffects(newState);
					  neighbors.push_back(newState);
				  }
			  }
		  });

		astar.begin(startState, goal->getRequiredConditions());
		SearchState state = astar.search(1000);

		std::vector<std::shared_ptr<GOAP::Action>> actions;
		if (state == FOUND)
		{
			for (const auto &state : astar.result())
			{
				// Konvertiere WorldStates zurück in Aktionen
				// Dies hängt davon ab, wie deine Aktionen und WorldStates definiert sind
			}
		}

		return actions;
	}
};

void goap()
{
	Wolf wolf;
	Sheep sheep;
	MyPlanner planner;

	allActions.push_back(std::make_shared<HuntAction>());
	allActions.push_back(std::make_shared<FleeAction>());

	while (true)
	{
		// Planung für den Wolf
		if (!wolf.getGoal()->isSatisfied(wolf.currentState))
		{
			ActionList wolfActions = planner.plan(wolf.currentState, wolf.getGoal());
			for (auto action : wolfActions)
			{
				wolf.performAction(action);
			}
		}

		// Planung für das Schaf
		if (!sheep.getGoal()->isSatisfied(sheep.currentState))
		{
			ActionList sheepActions = planner.plan(sheep.currentState, sheep.getGoal());
			for (auto action : sheepActions)
			{
				sheep.performAction(action);
			}
		}

		// Weitere Spiellogik (z.B. Zustandsaktualisierungen, Kollisionserkennung, Rendering usw.)

		// Spiel-Loop-Ende (z.B. Prüfung auf Spielende, Zeitaktualisierungen usw.)
	}
}

int main(int /*argc*/, char ** /*argv*/)
{
	using namespace std;
	goap();
	exit(0);

	::setenv("TERM", "xterm-256color", 0);

	MyApplication app;

	return app.run();
}
