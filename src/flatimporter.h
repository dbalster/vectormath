/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef FLATIMPORTER_
#define FLATIMPORTER_

#include <fstream>

#include <memory>

class Action;
class FCurve;
class Node;
class Curve;
class Spline;

class FlatImporter
{
	void parseSplines(std::istream &file, std::shared_ptr<Curve> curve);
	void parseSpline(std::istream &file, Spline &spline);
	void parseFCurve(std::istream &file, FCurve &fcurve);
	void parseFCurves(std::istream &file, std::shared_ptr<Action>);
	void parseCurve(std::istream &file);
	void parseCurves(std::istream &file);
	void parseAction(std::istream &file);
	void parseActions(std::istream &file);
	void parseMaterial(std::istream &file);
	void parseMesh(std::istream &file);
	void parseCamera(std::istream &file);
	void parseMaterials(std::istream &file);
	void parseAssets(std::istream &file);
	void parseModifiers(std::istream &file, std::shared_ptr<Node>);
	void parseNode(std::shared_ptr<Node> parent, std::istream &file);

public:
	float fps;
	std::shared_ptr<Node> load(const std::string &filename);
};

#endif
