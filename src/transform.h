/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "scene.h"
#include "vectormath.h"

enum transform_type
{
	TRANSFORM_MATRIX,
	TRANSFORM_ROTATE,
	TRANSFORM_SCALE,
	TRANSFORM_TRANSLATE,
	TRANSFORM_SKEW,
	TRANSFORM_LOOKAT,
	TRANSFORM_QUATERNION,
	TRANSFORM_COUNT
};

class Transform
{
	Transform(const Transform &) = delete;
	Transform(const Transform &&) = delete;
	Transform &operator=(const Transform &) = delete;
	Transform &operator=(Transform &&) = delete;

public:
	std::string name;
	Transform *next;
	Transform(const std::string &_name)
	  : name(_name)
	  , next(nullptr)
	{
	}
	virtual ~Transform()
	{
		if (next)
			delete next;
	}

	virtual M43 matrix() const = 0;

	virtual auto clone() const -> Transform * = 0;

	virtual auto resolve(const std::string &_datapath) -> Bindings<float>::setter_type = 0;

	void eval(M43 &_m) const
	{
		if (next)
			next->eval(_m);
		_m = matrix() * _m;
	}
};

class TranslateTransform : public Transform
{
public:
	V3 location;
	TranslateTransform(const std::string &_name, const V3 &_v)
	  : Transform(_name)
	  , location(_v)
	{
	}
	TranslateTransform(const std::string &_name, float _x, float _y, float _z)
	  : Transform(_name)
	  , location(_x, _y, _z)
	{
	}
	auto clone() const -> Transform * override
	{
		return new TranslateTransform(name, location);
	}

	auto resolve(const std::string &_datapath) -> Bindings<float>::setter_type override
	{
		if (_datapath == "location.X")
			return {[this](float v) { location.x = v; }};
		if (_datapath == "location.Y")
			return {[this](float v) { location.y = v; }};
		if (_datapath == "location.Z")
			return {[this](float v) { location.z = v; }};
		return {};
	}

	M43 matrix() const override
	{
		return M43::translate(location);
	}
};

class ScaleTransform : public Transform
{
public:
	V3 scale;
	ScaleTransform(const std::string &_name, const V3 &_v)
	  : Transform(_name)
	  , scale(_v)
	{
	}
	ScaleTransform(const std::string &_name, float _u)
	  : Transform(_name)
	  , scale(_u, _u, _u)
	{
	}
	ScaleTransform(const std::string &_name, float _x, float _y, float _z)
	  : Transform(_name)
	  , scale(_x, _y, _z)
	{
	}
	auto clone() const -> Transform * override
	{
		return new ScaleTransform(name, scale);
	}
	auto resolve(const std::string &_datapath) -> Bindings<float>::setter_type override
	{
		if (_datapath == "scale.X")
			return {[this](float v) { scale.x = v; }};
		if (_datapath == "scale.Y")
			return {[this](float v) { scale.y = v; }};
		if (_datapath == "scale.Z")
			return {[this](float v) { scale.z = v; }};
		return {};
	}

	M43 matrix() const override
	{
		return M43::scale(scale);
	}
};

class RotateEulerTransform : public Transform
{
public:
	V3 euler;
	RotateEulerTransform(const std::string &_name, float _a, float _b, float _c)
	  : Transform(_name)
	  , euler(_a, _b, _c)
	{
	}
	auto clone() const -> Transform * override
	{
		return new RotateEulerTransform(name, euler.x, euler.y, euler.z);
	}
	auto resolve(const std::string &_datapath) -> Bindings<float>::setter_type override
	{
		if (_datapath == "rotation_euler.X")
			return {[this](float v) { euler.x = v; }};
		if (_datapath == "rotation_euler.Y")
			return {[this](float v) { euler.y = v; }};
		if (_datapath == "rotation_euler.Z")
			return {[this](float v) { euler.z = v; }};
		return {};
	}

	M43 matrix() const override
	{
		return M43::rotate(V3{0, 0, 1}, euler.z) * M43::rotate(V3{0, 1, 0}, euler.y) * M43::rotate(V3{1, 0, 0}, euler.x);
	}
};

#endif
