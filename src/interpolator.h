/*
 */

#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <type_traits>
#include <vector>

#include <algorithm>
#include <memory>
#include <type_traits>
#include <vector>

template <typename T> class KDTree
{
	static_assert(std::is_arithmetic<T>::value, "Template parameter T must be a numeric type.");

private:
	struct Node
	{
		float x;
		float y;
		T value;
		std::unique_ptr<Node> left;
		std::unique_ptr<Node> right;
		int axis; // 0 for x, 1 for y

		Node(float _x, float _y, const T &_value, int _axis)
		  : x(_x)
		  , y(_y)
		  , value(_value)
		  , left(nullptr)
		  , right(nullptr)
		  , axis(_axis)
		{
		}
	};

	std::unique_ptr<Node> root;

public:
	void insert(float x, float y, const T &value)
	{
		insert(root, x, y, value, 0);
	}

	// Interpolation is not implemented; the findNearest function demonstrates how to search in the tree
	T findNearest(float x, float y)
	{
		Node *nearest = nullptr;
		searchNearest(root.get(), x, y, nearest, std::numeric_limits<float>::max());
		if (nearest == nullptr)
		{
			throw std::runtime_error("No points in KD Tree");
		}
		return nearest->value;
	}

private:
	void insert(std::unique_ptr<Node> &node, float x, float y, const T &value, int depth)
	{
		if (!node)
		{
			node = std::make_unique<Node>(x, y, value, depth % 2);
			return;
		}

		int axis = node->axis;
		if ((axis == 0 && x < node->x) || (axis == 1 && y < node->y))
		{
			insert(node->left, x, y, value, depth + 1);
		}
		else
		{
			insert(node->right, x, y, value, depth + 1);
		}
	}

	void searchNearest(Node *node, float x, float y, Node *&bestNode, float &bestDist)
	{
		if (node == nullptr)
			return;

		float dist = distanceSquared(x, y, node->x, node->y);
		if (dist < bestDist)
		{
			bestDist = dist;
			bestNode = node;
		}

		int axis = node->axis;
		Node *goodSide = (axis == 0) ? (x < node->x ? node->left.get() : node->right.get()) : (y < node->y ? node->left.get() : node->right.get());
		Node *badSide = (goodSide == node->left.get()) ? node->right.get() : node->left.get();

		searchNearest(goodSide, x, y, bestNode, bestDist);

		// Check if we need to explore the other side
		if (distToAxis(x, y, node->x, node->y, axis) < bestDist)
		{
			searchNearest(badSide, x, y, bestNode, bestDist);
		}
	}

	float distanceSquared(float x1, float y1, float x2, float y2)
	{
		return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
	}

	float distToAxis(float x, float y, float px, float py, int axis)
	{
		return axis == 0 ? std::abs(x - px) : std::abs(y - py);
	}
};

template <typename T> class Interpolator
{
	static_assert(std::is_arithmetic<T>::value, "Template parameter T must be a numeric type.");

private:
	struct Point
	{
		float x;
		float y;
		T value;

		Point(float _x, float _y, const T &_value)
		  : x(_x)
		  , y(_y)
		  , value(_value)
		{
		}
	};

	std::vector<Point> points; // This could be replaced by a KD-Tree for efficiency

public:
	void add(float _x, float _y, const T &_value)
	{
		points.emplace_back(_x, _y, _value);
	}

	void clear()
	{
		// If using a KD-Tree, this should be replaced with tree-specific clearing method
		points.clear();
	}

	// A basic implementation of the 3-point interpolation in 2D space
	// The interpolation is based on the assumption that the points form a non-degenerate triangle
	auto get(float _x, float _y) -> T
	{
		if (points.size() < 3)
		{
			throw std::runtime_error("Not enough points to perform interpolation.");
		}

		// Here you would replace this with the nearest neighbor search using a KD-Tree
		// For simplicity, we're going to sort by distance and pick the closest three
		std::ranges::sort(points, [_x, _y](const Point &a, const Point &b) {
			float distA = (a.x - _x) * (a.x - _x) + (a.y - _y) * (a.y - _y);
			float distB = (b.x - _x) * (b.x - _x) + (b.y - _y) * (b.y - _y);
			return distA < distB;
		});

		// After sorting, the first three points are the closest
		auto p1 = points[0];
		auto p2 = points[1];
		auto p3 = points[2];

		// Compute the areas and use them as weights for the values
		float areaTotal = std::abs((p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y));
		float area1 = std::abs((_x - p2.x) * (_y - p3.y) - (_x - p3.x) * (_y - p2.y)) / areaTotal;
		float area2 = std::abs((_x - p3.x) * (_y - p1.y) - (_x - p1.x) * (_y - p3.y)) / areaTotal;
		float area3 = std::abs((_x - p1.x) * (_y - p2.y) - (_x - p2.x) * (_y - p1.y)) / areaTotal;

		// Interpolate the values based on the areas
		T interpolatedValue = p1.value * area1 + p2.value * area2 + p3.value * area3;
		return interpolatedValue;
	}
};
