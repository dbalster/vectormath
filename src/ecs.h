#ifndef ECS_H
#define ECS_H

#include <cstdint>
#include <memory>
#include <typeindex>
#include <unordered_map>
#include <vector>

using Entity = std::uint32_t;
const Entity MAX_ENTITIES = 1000;

class ComponentStore
{
public:
	template <typename T> void declare()
	{
		std::type_index typeName = std::type_index(typeid(T));

		componentTypes[typeName] = nextComponentType;
		componentArrays[typeName] = std::make_shared<std::vector<std::shared_ptr<T>>>(MAX_ENTITIES);
		nextComponentType++;
	}

	template <typename T> void add(Entity entity, T component)
	{
		std::shared_ptr<T> compPtr = std::make_shared<T>(component);
		(*getComponentArray<T>())[entity] = compPtr;
	}

	template <typename T> std::weak_ptr<T> get(Entity entity)
	{
		auto compPtr = (*getComponentArray<T>())[entity];
		if (compPtr)
		{
			return std::weak_ptr<T>(compPtr);
		}
		else
		{
			return std::weak_ptr<T>();
		}
	}

private:
	std::unordered_map<std::type_index, std::shared_ptr<void>> componentArrays;
	std::unordered_map<std::type_index, std::size_t> componentTypes;
	std::size_t nextComponentType = 0;

	template <typename T> std::shared_ptr<std::vector<std::shared_ptr<T>>> getComponentArray()
	{
		std::type_index typeName = std::type_index(typeid(T));
		return std::static_pointer_cast<std::vector<std::shared_ptr<T>>>(componentArrays[typeName]);
	}
};

#endif
