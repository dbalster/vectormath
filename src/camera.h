/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef CAMERA_H
#define CAMERA_H

#include "asset.h"
#include <cmath>

class Camera : public Asset
{
public:
	asset_type type() const noexcept override
	{
		return ASSET_CAMERA;
	}

	Camera()
	  : near(0.1)
	  , far(100)
	  , aspectRatio(1.77f)
	  , yFov(24)
	{
	}

	/* lens/angle conversion (radians) */
	static inline auto focallength_to_fov(float focal_length, float sensor) -> float
	{
		return 2.0F * atan((sensor / 2.0F) / focal_length);
	}

	static inline auto fov_to_focallength(float hfov, float sensor) -> float
	{
		return (sensor / 2.0F) / tan(hfov * 0.5F);
	}

	static inline auto xfov_to_yfov(float xfov, float aspect_ratio) -> float
	{
		return 2.0F * atan(tan(xfov * 0.5F) / aspect_ratio);
	}

	float near;
	float far;
	float aspectRatio;
	float yFov;
};

#endif
