/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef NODE_H
#define NODE_H

#include "rasterizer.h"
#include "scene.h"
#include "transform.h"
#include "vectormath.h"
#include <chrono>
#include <list>
#include <memory>
#include <string>

class Asset;
class Action;

/*
	simple scene tree.

	nodes have strong up(parent) and down(children) relations, so chop them if a node needs to be deleted

	find() will return a weak reference

*/

class Node : public std::enable_shared_from_this<Node>, public Bindable
{
public:
	Node(const Node &) = delete;
	Node(Node &&) = delete;
	Node &operator=(const Node &) = delete;
	Node &operator=(Node &&) = delete;

	std::shared_ptr<Node> parent;

	std::shared_ptr<Asset> asset;
	std::shared_ptr<Action> action;
	Transform *transforms;

	struct CullContext
	{
		Node *node;
		float distanceFromCamera;
		render_pass pass;

		CullContext(Node *_node, float _distanceFromCamera, render_pass _pass)
		  : node(_node)
		  , distanceFromCamera(_distanceFromCamera)
		  , pass(_pass)
		{
		}
	};

	class Modifier
	{
	public:
		std::string offset_object;
		V3 relative;
		V3 constant;
		int count;
		bool use_relative;
		bool use_constant;

		void getOffset(std::shared_ptr<Node> _node, M43 &_offset);
	};

	std::vector<Modifier> modifiers;

	std::list<std::shared_ptr<Node>> children;
	std::string name;

	M43 world; // our place in the world
	M43 local; // evaluated transforms
	AABB3 aabb;

	unsigned int inactive : 1;
	unsigned int uncullable : 1;
	unsigned int hidden : 1;
	unsigned int dirty : 1;

	Node()
	  : transforms(nullptr)
	  , inactive(false)
	  , uncullable(false)
	  , hidden(false)
	  , dirty(true)

	{
	}

	virtual ~Node();

	void animate(float _elapsedTimeInSeconds);
	void transform(); // animate, simulate, ...
	void updateAABB();

	auto clone(bool _deep) const -> std::shared_ptr<Node>;

	void render(Rasterizer &_rasterizer);

	// count nodes
	int count() const;
	void clear();

	auto resolve(const std::string &_datapath) -> setter override;

	void cull(const Frustum &_frustum, std::vector<CullContext> &_output);
	void add(std::shared_ptr<Node> _child);
	void remove();
	std::weak_ptr<Node> find(const std::string &_name);
	std::weak_ptr<Node> findInChildren(const std::string &_name);

	void clearTransforms();
	void prependTransform(Transform *_trfm);
	void appendTransform(Transform *_trfm);

	template <typename T> T *findTransform(const std::string &_name)
	{
		for (Transform *trfm = transforms; trfm; trfm = trfm->next)
		{
			if (trfm->name == _name)
				return reinterpret_cast<T *>(trfm);
		}
		return nullptr;
	}

	template <typename T> void get(const std::string &_name, std::function<void(const T &)> _func) const
	{
		for (Transform *trfm = transforms; trfm; trfm = trfm->next)
		{
			if (trfm->name == _name)
			{
				_func(reinterpret_cast<const T &>(*trfm));
			}
		}
	}
	template <typename T> void set(const std::string &_name, std::function<void(T &)> _func)
	{
		for (Transform *trfm = transforms; trfm; trfm = trfm->next)
		{
			if (trfm->name == _name)
			{
				_func(reinterpret_cast<T &>(*trfm));
				dirty = true;
				return;
			}
		}
	}

private:
	void modify(Rasterizer &_rasterizer, std::vector<Modifier>::iterator current, const M43 &_world);
};

#endif
