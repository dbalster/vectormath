/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef TERMINAL_H
#define TERMINAL_H

#include <chrono>
#include <functional>
#include <string>
#include <vector>

enum Keys
{
	KEY_ESCAPE = 27,
	KEY_TAB = 9,
	KEY_ENTER = 10,
	KEY_RETURN = 13,
	KEY_UP = 1000,
	KEY_DOWN,
	KEY_LEFT,
	KEY_RIGHT,
	KEY_HOME,
	KEY_END,
	KEY_INSERT,
	KEY_DELETE,
	KEY_PGUP,
	KEY_PGDOWN,
	KEY_F1,
	KEY_F2,
	KEY_F3,
	KEY_F4,
	KEY_F5,
	KEY_F6,
	KEY_F7,
	KEY_F8,
	KEY_F9,
	KEY_F10,
	MOUSE_WHEEL_DOWN = 2000,
	MOUSE_WHEEL_UP,
	MOUSE_0_DOWN,
	MOUSE_0_UP,
	MOUSE_1_DOWN,
	MOUSE_1_UP,
	MOUSE_2_DOWN,
	MOUSE_2_UP,
	MOUSE_0_MOVE,
	MOUSE_1_MOVE,
	MOUSE_2_MOVE,
};

#if (defined(_WIN32) || defined(WIN32))
#else
#define HAS_TERMIOS
#endif

#if defined(HAS_TERMIOS)
#include <signal.h>
#include <termios.h>
#endif
class Terminal
{
#if defined(HAS_TERMIOS)
	struct termios originalSettings;
#endif

	std::vector<char> buffer;
	int offset{0};
	size_t size{0};
	std::chrono::steady_clock::time_point last_time_point[6];
	int since_last[6]{0, 0, 0, 0, 0, 0}; // for 3 buttons
	std::string sequence;

public:
	int mouseX{0};
	int mouseY{0};
	bool isMouse{false};
	int width{0}, height{0};
	Terminal(size_t _size);
	Terminal(const Terminal &) = delete;
	auto operator=(const Terminal &) -> Terminal & = delete;
	virtual ~Terminal();

	std::function<void(int _width, int _height)> sizeChanged;
	virtual void querySize();

	int read(int timeoutMs);

	auto printf(const char *_fmt, ...) -> size_t;
	auto write(const char *_s) -> size_t;
	auto write(const char *_s, size_t _l) -> size_t;
	auto write(const std::string &_s) -> size_t
	{
		return write(_s.data(), _s.size());
	}
	void flush();

	void setCursor(int _x, int _y);
	void setForeColor(const std::string &_);
	void setBackColor(const std::string &_);
	void setForeColor(uint8_t _r, uint8_t _g, uint8_t _b);
	void setBackColor(uint8_t _r, uint8_t _g, uint8_t _b);
	void hideCursor();
	void showCursor();
	void altScreen();
	void normalScreen();
	void clear();
	void mouseOn();
	void mouseOff();
	void reset();
	void bold();
	void setTitle(const char *_s);
	auto setEcho(bool _on) -> bool;
	auto linebuffered(bool _on) -> bool;

	void enterRawMode();
	void leaveRawMode();

	void init();
	void poll();
};

#endif
