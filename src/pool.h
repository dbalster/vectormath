/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#ifndef POOL_H
#define POOL_H

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

template <typename T> class Pool
{
	static std::unordered_map<std::string, std::shared_ptr<T>> pool;

public:
	static void add(const std::string &_key, std::shared_ptr<T> _value)
	{
		pool[_key] = _value;
	}
	static std::shared_ptr<T> find(const std::string &_key)
	{
		return pool[_key];
	}
	static void remove(const std::string &_key)
	{
		pool.erase(_key);
	}
	static void clear()
	{
		pool.clear();
	}
	static void for_each(std::function<void(std::shared_ptr<T>)> _fn)
	{
		for (auto &o : pool)
		{
			_fn(o.second);
		}
	}
};

template <typename T> std::unordered_map<std::string, std::shared_ptr<T>> Pool<T>::pool;

#endif
