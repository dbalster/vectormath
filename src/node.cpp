/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "node.h"

#include "asset.h"
#include "mesh.h"
#include "rasterizer.h"
#include "scene.h"
#include "transform.h"
#include "material.h"
#include <string>

Node::~Node()
{
	clear();
}

void Node::clear()
{
	action.reset();
	asset.reset();
	parent.reset();
	for (auto &child : children)
	{
		child->clear();
	}
	children.clear();
	modifiers.clear();
	clearTransforms();
}

void Node::cull(const Frustum &_frustum, std::vector<CullContext> &_output)
{
	if (!uncullable && !_frustum.intersects(aabb))
		return;
	if (asset && asset->type() == ASSET_MESH)
	{
		// project the center of the AABB to the NEAR plane of the camera = distance to screen
		float z = _frustum.planes[NEAR].distance(aabb.center[0], aabb.center[1], aabb.center[2]);

		auto mesh = static_pointer_cast<Mesh>(asset);
		for (auto &material : mesh->materials)
		{
			_output.push_back(CullContext(this, z, material->opaque ? PASS_OPAQUE : PASS_TRANSPARENT));
		}
	}
	for (auto child : children)
	{
		child->cull(_frustum, _output);
	}
}

void Node::animate(float _elapsedTimeInSeconds)
{
	if (asset)
	{
		asset->animate(_elapsedTimeInSeconds);
	}
	if (action)
	{
		action->animate(this, _elapsedTimeInSeconds);
		dirty = true;
	}

	for (auto child : children)
		child->animate(_elapsedTimeInSeconds);
}

auto Node::resolve(const std::string &_datapath) -> setter
{
	auto binding = bindings.get(_datapath);
	if (!binding)
	{
		if (transforms != nullptr)
		{
			for (Transform *trfm = transforms; trfm; trfm = trfm->next)
			{
				if (0 == _datapath.compare(0, trfm->name.length(), trfm->name))
				{
					binding = trfm->resolve(_datapath);
					if (binding)
					{
						// todo: not found
						bindings.set(_datapath, binding);
						break;
					}
				}
			}
		}
	}
	return binding;
}

void Node::transform()
{
	if (transforms != nullptr)
	{
		if (dirty)
		{
			local = M43();
			transforms->eval(local);
		}
		dirty = false;
	}

	if (parent != nullptr)
		world = parent->world * local;

	for (auto child : children)
		child->transform();
}

void Node::updateAABB()
{
	if (asset && asset->type() == ASSET_MESH)
	{
		std::shared_ptr<Mesh> mesh = std::static_pointer_cast<Mesh>(asset);

		aabb = mesh->aabb * world;
	}
	for (auto child : children)
	{
		child->updateAABB();
		aabb.merge(child->aabb);
	}
}

void Node::add(std::shared_ptr<Node> _child)
{
	children.push_back(_child);
	_child->parent = shared_from_this();
}

void Node::remove()
{
	if (parent != nullptr)
	{
		parent->children.remove(shared_from_this());
		parent = nullptr;
	}
}

std::weak_ptr<Node> Node::findInChildren(const std::string &_name)
{
	if (name == _name)
		return weak_from_this();
	for (auto child : children)
	{
		auto res = child->findInChildren(_name);
		if (!res.expired())
			return res;
	}
	return std::weak_ptr<Node>();
}

std::weak_ptr<Node> Node::find(const std::string &_name)
{
	auto current = shared_from_this();
	while (current->parent != nullptr)
	{
		current = current->parent;
	}
	if (current)
		return current->findInChildren(_name);
	return std::weak_ptr<Node>();
}

void Node::clearTransforms()
{
	while (transforms != nullptr)
	{
		auto transform = transforms;
		transforms = transforms->next;
		transform->next = nullptr;
		delete transform;
	}
	transforms = nullptr;
}

void Node::prependTransform(Transform *_trfm)
{
	_trfm->next = transforms;
	transforms = _trfm;
}

void Node::appendTransform(Transform *_trfm)
{
	if (transforms == nullptr)
	{
		transforms = _trfm;
	}
	else
	{
		Transform *transform = transforms;
		while (transform->next)
			transform = transform->next;
		transform->next = _trfm;
	}
}

auto Node::clone(bool _deep) const -> std::shared_ptr<Node>
{
	auto node = std::make_shared<Node>();
	static int serial = 1;

	node->name = name + " clone " + std::to_string(serial++);

	// shared asset and action
	node->asset = asset;
	node->action = action;
	node->uncullable = uncullable;
	node->dirty = true;

	node->modifiers = modifiers;

	// transforms are not shared, so we have to clone them
	Transform *last = nullptr;
	for (Transform *trfm = transforms; trfm; trfm = trfm->next)
	{
		auto copy = trfm->clone();
		if (last)
		{
			last->next = copy;
		}
		else
		{
			node->transforms = copy;
		}
		last = copy;
	}

	if (_deep)
	{
		for (auto &child : children)
		{
			node->add(child->clone(_deep));
		}
	}

	return node;
}

void Node::Modifier::getOffset(std::shared_ptr<Node> _node, M43 &_offset)
{
	auto oo = _node->find(offset_object);
	if (use_relative)
	{
		// TODO: relative to object's bounding box!
		_offset = M43::translate(relative);
	}
	if (use_constant)
	{
		_offset = _offset * M43::translate(constant);
	}
	if (auto node = oo.lock())
	{
		_offset = _offset * node->local;
	}
}

void Node::modify(Rasterizer &_rasterizer, std::vector<Modifier>::iterator current, const M43 &_world)
{
	if (current == modifiers.end())
		return;

	Modifier &modifier = *current;
	M43 offset;
	current->getOffset(shared_from_this(), offset);
	M43 trfm = _world;
	for (int i = 0; i < modifier.count; ++i)
	{
		asset->render(_rasterizer, trfm);
		modify(_rasterizer, std::next(current), trfm);
		trfm = offset * trfm; // * offset;
	}
}

int Node::count() const
{
	int n = 1; // self
	for (auto &child : children)
	{
		n += child->count();
	}
	return n;
}

void Node::render(Rasterizer &_rasterizer)
{
	if (asset == nullptr || hidden)
		return;

	// either render directly or through the modifier stack

	if (modifiers.empty())
	{
		asset->render(_rasterizer, world);
	}
	else
	{
		modify(_rasterizer, modifiers.begin(), world);
	}
}
