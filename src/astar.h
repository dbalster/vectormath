#ifndef ASTAR_H
#define ASTAR_H

#include <algorithm>
#include <functional>
#include <queue>
#include <unordered_map>
#include <vector>

/*
	basic A*
*/

enum SearchState
{
	NOT_FOUND,
	FOUND,
	SEARCHING,
};

template <typename Cell, typename Cost> class AStar
{
public:
	using HeuristicFunction = std::function<Cost(const Cell &, const Cell &)>;
	using CostFunction = std::function<Cost(const Cell &, const Cell &)>;
	using NeighborFunction = std::function<void(std::vector<Cell> &, const Cell &)>;

	struct PriorityQueueNode
	{
		Cell cell;
		Cost gScore;
		Cost fScore;

		bool operator<(const PriorityQueueNode &other) const
		{
			return fScore > other.fScore;
		}
	};

	AStar(HeuristicFunction h, CostFunction c, NeighborFunction n)
	  : heuristic(h)
	  , cost(c)
	  , nearby(n)
	{
	}

	void begin(const Cell &_start, const Cell &_goal)
	{
		start = _start;
		goal = _goal;

		clear();
		totalSteps = 0;

		openSet.push({start, 0, heuristic(start, goal)});
		gScoreMap[start] = 0;
	}

	void clear()
	{
		openSet = std::priority_queue<PriorityQueueNode>();
		cameFrom.clear();
		gScoreMap.clear();
		neighbors.clear();
	}

	inline int getTotalSteps() const
	{
		return totalSteps;
	}

	SearchState search(int _steps)
	{
		while (!openSet.empty() && _steps > 0)
		{
			_steps--;
			totalSteps++;
			auto current = openSet.top().cell;
			if (current == goal)
			{
				path.clear();
				while (cameFrom.find(current) != cameFrom.end())
				{
					path.push_back(current);
					current = cameFrom[current];
				}
				path.push_back(current);
				std::reverse(path.begin(), path.end());
				return FOUND;
			}

			openSet.pop();

			neighbors.clear();
			nearby(neighbors, current);
			for (const auto &neighbor : neighbors)
			{
				auto tentative_gScore = gScoreMap[current] + cost(current, neighbor);
				if (gScoreMap.find(neighbor) == gScoreMap.end() || tentative_gScore < gScoreMap[neighbor])
				{
					cameFrom[neighbor] = current;
					gScoreMap[neighbor] = tentative_gScore;
					openSet.push({neighbor, tentative_gScore, tentative_gScore + heuristic(neighbor, goal)});
				}
			}
		}

		return openSet.empty() ? NOT_FOUND : SEARCHING;
	}

	inline const std::vector<Cell> &result() const
	{
		return path;
	}

	std::vector<Cell> debug()
	{
		std::vector<Cell> p;
		auto current = openSet.top().cell;
		while (cameFrom.find(current) != cameFrom.end())
		{
			p.push_back(current);
			current = cameFrom[current];
		}
		p.push_back(current);
		std::reverse(p.begin(), p.end());
		return p;
	}

private:
	HeuristicFunction heuristic;
	CostFunction cost;
	NeighborFunction nearby;

	std::priority_queue<PriorityQueueNode> openSet;
	std::unordered_map<Cell, Cell, typename Cell::Hash> cameFrom;
	std::unordered_map<Cell, Cost, typename Cell::Hash> gScoreMap;
	std::vector<Cell> neighbors;
	Cell start;
	Cell goal;
	int totalSteps;
	std::vector<Cell> path;
};

#endif
