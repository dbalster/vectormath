#ifndef GOAP_H
#define GOAP_H

#include "blackboard.h"
#include <functional>
#include <initializer_list>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace GOAP
{

using WorldStateMap = std::unordered_map<std::string, bool>;
struct WorldState : public WorldStateMap
{
	WorldState()
	  : WorldStateMap()
	{
	}
	WorldState(std::initializer_list<WorldStateMap::value_type> x)
	  : WorldStateMap(x)
	{
	}
	struct Hash
	{
		size_t operator()(const WorldState &node) const
		{
			return node.size();
		}
	};
};

class Action
{
public:
	virtual ~Action() = default;
	virtual bool checkPreconditions(const WorldState &state) const = 0;
	virtual void applyEffects(WorldState &state) const = 0;
	virtual float getCost() const = 0;
	virtual void execute() = 0;
};

class Goal
{
public:
	virtual ~Goal() = default;
	virtual bool isSatisfied(const WorldState &state) const = 0;
	virtual WorldState getRequiredConditions() const = 0;
	// Weitere Funktionen nach Bedarf};
};

using ActionList = std::vector<std::shared_ptr<Action>>;
class Planner
{
public:
	Planner();
	virtual ~Planner();
	ActionList plan(const WorldState &state, std::shared_ptr<Goal> goal);
};

}; // namespace GOAP

#endif
