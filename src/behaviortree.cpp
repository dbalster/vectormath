#include "behaviortree.h"

#include <bits/chrono.h>
#include <chrono>
#include <iostream>
#include <memory>

using namespace std::chrono;

Behaviortree::Node::Node()
  : Hashed()
{
}

Behaviortree::Node::~Node()
{
}

bool Behaviortree::Node::open(ContextWithBlackboard &_context) const
{
	_context[hash]["_"] = true;
	_context[hash]["$"] = ERROR;
	return true;
}

void Behaviortree::Node::close(ContextWithBlackboard &_context) const
{
	_context[hash]["_"] = false;
}

Behaviortree::Status Behaviortree::Node::run(ContextWithBlackboard &_context) const
{
	auto status = ERROR;

	if (!_context.contains(hash))
	{
		_context[hash]["_"] = false;
		_context[hash]["$"] = ERROR;
	}

	if (!std::any_cast<bool>(_context[hash]["_"]))
	{
		if (!open(_context))
			return FAILURE;
	}
	status = execute(_context);
	_context[hash]["$"] = status;

	if (status != RUNNING)
	{
		if (std::any_cast<bool>(_context[hash]["_"]))
		{
			close(_context);
		}
	}

	return status;
}

Behaviortree::Status Behaviortree::Inverter::execute(ContextWithBlackboard &_context) const
{
	if (child == nullptr)
		return ERROR;
	auto status = child->run(_context);
	if (status == SUCCESS)
		return FAILURE;
	if (status == FAILURE)
		return SUCCESS;
	return status;
}

Behaviortree::Status Behaviortree::Failer::execute(ContextWithBlackboard &_context) const
{
	return FAILURE;
}

Behaviortree::Status Behaviortree::Succeeder::execute(ContextWithBlackboard &_context) const
{
	return SUCCESS;
}

bool Behaviortree::Waiter::open(ContextWithBlackboard &_context) const
{
	_context[hash]["now"] = duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch());
	return Node::open(_context);
}

Behaviortree::Status Behaviortree::Waiter::execute(ContextWithBlackboard &_context) const
{
	auto saved_time_ms = std::any_cast<milliseconds>(_context[hash]["now"]);

	auto saved_time = high_resolution_clock::time_point(milliseconds(saved_time_ms));
	auto now = high_resolution_clock::now();

	if (duration_cast<milliseconds>(now - saved_time) < delay)
	{
		return RUNNING;
	}
	return SUCCESS;
}

bool Behaviortree::Repeater::open(ContextWithBlackboard &_context) const
{
	_context[hash]["i"] = 0;
	return Node::open(_context);
}

Behaviortree::Status Behaviortree::Repeater::execute(ContextWithBlackboard &_context) const
{
	if (child == nullptr)
		return ERROR;

	auto i = std::any_cast<int>(_context[hash]["i"]);

	if (i < count)
	{
		auto status = child->run(_context);
		switch (status)
		{
		case SUCCESS:
			_context[hash]["i"] = i + 1;
			return RUNNING;
		case FAILURE:
			_context[hash]["i"] = count + 1;
			return FAILURE;
		case RUNNING:
			return RUNNING;
		case ERROR:
			return ERROR;
		}
	}
	return (i > count) ? FAILURE : SUCCESS;
}

Behaviortree::Status Behaviortree::Selector::execute(ContextWithBlackboard &_context) const
{
	for (auto &child : children)
	{
		auto status = child->run(_context);
		if (status != FAILURE)
			return status;
	}
	return FAILURE;
}

Behaviortree::Status Behaviortree::Sequence::execute(ContextWithBlackboard &_context) const
{
	for (auto &child : children)
	{
		auto status = child->run(_context);
		if (status != SUCCESS)
			return status;
	}
	return SUCCESS;
}

bool Behaviortree::MemSequence::open(ContextWithBlackboard &_context) const
{
	_context[hash]["i"] = 0UL;
	return Composite::open(_context);
}

Behaviortree::Status Behaviortree::MemSequence::execute(ContextWithBlackboard &_context) const
{
	if (children.empty())
		return ERROR;

	for (auto index = std::any_cast<size_t>(_context[hash]["i"]); index < children.size(); ++index)
	{
		auto child = children.at(index);
		auto status = child->run(_context);
		if (status != SUCCESS)
		{
			if (status == RUNNING)
			{
				_context[hash]["i"] = index;
			}
			return status;
		}
	}

	return SUCCESS;
}

bool Behaviortree::MemSelector::open(ContextWithBlackboard &_context) const
{
	_context[hash]["i"] = 0UL;
	return Composite::open(_context);
}

Behaviortree::Status Behaviortree::MemSelector::execute(ContextWithBlackboard &_context) const
{
	if (children.empty())
		return ERROR;

	for (auto index = std::any_cast<size_t>(_context[hash]["i"]); index < children.size(); ++index)
	{
		auto child = children.at(index);
		auto status = child->run(_context);
		if (status != FAILURE)
		{
			if (status == RUNNING)
			{
				_context[hash]["i"] = index;
			}
			return status;
		}
	}

	return FAILURE;
}

auto Behaviortree::Executor::execute(ContextWithBlackboard &_context) const -> Status
{
	int n_success = 0;
	int n_failure = 0;
	int n_children = 0;
	for (auto &child : children)
	{
		auto status = child->run(_context);
		if (status == SUCCESS)
		{
			n_success++;
		}
		else if (status == FAILURE)
		{
			n_failure++;
		}
		else if (status == ERROR)
		{
			return status;
		}
		n_children++;
	}

	if (n_success >= n_children)
		return SUCCESS;
	if (n_failure >= n_children)
		return FAILURE;
	return RUNNING;
}

Behaviortree::Status Behaviortree::Printer::execute(ContextWithBlackboard &_context) const
{
	std::cout << text << "\n";
	return SUCCESS;
}
