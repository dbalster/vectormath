winget install git.git
winget install Microsoft.VisualStudioCode
winget install LLVM.LLVM
winget install meson
winget install ninja-build.ninja

[System.Environment]::SetEnvironmentVariable('CC','clang.exe', 'User')
[System.Environment]::SetEnvironmentVariable('CXX','clang++.exe', 'User')

function updatePath {
	Param(
  	[string]$component
    ) 

	$path = [Environment]::GetEnvironmentVariable("PATH", [EnvironmentVariableTarget]::User)
	if (-not $path.Split(';').Contains($component)) {
			$path = $path + ";" + $component
			[Environment]::SetEnvironmentVariable("PATH", $path, [EnvironmentVariableTarget]::User)
	}
	# update and use path
	$env:Path = [Environment]::GetEnvironmentVariable("PATH", [EnvironmentVariableTarget]::User)
}

updatePath("C:\Program Files\LLVM\bin")
updatePath("C:\Program Files\Meson")

git submodule update --init
meson wrap install catch2
meson setup builddir --buildtype=debug
meson compile -C builddir main
