FROM debian:12

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    git \
    clang \
    meson \
    ninja-build \
		libglib2.0-dev \
		libfreetype-dev \
		autoconf automake libtool \
    curl \
    clang-format \
		pkg-config \
    clangd \
		cmake \
    gnupg \
    ca-certificates \
    apt-transport-https \
    lsb-release

WORKDIR /
RUN git clone https://github.com/hpjansson/chafa.git
WORKDIR /chafa
RUN ./autogen.sh
RUN ./configure --prefix=/usr --without-tools --without-avif --without-jpeg --without-svg --without-tiff --without-webp 
RUN make && make install

RUN curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor -o microsoft.gpg
RUN mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
RUN sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
RUN apt-get update && apt-get install -y code
RUN apt-get update && apt-get install -y ccache

RUN curl -fsSL https://code-server.dev/install.sh | sh

WORKDIR /
RUN git clone https://codeberg.org/dbalster/vectormath.git
WORKDIR /vectormath
RUN rm -f builddir
ENV CC=clang
ENV CXX=clang++
RUN sh ./setup.sh
EXPOSE 4443
ENTRYPOINT ["code-server", "--bind-addr", "0.0.0.0:4443","--cert", "--auth", "none"]

