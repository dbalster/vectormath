# a 3D engine for ANSI terminals

![Screenshot](screencast.gif)

This is a simple software rasterizer that renders using ANSI escape sequences. The code features

- instead of ascii dithering, it always prints the unicode half-block glyph and sets the fore and background color in full true color. this doubles the vertical pixel resolution.

- written in plain C++
- uses meson for project files
- provides a simple Dockerfile, if needed
- custom blender exporter (python)
- supports texture mapping and transparency
- supports blender keyframe animation for various properties

		* location
		* rotation 
		* scale
		* diffuse color, alpha

- has some basic unit tests (catch2)
- uses the greate stb_image libary
 
# setup

- clone this repository

## windows
- run the setup.ps1 powershell script

## linux
- run the setup.sh bash script (assuming debian)

# roadmap!

- I've just added chafa (https://github.com/hpjansson/chafa) which is briliant and boosts the output a lot. However, it made it less portable, so I plan of creating a custom alternative to chafa: since I know the target resolution I can allocate a perfect fit internal color buffer. that means I do not need to up/downscale or sample to target resolution. instead, I can directly take the pixels and use a dictionary to lookup the perfect glyph. basic idea would be: create a tool that analyses a font, ie python, freetype, get all glpyhs, sort them by coverage. find a good lookup method (k-means etc) and done. 

