#!/usr/bin/make

all::
	meson compile -C builddir main

init::
	meson setup builddir --reconfigure --buildtype=minsize


run::
	./builddir/main

test::
	./builddir/main_test

# use like this:
# CXX=gcc-12 CXXFLAGS="-fsanitize=address" LDFLAGS="-lm -lstdc++"  make release
# CXX=clang-15 CXXFLAGS="-fsanitize=address" LDFLAGS="-lm -lstdc++"  make release

release::
	rm -rf builddir
	meson setup builddir --buildtype=release
	meson compile -C builddir main

debug::
	rm -rf builddir
	meson setup builddir --buildtype=debug
	meson compile -C builddir main


clean::
	ninja clean -C builddir
