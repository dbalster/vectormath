git submodule update --init
mkdir -p subprojects
meson wrap install catch2
meson setup builddir --buildtype=debug
meson compile -C builddir main
