/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "common.h"

using namespace Catch;
using namespace Catch::Matchers;

static M3 unit{1, 0, 0, 0, 1, 0, 0, 0, 1};

class CheckM3 : public MatcherBase<M3>
{
	M3 m;

public:
	CheckM3(const M3 &_m)
	  : m(_m)
	{
	}

	static inline bool eq(float _a, float _b)
	{
		return std::fabs(_b - _a) < 0.0001f;
	}

	bool match(const M3 &_m) const override
	{
		return eq(m.m[0][0], _m.m[0][0]) && eq(m.m[0][1], _m.m[0][1]) && eq(m.m[0][2], _m.m[0][2]) && eq(m.m[1][0], _m.m[1][0]) && eq(m.m[1][1], _m.m[1][1]) &&
			   eq(m.m[1][2], _m.m[1][2]) && eq(m.m[2][0], _m.m[2][0]) && eq(m.m[2][1], _m.m[2][1]) && eq(m.m[2][2], _m.m[2][2]);
	}

	std::string describe() const override
	{
		std::ostringstream ss;
		ss << m;
		return ss.str();
	}
};

// The builder function
inline CheckM3 IsEqual(const M3 &_m)
{
	return CheckM3(_m);
}

TEST_CASE("M3 is unit by default")
{
	M3 m;

	REQUIRE_THAT(m, IsEqual(unit));
}

TEST_CASE("M3 construct")
{
	M3 m(1, 2, 3, 4, 5, 6, 7, 8, 9);

	REQUIRE_THAT(m, IsEqual({1, 2, 3, 4, 5, 6, 7, 8, 9}));
}

TEST_CASE("M3 * M3")
{
	/*
		to verify, the 3rd row is printed here, too

		t =
	  ⎧ 1 0 1 ⎫
	  ⎪ 0 1 2 ⎪
		⎩ 0 0 1 ⎭
		s =
	  ⎧ 3 0 0 ⎫
	  ⎪ 0 4 0 ⎪
		⎩ 0 0 1 ⎭
		s*t =
	  ⎧ 1 0 1 ⎫⎧ 3 0 0 ⎫⎧ 3 0 1 ⎫
	  ⎪ 0 1 2 ⎪⎪ 0 4 0 ⎪⎪ 0 4 2 ⎪
		⎩ 0 0 1 ⎭⎩ 0 0 1 ⎭⎩ 0 0 1 ⎭
		t*s =
	  ⎧ 1 0 1 ⎫⎧ 3 0 0 ⎫⎧ 3 0 3 ⎫
	  ⎪ 0 1 2 ⎪⎪ 0 4 0 ⎪⎪ 0 4 8 ⎪
		⎩ 0 0 1 ⎭⎩ 0 0 1 ⎭⎩ 0 0 1 ⎭
	*/

	M3 a, b, c;
	c = a * b;

	REQUIRE_THAT(c, IsEqual(unit));
}

TEST_CASE("M3.invert unit")
{
	M3 m;

	m.invert();

	REQUIRE_THAT(m, IsEqual(unit));
}

TEST_CASE("M3.transpose")
{
	M3 m = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	m.transpose();

	REQUIRE_THAT(m, IsEqual({1, 4, 7, 2, 5, 8, 3, 6, 9}));
}

TEST_CASE("M3.determinant")
{
	M3 m = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	float a = m.determinant();

	float b = unit.determinant();

	REQUIRE(a == 0.0f);
	REQUIRE(b == 1.0f);
}

TEST_CASE("M3.transform")
{
	M3 m = {1, 0, 3, 0, 1, 3, 0, 0, 1};

	auto r = m * V3{1, 1, 0};

	REQUIRE(r.x == 1.0f);
	REQUIRE(r.y == 1.0f);
	REQUIRE(r.z == 6.0f);
}

// todo:
// op* V2
// transform
// determinant
