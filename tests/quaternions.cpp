/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "common.h"

TEST_CASE("Q - Default constructor initializes to identity quaternion", "[Q]")
{
	Q quaternion;
	REQUIRE(quaternion.x == Approx(0.0f));
	REQUIRE(quaternion.y == Approx(0.0f));
	REQUIRE(quaternion.z == Approx(0.0f));
	REQUIRE(quaternion.w == Approx(1.0f));
}

// Test Case: Norm of a Quaternion
TEST_CASE("Q - Norm calculation is correct", "[Q]")
{
	Q quaternion(1.0f, 2.0f, 3.0f, 4.0f);
	float expectedNorm = 1.0f * 1.0f + 2.0f * 2.0f + 3.0f * 3.0f + 4.0f * 4.0f;
	REQUIRE(quaternion.norm() == Approx(expectedNorm));
}

// Test Case: Normalization of Quaternion
TEST_CASE("Q - Normalization of quaternion is correct", "[Q]")
{
	Q quaternion(1.0f, 2.0f, 3.0f, 4.0f);
	Q normalizedQuat = quaternion.normalized();

	float len = quaternion.length();
	REQUIRE(normalizedQuat.x == Approx(quaternion.x / len));
	REQUIRE(normalizedQuat.y == Approx(quaternion.y / len));
	REQUIRE(normalizedQuat.z == Approx(quaternion.z / len));
	REQUIRE(normalizedQuat.w == Approx(quaternion.w / len));
}

// Test Case: Inverse of Quaternion
TEST_CASE("Q - Inverse calculation is correct", "[Q]")
{
	Q quaternion(1.0f, 2.0f, 3.0f, 4.0f);
	Q inverseQuat = quaternion.inversed();

	std::cout << quaternion << inverseQuat;

	REQUIRE(inverseQuat.x == Approx(-quaternion.x / quaternion.norm()));
	REQUIRE(inverseQuat.y == Approx(-quaternion.y / quaternion.norm()));
	REQUIRE(inverseQuat.z == Approx(-quaternion.z / quaternion.norm()));
	REQUIRE(inverseQuat.w == Approx(quaternion.w / quaternion.norm()));
}

// Test Case: Quaternion Multiplication
TEST_CASE("Q - Quaternion multiplication is correct", "[Q]")
{
	Q q1(1.0f, 0.0f, 0.0f, 0.0f);
	Q q2(0.0f, 1.0f, 0.0f, 0.0f);
	Q result = q1 * q2;

	REQUIRE(result.x == Approx(0.0f));
	REQUIRE(result.y == Approx(0.0f));
	REQUIRE(result.z == Approx(1.0f));
	REQUIRE(result.w == Approx(0.0f));
}

// Test Case: Conversion from Axis-Angle to Quaternion
TEST_CASE("Q - Conversion from axis-angle to quaternion is correct", "[Q]")
{
	V3 axis(0.0f, 1.0f, 0.0f); // Y-axis
	float angle = M_PI / 2;	   // 90 degrees

	Q quaternionFromAxisAngle = Q::from_axis_angle(axis, angle);

	REQUIRE(quaternionFromAxisAngle.x == Approx(0.0f));
	REQUIRE(quaternionFromAxisAngle.y == Approx(sin(angle / 2)));
	REQUIRE(quaternionFromAxisAngle.z == Approx(0.0f));
	REQUIRE(quaternionFromAxisAngle.w == Approx(cos(angle / 2)));
}

// Test Case: Conversion from Euler Angles to Quaternion
TEST_CASE("Q - Conversion from euler angles to quaternion is correct", "[Q]")
{
	V3 eulers(M_PI / 2, M_PI / 2, 0.0f); // 90 degrees for X and Y axes

	Q quaternionFromEuler = Q::from_euler(eulers);

	// We need specific checks depending on the convention used for Euler angles -> quaternion conversion
	// Here is an example assuming the common 'XYZ' order of rotations
	REQUIRE(quaternionFromEuler.x == Approx(sqrt(0.5f)));
	REQUIRE(quaternionFromEuler.y == Approx(sqrt(0.5f)));
	REQUIRE(quaternionFromEuler.z == Approx(0.0f));
	REQUIRE(quaternionFromEuler.w == Approx(0.0f));
}
