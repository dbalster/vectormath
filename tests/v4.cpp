/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "common.h"

TEST_CASE("V4 methods and operators")
{
	SECTION("V4 Default Constructor Initializes to Zero")
	{
		V4 v;
		REQUIRE(v.x == 0.0F);
		REQUIRE(v.y == 0.0F);
		REQUIRE(v.z == 0.0F);
		REQUIRE(v.w == 0.0F);
	}

	SECTION("V4 Constructor with Parameters")
	{
		V4 v(1.0F, -1.0F, 2.0F, 3.0F);
		REQUIRE(v.x == 1.0F);
		REQUIRE(v.y == -1.0F);
		REQUIRE(v.z == 2.0F);
		REQUIRE(v.w == 3.0F);
	}

	SECTION("V4 Copy Constructor")
	{
		V4 original(1.0F, -1.0F, 2.0F, 3.0F);
		V4 copied(original);

		REQUIRE(copied.x == original.x);
		REQUIRE(copied.y == original.y);
		REQUIRE(copied.z == original.z);
		REQUIRE(copied.w == original.w);
	}

	SECTION("V4 Subtraction Operator")
	{
		V4 result = V4(1.0F, 2.0F, 3.0F, 4.0F) - V4(5.0F, 6.0F, 7.0F, 8.0F);

		REQUIRE(result.x == Approx(-4.0F).margin(EPSILON));
		REQUIRE(result.y == Approx(-4.0F).margin(EPSILON));
		REQUIRE(result.z == Approx(-4.0F).margin(EPSILON));
		REQUIRE(result.w == Approx(-4.0F).margin(EPSILON));
	}

	SECTION("V4 Addition Operator")
	{
		V4 result = V4(1.0F, 2.0F, 3.0F, 4.0F) + V4(5.0F, 6.0F, 7.0F, 8.0F);

		REQUIRE(result.x == Approx(6.0F).margin(EPSILON));
		REQUIRE(result.y == Approx(8.0F).margin(EPSILON));
		REQUIRE(result.z == Approx(10.0F).margin(EPSILON));
		REQUIRE(result.w == Approx(12.0F).margin(EPSILON));
	}

	SECTION("V4 Scalar Multiplication")
	{
		V4 result = V4(1.0F, 2.0F, 3.0F, 4.0F) * 5.0F;

		REQUIRE(result.x == Approx(5.0F).margin(EPSILON));
		REQUIRE(result.y == Approx(10.0F).margin(EPSILON));
		REQUIRE(result.z == Approx(15.0F).margin(EPSILON));
		REQUIRE(result.w == Approx(20.0F).margin(EPSILON));
	}

	SECTION("V4 Multiplication Operator (Dot Product)")
	{
		float zeroProduct = V4(1.0F, 0.0F, 0.0F, 0.0F) * V4(0.0F, 1.0F, 0.0F, 0.0F);
		float oneProduct = V4(1.0F, 0.0F, 0.0F, 0.0F) * V4(1.0F, 0.0F, 0.0F, 0.0F);

		REQUIRE(zeroProduct == 0.0F);
		REQUIRE(oneProduct == 1.0F);
	}

	SECTION("V4 Dot Method")
	{
		float zeroDot = V4(1.0F, 0.0F, 0.0F, 0.0F).dot(V4(0.0F, 1.0F, 0.0F, 0.0F));
		float oneDot = V4(1.0F, 0.0F, 0.0F, 0.0F).dot(V4(1.0F, 0.0F, 0.0F, 0.0F));

		REQUIRE(zeroDot == 0.0F);
		REQUIRE(oneDot == 1.0F);
	}

	SECTION("V4 Bitwise OR Operator (Dot Product)")
	{
		float zeroBitwiseOR = V4(1.0F, 0.0F, 0.0F, 0.0F) | V4(0.0F, 1.0F, 0.0F, 0.0F);
		float oneBitwiseOR = V4(1.0F, 0.0F, 0.0F, 0.0F) | V4(1.0F, 0.0F, 0.0F, 0.0F);

		REQUIRE(zeroBitwiseOR == 0.0F);
		REQUIRE(oneBitwiseOR == 1.0F);
	}

	SECTION("V4 Addition Assignment Operator")
	{
		V4 a(1.0F, 2.0F, 3.0F, 4.0F);
		a += V4(5.0F, 6.0F, 7.0F, 8.0F);

		REQUIRE(a.x == Approx(6.0F).margin(EPSILON));
		REQUIRE(a.y == Approx(8.0F).margin(EPSILON));
		REQUIRE(a.z == Approx(10.0F).margin(EPSILON));
		REQUIRE(a.w == Approx(12.0F).margin(EPSILON));
	}

	SECTION("V4 Subtraction Assignment Operator")
	{
		V4 a(1.0F, 2.0F, 3.0F, 4.0F);
		a -= V4(5.0F, 6.0F, 7.0F, 8.0F);

		REQUIRE(a.x == Approx(-4.0F).margin(EPSILON));
		REQUIRE(a.y == Approx(-4.0F).margin(EPSILON));
		REQUIRE(a.z == Approx(-4.0F).margin(EPSILON));
		REQUIRE(a.w == Approx(-4.0F).margin(EPSILON));
	}

	SECTION("V4 Unary Minus Operator")
	{
		V4 a(1.0F, 2.0F, 3.0F, 4.0F);
		V4 b = -a;

		REQUIRE(b.x == Approx(-1.0F).margin(EPSILON));
		REQUIRE(b.y == Approx(-2.0F).margin(EPSILON));
		REQUIRE(b.z == Approx(-3.0F).margin(EPSILON));
		REQUIRE(b.w == Approx(-4.0F).margin(EPSILON));
	}

	SECTION("V4 Norm Method")
	{
		V4 a(1.0F, 0.0F, 0.0F, 0.0F), b(1.0F, 1.0F, 1.0F, 1.0F), c(1.0F, 2.0F, 3.0F, 4.0F);
		float r1 = a.norm();
		float r2 = b.norm();
		float r3 = c.norm();

		REQUIRE(r1 == 1.0F);
		REQUIRE(r2 == 4.0F);
		REQUIRE(r3 == 30.0F);
	}

	SECTION("V4 Length Method")
	{
		V4 a(1.0F, 0.0F, 0.0F, 0.0F), b(1.0F, 1.0F, 1.0F, 1.0F), c(1.0F, 2.0F, 3.0F, 4.0F);
		float r1 = a.length();
		float r2 = b.length();
		float r3 = c.length();

		REQUIRE(r1 == Approx(1.0F).margin(EPSILON));
		REQUIRE(r2 == Approx(2.0F).margin(EPSILON));
		REQUIRE(r3 == Approx(5.47723F).margin(EPSILON)); // sqrt(1^2 + 2^2 + 3^2 + 4^2)
	}

	// ... The rest of your test code would go above

	SECTION("V4 Normalized Method")
	{
		V4 a(10.0F, 0.0F, 0.0F, 0.0F), b(1.0F, 1.0F, 1.0F, 1.0F), c(1.0F, 2.0F, 3.0F, 4.0F);
		auto r1 = a.normalized();
		auto r2 = b.normalized();
		auto r3 = c.normalized();

		REQUIRE(r1.length() == Approx(1.0F).margin(EPSILON));
		REQUIRE(r2.length() == Approx(1.0F).margin(EPSILON));
		REQUIRE(r3.length() == Approx(1.0F).margin(EPSILON));
		// No test for zero vector normalization as it may not be defined
	}

	SECTION("Static Distance Method Between Two V4 Vectors")
	{
		float distance = V4::distance(V4(1.0F, 1.0F, 1.0F, 1.0F), V4(-1.0F, -1.0F, -1.0F, -1.0F));
		float zeroDistance = V4::distance(V4(1.0F, 2.0F, 3.0F, 4.0F), V4(1.0F, 2.0F, 3.0F, 4.0F));

		REQUIRE(distance == Approx(sqrtf(16.0F)).margin(EPSILON)); // sqrt((2^2) * 4)
		REQUIRE(zeroDistance == Approx(0.0F).margin(EPSILON));
	}

	SECTION("Constructing V4 from V3 and W Component")
	{
		V3 v(1.0F, 2.0F, 3.0F);
		V4 w(v, 4.0F);

		REQUIRE(w.x == Approx(1.0F).margin(EPSILON));
		REQUIRE(w.y == Approx(2.0F).margin(EPSILON));
		REQUIRE(w.z == Approx(3.0F).margin(EPSILON));
		REQUIRE(w.w == Approx(4.0F).margin(EPSILON));
	}

	SECTION("Converting V4 to Homogeneous Coordinates")
	{
		V4 v(1.0F, 2.0F, 3.0F, 3.0F);
		auto r = v.homogeneous();

		REQUIRE(r.x == Approx(1.0F / 3.0F).margin(EPSILON)); // x/w
		REQUIRE(r.y == Approx(2.0F / 3.0F).margin(EPSILON)); // y/w
		REQUIRE(r.z == Approx(1.0F).margin(EPSILON));		 // z/w (should be 1 because z = w)
	}

	SECTION("Cross Product of V4 Vectors")
	{
		V4 x{1.0F, 0.0F, 0.0F, 0.0F};
		V4 y{0.0F, 1.0F, 0.0F, 0.0F};
		V4 z{0.0F, 0.0F, 1.0F, 0.0F};

		auto crossProduct = x.cross(y);

		REQUIRE(crossProduct.x == Approx(z.x).margin(EPSILON));
		REQUIRE(crossProduct.y == Approx(z.y).margin(EPSILON));
		REQUIRE(crossProduct.z == Approx(z.z).margin(EPSILON));
		// Not requiring exact equality since cross product might have slight floating-point variations
	}

	SECTION("Bitwise XOR Operator for Cross Product of V4 Vectors")
	{
		V4 x{1.0F, 0.0F, 0.0F, 0.0F};
		V4 y{0.0F, 1.0F, 0.0F, 0.0F};
		V4 z{0.0F, 0.0F, 1.0F, 0.0F};

		auto result = x ^ y;

		REQUIRE(result.x == Approx(z.x).margin(EPSILON));
		REQUIRE(result.y == Approx(z.y).margin(EPSILON));
		REQUIRE(result.z == Approx(z.z).margin(EPSILON));
	}
}
