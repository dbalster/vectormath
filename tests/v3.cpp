/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "common.h"

TEST_CASE("Vector initialization", "[vectormath]")
{
	SECTION("Default constructor")
	{
		V3 v;
		REQUIRE(v.x == Approx(0.0f).margin(EPSILON));
		REQUIRE(v.y == Approx(0.0f).margin(EPSILON));
		REQUIRE(v.z == Approx(0.0f).margin(EPSILON));
	}

	SECTION("Parameterized constructor")
	{
		V3 v(1.0f, -2.0f, 3.0f);
		REQUIRE(v.x == Approx(1.0f).margin(EPSILON));
		REQUIRE(v.y == Approx(-2.0f).margin(EPSILON));
		REQUIRE(v.z == Approx(3.0f).margin(EPSILON));
	}

	SECTION("Copy constructor")
	{
		V3 orig(1.0f, -2.0f, 3.0f);
		V3 copy(orig);
		REQUIRE(copy.x == Approx(orig.x).margin(EPSILON));
		REQUIRE(copy.y == Approx(orig.y).margin(EPSILON));
		REQUIRE(copy.z == Approx(orig.z).margin(EPSILON));
	}
}

TEST_CASE("Vector arithmetic", "[vectormath]")
{
	V3 a(1.0f, 2.0f, 3.0f);
	V3 b(-1.0f, -2.0f, -3.0f);
	float scalar = 2.0f;

	SECTION("Addition")
	{
		V3 result = a + b; // Should be (0, 0, 0)
		REQUIRE(result.x == Approx(0.0f).margin(EPSILON));
		REQUIRE(result.y == Approx(0.0f).margin(EPSILON));
		REQUIRE(result.z == Approx(0.0f).margin(EPSILON));
	}

	SECTION("Subtraction")
	{
		V3 result = a - b; // Should be (2, 4, 6)
		REQUIRE(result.x == Approx(2.0f).margin(EPSILON));
		REQUIRE(result.y == Approx(4.0f).margin(EPSILON));
		REQUIRE(result.z == Approx(6.0f).margin(EPSILON));
	}

	SECTION("Scalar multiplication")
	{
		V3 result = a * scalar; // Should be (2, 4, 6)
		REQUIRE(result.x == Approx(2.0f).margin(EPSILON));
		REQUIRE(result.y == Approx(4.0f).margin(EPSILON));
		REQUIRE(result.z == Approx(6.0f).margin(EPSILON));
	}

	SECTION("Dot product")
	{
		float result = a.dot(b); // Should be -14
		REQUIRE(result == Approx(-14.0f).margin(EPSILON));
	}

	SECTION("Cross product")
	{
		V3 c(0.0f, 1.0f, 0.0f);
		V3 d(0.0f, 0.0f, 1.0f);
		V3 result = c ^ d; // Should be (1, 0, 0)
		REQUIRE(result.x == Approx(1.0f).margin(EPSILON));
		REQUIRE(result.y == Approx(0.0f).margin(EPSILON));
		REQUIRE(result.z == Approx(0.0f).margin(EPSILON));
	}
}

TEST_CASE("Vector operations", "[vectormath]")
{
	V3 a(1.0f, 0.0f, 0.0f);
	V3 b(0.0f, 1.0f, 0.0f);
	V3 c(0.0f, 0.0f, 1.0f);

	SECTION("Normalization")
	{
		a.normalize();
		REQUIRE(a.length() == Approx(1.0f).margin(EPSILON));

		V3 zeroVec;
		zeroVec.normalize(); // Should not change zero vector
		REQUIRE(zeroVec.x == Approx(0.0f).margin(EPSILON));
		REQUIRE(zeroVec.y == Approx(0.0f).margin(EPSILON));
		REQUIRE(zeroVec.z == Approx(0.0f).margin(EPSILON));
	}

	SECTION("Distance")
	{
		float dist = V3::distance(a, b); // Should be sqrt(2)
		REQUIRE(dist == Approx(std::sqrt(2.0f)).margin(EPSILON));
	}

	SECTION("Length")
	{
		float len = c.length(); // Should be 1
		REQUIRE(len == Approx(1.0f).margin(EPSILON));
	}

	SECTION("Dot product with operator")
	{
		float result = a | b; // Should be 0
		REQUIRE(result == Approx(0.0f).margin(EPSILON));
	}

	// Other tests could include +=, -=, unary -, .norm(), .normalized(),
	// constructors from other types of vectors (if applicable), etc.
}

TEST_CASE("Vector in-place arithmetic", "[vectormath]")
{
	V3 v(1.0f, 2.0f, 3.0f);
	V3 u(-3.0f, -4.0f, -5.0f);

	SECTION("In-place addition")
	{
		v += u; // Should be (-2, -2, -2)
		REQUIRE(v.x == Approx(-2.0f).margin(EPSILON));
		REQUIRE(v.y == Approx(-2.0f).margin(EPSILON));
		REQUIRE(v.z == Approx(-2.0f).margin(EPSILON));
	}

	SECTION("In-place subtraction")
	{
		v -= u; // Should be (4, 6, 8)
		REQUIRE(v.x == Approx(4.0f).margin(EPSILON));
		REQUIRE(v.y == Approx(6.0f).margin(EPSILON));
		REQUIRE(v.z == Approx(8.0f).margin(EPSILON));
	}
}

TEST_CASE("Vector unary operations", "[vectormath]")
{
	V3 v(5.0f, -6.0f, 7.0f);

	SECTION("Unary minus")
	{
		V3 negated = -v; // Should be (-5, 6, -7)
		REQUIRE(negated.x == Approx(-5.0f).margin(EPSILON));
		REQUIRE(negated.y == Approx(6.0f).margin(EPSILON));
		REQUIRE(negated.z == Approx(-7.0f).margin(EPSILON));
	}
}

TEST_CASE("Vector normalization operations", "[vectormath]")
{
	V3 a(4.0f, 0.0f, 3.0f);

	SECTION("Normalized vector")
	{
		V3 normalized = a.normalized(); // Should return a new normalized vector
		REQUIRE(normalized.length() == Approx(1.0f).margin(EPSILON));
		// Original vector should remain unchanged
		REQUIRE(a == V3(4.0f, 0.0f, 3.0f));
	}

	SECTION("Normalize zero length vector")
	{
		V3 zeroVec(0.0f, 0.0f, 0.0f);
		zeroVec.normalize();
		// The result is undefined, so no equality check will be performed here.
		// However, we document that the behavior needs to be checked against the implementation.
	}
}

// Additional features can be tested, including constructors from different types of vectors
// (if any), projection, reflection, or any other vector-related functionalities provided.
