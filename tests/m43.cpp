/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "common.h"

auto isIdentityMatrix(const M43 &matrix) -> bool
{
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 3; ++col)
		{
			float expectedValue = (row == col) ? 1.0f : 0.0f;
			if (std::abs(matrix.m[row][col] - expectedValue) > EPSILON)
			{
				return false;
			}
		}
	}
	return true;
}

// Define a helper function to compare matrices with an epsilon value
auto areMatricesEqual(const M43 &a, const M43 &b, float epsilon = EPSILON) -> bool
{
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 3; ++col)
		{
			if (std::abs(a.m[row][col] - b.m[row][col]) > epsilon)
			{
				return false;
			}
		}
	}
	return true;
}
#if 0
enum rotation_order_type
{
	XYZ,
	XZY,
	YXZ,
	YZX,
	ZXY,
	ZYX
};

class Q
{
public:
	float x, y, z, w;
};

class M43
{
public:
	float m[4][3];

	M43();
	static auto translate(const V3 &_v) -> M43;
	static auto translate(float _x, float _y, float _z) -> M43;
	static auto scale(const V3 &_v) -> M43;
	static auto scale(float _x, float _y, float _z) -> M43;
	static auto scale(float _u) -> M43;
	static auto rotate(const V3 &_axis, float _angleInDegree) -> M43;
	static auto euler(rotation_order_type _order, const V3 &_v) -> M43;
	M43(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22, float m30, float m31, float m32);
	M43(const V3 &_r0, const V3 &_r1, const V3 &_r2, const V3 &_r3);
	M43(const V4 &_r0, const V4 &_r1, const V4 &_r2);
	auto operator*(const M43 &_rhs) const -> M43;
	auto operator*(const M4 &_rhs) const -> M4;
	auto invert() -> M43 &;
	auto inverted() const -> M43;
	static auto quaternion(const Q &_q) -> M43;
	auto transform(const V3 &_v) const -> V3;
	auto operator*(const V3 &_v) const -> V3;
	auto transpose() -> M43 &;
	auto transposed() const -> M43;
};
#endif

// Test Case: Default Constructor Initializes to Identity Matrix
TEST_CASE("M43 - Default constructor initializes to identity matrix", "[M43]")
{
	M43 matrix;
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 3; ++col)
		{
			if (row == col)
			{
				REQUIRE(matrix.m[row][col] == Approx(1.0f));
			}
			else
			{
				REQUIRE(matrix.m[row][col] == Approx(0.0f));
			}
		}
	}
}

// Test Case: Translate creates translation matrix
TEST_CASE("M43 - Translate function creates correct translation matrix", "[M43]")
{
	float tx = 10.0f, ty = 5.0f, tz = -7.0f;
	M43 matrix = M43::translate(tx, ty, tz);

	REQUIRE(matrix.m[3][0] == Approx(tx));
	REQUIRE(matrix.m[3][1] == Approx(ty));
	REQUIRE(matrix.m[3][2] == Approx(tz));
}

// Test Case: Scale creates scaling matrix
TEST_CASE("M43 - Scale function creates correct scaling matrix", "[M43]")
{
	float sx = 2.0f, sy = 3.0f, sz = 4.0f;
	M43 matrix = M43::scale(sx, sy, sz);

	REQUIRE(matrix.m[0][0] == Approx(sx));
	REQUIRE(matrix.m[1][1] == Approx(sy));
	REQUIRE(matrix.m[2][2] == Approx(sz));
}

// Test Case: Uniform Scale creates uniform scaling matrix
TEST_CASE("M43 - Uniform scale function creates correct uniform scaling matrix", "[M43]")
{
	float s = 5.0f;
	M43 matrix = M43::scale(s);

	REQUIRE(matrix.m[0][0] == Approx(s));
	REQUIRE(matrix.m[1][1] == Approx(s));
	REQUIRE(matrix.m[2][2] == Approx(s));
}

// Test Case: Multiplication by vector
TEST_CASE("M43 - Multiplication by V3 is correct", "[M43]")
{
	V3 vec(1.0f, 2.0f, 3.0f);
	M43 matrix = M43::scale(2.0f); // Using uniform scale for simplicity

	V3 result = matrix * vec;

	REQUIRE(result.x == Approx(2.0f));
	REQUIRE(result.y == Approx(4.0f));
	REQUIRE(result.z == Approx(6.0f));
}

// Test Case: Rotation around an axis
TEST_CASE("M43 - Rotation around an axis is correct", "[M43]")
{
	V3 axis(0.0f, 1.0f, 0.0f); // Y-axis
	float angleInRadians = deg2rad(90.0f);
	M43 rotationMatrix = M43::rotate(axis, angleInRadians);

	// Assuming V3 implements the multiplication properly and an identity vector exists.
	V3 originalVector(1.0f, 0.0f, 0.0f); // X-axis vector
	V3 rotatedVector = rotationMatrix * originalVector;

	REQUIRE_THAT(rotatedVector.x, Catch::Matchers::WithinAbs(0.0f, EPSILON));
	REQUIRE(rotatedVector.y == Approx(0.0f));
	REQUIRE(rotatedVector.z == Approx(-1.0f));
}

TEST_CASE("M43 - Euler angle rotation produces correct rotation matrix", "[M43]")
{
	V3 eulerAngles(45.0f, 45.0f, 45.0f); // Some arbitrary Euler angles
	rotation_order_type order = XYZ;
	M43 matrix = M43::euler(order, eulerAngles);

	// Perform validations similar to the one we would do with axis-angle rotations
	// It's generally complex to verify correctness of this without a solid reference.
	// Here are simplified checks instead.
	bool hasNonZeroEntries = false;
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 3; ++col)
		{
			if (!isIdentityMatrix(matrix) && std::abs(matrix.m[row][col]) > EPSILON)
			{
				hasNonZeroEntries = true;
				break;
			}
		}
	}
	REQUIRE(hasNonZeroEntries);
}

// Test Case: Inversion of a matrix
TEST_CASE("M43 - Inversion of a matrix is correct", "[M43]")
{
	// Create a non-singular matrix
	M43 matrix(3.0f, 0.0f, 2.0f, 2.0f, 0.0f, -2.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
	M43 inverseMatrix = matrix.inverted();

	// Identity matrix expected when multiplying with its inverse
	M43 identity = matrix * inverseMatrix;

	M43 expectedIdentity;
	REQUIRE(areMatricesEqual(identity, expectedIdentity));
}

// Test Case: Matrix multiplication with identity matrix
TEST_CASE("M43 - Matrix multiplication with identity matrix preserves original matrix", "[M43]")
{
	M43 identityMatrix;
	M43 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f);

	M43 resultLeft = identityMatrix * matrix;
	M43 resultRight = matrix * identityMatrix;

	REQUIRE(areMatricesEqual(resultLeft, matrix));
	REQUIRE(areMatricesEqual(resultRight, matrix));
}

// Test Case: Inversion of identity matrix
TEST_CASE("M43 - Inversion of identity matrix results in identity matrix", "[M43]")
{
	M43 identityMatrix;
	M43 invertedIdentity = identityMatrix.inverted();

	REQUIRE(areMatricesEqual(identityMatrix, invertedIdentity));
}

// Test Case: Non-invertible matrix throws or returns special case
TEST_CASE("M43 - Inverting a non-invertible matrix throws or signals error", "[M43]")
{
	M43 nonInvertibleMatrix(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);

	// Since there's no information on whether an exception should be expected or
	// an invalid matrix returned, both scenarios can be tested if applicable
	bool exceptionThrown = false;
	try
	{
		M43 inverseMatrix = nonInvertibleMatrix.inverted();
	}
	catch (const std::exception &)
	{
		exceptionThrown = true;
	}

	REQUIRE(exceptionThrown);
	// OR for a signaling error through the method itself
	// REQUIRE(nonInvertibleMatrix.inverted().isInvalid());
}

// Test Case: Transpose operation on a matrix
TEST_CASE("M43 - Transposition of a matrix is correct", "[M43]")
{
	M43 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f);
	M43 transposedMatrix = matrix.transposed();

	for (int row = 0; row < 3; ++row)
	{
		for (int col = 0; col < 3; ++col)
		{
			REQUIRE(transposedMatrix.m[col][row] == Approx(matrix.m[row][col]).epsilon(EPSILON));
		}
	}
}

// Test Case: Transformation of a vector by a matrix
TEST_CASE("M43 - Transforming V3 by M43 is correct", "[M43]")
{
	M43 matrix = M43::translate(5.0f, -3.0f, 2.0f);
	V3 originalVec(1.0f, 2.0f, 3.0f);
	V3 transformedVec = matrix.transform(originalVec);

	REQUIRE(transformedVec.x == Approx(originalVec.x + 5.0f).epsilon(EPSILON));
	REQUIRE(transformedVec.y == Approx(originalVec.y - 3.0f).epsilon(EPSILON));
	REQUIRE(transformedVec.z == Approx(originalVec.z + 2.0f).epsilon(EPSILON));
}

// Test Case: Operator* for Matrix-Matrix multiplication is correct
TEST_CASE("M43 - Multiplication by another M43 is correct", "[M43]")
{
	// Create two transformation matrices
	M43 matrix1 = M43::translate(1.0f, 2.0f, 3.0f);
	M43 matrix2 = M43::scale(2.0f);

	// Multiplying a translation by scaling should result in combined transformation
	M43 combinedMatrix = matrix1 * matrix2;

	// Verify that the result is a combined transformation matrix
	V3 transformedVec = combinedMatrix * V3(1.0f, 1.0f, 1.0f);

	REQUIRE(transformedVec.x == Approx(3.0f).epsilon(EPSILON)); // 1*scale + tx
	REQUIRE(transformedVec.y == Approx(4.0f).epsilon(EPSILON)); // 1*scale + ty
	REQUIRE(transformedVec.z == Approx(5.0f).epsilon(EPSILON)); // 1*scale + tz
}

// Test Case: Quaternion to matrix conversion
TEST_CASE("M43 - Quaternion to matrix conversion is correct", "[M43]")
{
	// Define a simple quaternion representing a 90-degree rotation around the Y-axis
	Q quaternion = {0.0f, 0.7071f, 0.0f, 0.7071f}; // w=cos(theta/2), y=sin(theta/2)

	M43 matrixFromQuaternion = M43::quaternion(quaternion);

	// The resulting matrix should perform the same rotation as the quaternion
	V3 originalVector(1.0f, 0.0f, 0.0f); // X-axis
	V3 rotatedVector = matrixFromQuaternion * originalVector;

	// TODO: quaternion calculations are horribly unprecise in this implementation

	REQUIRE(rotatedVector.x == Approx(0.0f).margin(1e-3f));
	REQUIRE(rotatedVector.y == Approx(0.0f).margin(1e-3f));
	REQUIRE(rotatedVector.z == Approx(-1.0f).margin(1e-3f)); // Rotated to the -Z axis
}

// Since we already have a helper method 'areMatricesEqual', we can use it for further tests.
// For example:

// Test Case: Transposed twice should yield the original matrix
TEST_CASE("M43 - Transposing the transposed matrix results in the original matrix", "[M43]")
{
	M43 originalMatrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f);

	M43 transposedTwice = originalMatrix.transposed().transposed();

	REQUIRE(areMatricesEqual(originalMatrix, transposedTwice));
}
