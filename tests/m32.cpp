/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "common.h"

using namespace Catch;
using namespace Catch::Matchers;

static M32 unit{1, 0, 0, 1, 0, 0};

class CheckM32 : public MatcherBase<M32>
{
	M32 m;

public:
	CheckM32(const M32 &_m)
	  : m(_m)
	{
	}

	static inline bool eq(float _a, float _b)
	{
		return std::fabs(_b - _a) < 0.0001f;
	}

	bool match(const M32 &_m) const override
	{
		return eq(m.m[0][0], _m.m[0][0]) && eq(m.m[0][1], _m.m[0][1]) && eq(m.m[1][0], _m.m[1][0]) && eq(m.m[1][1], _m.m[1][1]) && eq(m.m[2][0], _m.m[2][0]) &&
			   eq(m.m[2][1], _m.m[2][1]);
	}

	std::string describe() const override
	{
		std::ostringstream ss;
		ss << m;
		return ss.str();
	}
};

// The builder function
inline CheckM32 IsEqual(const M32 &_m)
{
	return CheckM32(_m);
}

TEST_CASE("M32 is unit by default")
{
	M32 m;

	REQUIRE_THAT(m, IsEqual(unit));
}

TEST_CASE("M32 construct")
{
	M32 m(1, 2, 3, 4, 5, 6);

	REQUIRE_THAT(m, IsEqual({1, 2, 3, 4, 5, 6}));
}

TEST_CASE("M32.translate(x,y)")
{
	auto m = M32::translate(2, 3);

	REQUIRE_THAT(m, IsEqual({1, 0, 0, 1, 2, 3}));
}

TEST_CASE("M32.translate(V2)")
{
	auto m = M32::translate({2, 3});

	REQUIRE_THAT(M32::translate({2, 3}), IsEqual({1, 0, 0, 1, 2, 3}));
}

TEST_CASE("M32.scale(x,y)")
{
	auto m = M32::scale(2, 3);

	REQUIRE_THAT(m, IsEqual({2, 0, 0, 3, 0, 0}));
}

TEST_CASE("M32.scale(u)")
{
	auto m = M32::scale(2);

	REQUIRE_THAT(m, IsEqual({2, 0, 0, 2, 0, 0}));
}

TEST_CASE("M32.scale(V2)")
{
	auto m = M32::scale({2, 3});

	REQUIRE_THAT(m, IsEqual({2, 0, 0, 3, 0, 0}));
}

TEST_CASE("M32.rotate(a)")
{
	/*
	  ⎧ cos(ɑ) -sin(ɑ) x ⎫
	  ⎩ sin(ɑ)  cos(ɑ) y ⎭

		cos( 0°) = 1
		cos(90°) = 0		cos(-90°) =  0
		sin( 0°) = 0
		sin(90°) = 1		sin(-90°) = -1

		sin(45°) == cos(45°) = √2/2 = 0.707106781...~ 0.70711
	*/

	auto m = M32::rotate(0);

	/*
	  ⎧ cos(0°) -sin(0°) x ⎫ ⎧ 1 0 x ⎫
	  ⎩ sin(0°)  cos(0°) y ⎭ ⎩ 0 1 y ⎭
	*/

	REQUIRE_THAT(m, IsEqual({1, 0, 0, 1, 0, 0}));

	m = M32::rotate(360);

	/*
		360° -> 0°
	  ⎧ cos(0°) -sin(0°) x ⎫ ⎧ 1 0 x ⎫
	  ⎩ sin(0°)  cos(0°) y ⎭ ⎩ 0 1 y ⎭
	*/

	REQUIRE_THAT(m, IsEqual({1, 0, 0, 1, 0, 0}));

	m = M32::rotate(-90);

	/*
	  ⎧ cos(-90°) -sin(-90°) x ⎫ ⎧ 0 1 x ⎫
	  ⎩ sin(-90°)  cos(-90°) y ⎭ ⎩-1 0 y ⎭
	*/

	REQUIRE_THAT(m, IsEqual({0, 1, -1, 0, 0, 0}));

	m = M32::rotate(+90);

	/*
	  ⎧ cos(90°) -sin(90°) x ⎫ ⎧ 0 -1 x ⎫
	  ⎩ sin(90°)  cos(90°) y ⎭ ⎩ 1  0 y ⎭
	*/

	REQUIRE_THAT(m, IsEqual({0, -1, 1, 0, 0, 0}));

	m = M32::rotate(45);

	/*
	  ⎧ cos(45°) -sin(45°) x ⎫ ⎧ √2/2 -√2/2 x ⎫
	  ⎩ sin(45°)  cos(45°) y ⎭ ⎩ √2/2  √2/2 y ⎭
	*/

	REQUIRE_THAT(m, IsEqual({0.70711F, -0.70711F, 0.70711F, 0.70711F, 0, 0}));
}

TEST_CASE("M32 * M32")
{
	/*
		to verify, the 3rd row is printed here, too

		t =
	  ⎧ 1 0 1 ⎫
	  ⎪ 0 1 2 ⎪
		⎩ 0 0 1 ⎭
		s =
	  ⎧ 3 0 0 ⎫
	  ⎪ 0 4 0 ⎪
		⎩ 0 0 1 ⎭
		s*t =
	  ⎧ 1 0 1 ⎫⎧ 3 0 0 ⎫⎧ 3 0 1 ⎫
	  ⎪ 0 1 2 ⎪⎪ 0 4 0 ⎪⎪ 0 4 2 ⎪
		⎩ 0 0 1 ⎭⎩ 0 0 1 ⎭⎩ 0 0 1 ⎭
		t*s =
	  ⎧ 1 0 1 ⎫⎧ 3 0 0 ⎫⎧ 3 0 3 ⎫
	  ⎪ 0 1 2 ⎪⎪ 0 4 0 ⎪⎪ 0 4 8 ⎪
		⎩ 0 0 1 ⎭⎩ 0 0 1 ⎭⎩ 0 0 1 ⎭
	*/

	auto t = M32::translate(1, 2);
	auto s = M32::scale(3, 4);

	auto st = s * t;
	auto ts = t * s;

	REQUIRE_THAT(ts, IsEqual({3, 0, 0, 4, 1, 2}));
	REQUIRE_THAT(st, IsEqual({3, 0, 0, 4, 3, 8}));
}

TEST_CASE("M32.invert unit")
{
	M32 m;

	m.invert();

	REQUIRE_THAT(m, IsEqual({1, 0, 0, 1, 0, 0}));
}

TEST_CASE("M32.invert")
{
	auto t = M32::translate(1, 2);
	auto s = M32::scale(3, 4);
	auto r = M32::rotate(90);

	auto m1 = t.inverted();
	auto m2 = s.inverted();
	auto m3 = r.inverted();

	auto m = s * r * t;
	auto m4 = m * m.inverted();
	auto m5 = m.inverted() * m;
	m.invert();

	// inverted translate
	REQUIRE_THAT(m1, IsEqual({1, 0, 0, 1, -1, -2}));

	// inverted scale
	REQUIRE_THAT(m2, IsEqual({0.33333F, 0, 0, 0.25f, 0, 0}));

	// inverted rotate
	REQUIRE_THAT(m3, IsEqual({0, 1, -1, 0, 0, 0}));

	// product with inverse: always unit
	REQUIRE_THAT(m4, IsEqual({1, 0, 0, 1, 0, 0}));
	REQUIRE_THAT(m5, IsEqual({1, 0, 0, 1, 0, 0}));

	REQUIRE_THAT(m, IsEqual({0, 0.33333f, -0.25f, 0, -1, -2}));
}

TEST_CASE("M32.transpose")
{
	M32 m = {1, 2, 3, 4, 5, 6};

	m.transpose();

	REQUIRE_THAT(m, IsEqual({1, 3, 2, 4, 5, 6}));
}

TEST_CASE("M32.determinant")
{
	M32 m = {1, 2, 3, 4, 5, 6};

	float a = m.determinant();

	float b = m.inverted().determinant();

	m = {0, 0, 0, 0, 0, 0};

	float c = m.determinant();

	REQUIRE(a < 0.0f);
	REQUIRE(b == -0.5f);
	REQUIRE(c == 0.0f);
}

// todo:
// op* V2
// transform
// determinant
