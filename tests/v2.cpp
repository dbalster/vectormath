/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "common.h"

TEST_CASE("Vector2D initialization", "[vectormath]")
{
	SECTION("Default constructor")
	{
		V2 v;
		REQUIRE(v.x == Approx(0.0f).margin(EPSILON));
		REQUIRE(v.y == Approx(0.0f).margin(EPSILON));
	}

	SECTION("Parameterized constructor")
	{
		V2 v(1.0f, -2.0f);
		REQUIRE(v.x == Approx(1.0f).margin(EPSILON));
		REQUIRE(v.y == Approx(-2.0f).margin(EPSILON));
	}

	SECTION("Copy constructor")
	{
		V2 orig(1.0f, -2.0f);
		V2 copy(orig);
		REQUIRE(copy.x == Approx(orig.x).margin(EPSILON));
		REQUIRE(copy.y == Approx(orig.y).margin(EPSILON));
	}
}

TEST_CASE("Vector2D arithmetic", "[vectormath]")
{
	V2 a(1.0f, 2.0f);
	V2 b(-1.0f, -2.0f);
	float scalar = 2.0f;

	SECTION("Addition")
	{
		V2 result = a + b; // Should be (0, 0)
		REQUIRE(result.x == Approx(0.0f).margin(EPSILON));
		REQUIRE(result.y == Approx(0.0f).margin(EPSILON));
	}

	SECTION("Subtraction")
	{
		V2 result = a - b; // Should be (2, 4)
		REQUIRE(result.x == Approx(2.0f).margin(EPSILON));
		REQUIRE(result.y == Approx(4.0f).margin(EPSILON));
	}

	SECTION("Scalar multiplication")
	{
		V2 result = a * scalar; // Should be (2, 4)
		REQUIRE(result.x == Approx(2.0f).margin(EPSILON));
		REQUIRE(result.y == Approx(4.0f).margin(EPSILON));
	}

	SECTION("Dot product")
	{
		float result = a.dot(b); // Should be -5
		REQUIRE(result == Approx(-5.0f).margin(EPSILON));
	}
}

TEST_CASE("Vector2D operations", "[vectormath]")
{
	V2 a(1.0f, 0.0f);
	V2 b(0.0f, 1.0f);

	SECTION("Normalization")
	{
		a.normalize();
		REQUIRE(a.length() == Approx(1.0f).margin(EPSILON));

		V2 zeroVec;
		zeroVec.normalize(); // Normalizing a zero vector may result in undefined behavior.
							 // so we skip checking the result
	}

	SECTION("Distance")
	{
		float dist = V2::distance(a, b); // Should be sqrt(2)
		REQUIRE(dist == Approx(std::sqrt(2.0f)).margin(EPSILON));
	}

	SECTION("Length")
	{
		float len = b.length(); // Should be 1
		REQUIRE(len == Approx(1.0f).margin(EPSILON));
	}

	// In reality, operator overloading for multiplying two V2s directly to get a scalar might not be common.
	// It's more usual to call a specific method like dot().
	SECTION("Dot product with operator")
	{
		float result = a * b; // Should be 0
		REQUIRE(result == Approx(0.0f).margin(EPSILON));
	}
}

TEST_CASE("Vector2D in-place operations", "[vectormath]")
{
	V2 v(1.0f, 2.0f);
	V2 u(-3.0f, -4.0f);

	SECTION("In-place addition")
	{
		v += u; // Should be (-2, -2)
		REQUIRE(v.x == Approx(-2.0f).margin(EPSILON));
		REQUIRE(v.y == Approx(-2.0f).margin(EPSILON));
	}

	SECTION("In-place subtraction")
	{
		v -= u; // Should be (4, 6)
		REQUIRE(v.x == Approx(4.0f).margin(EPSILON));
		REQUIRE(v.y == Approx(6.0f).margin(EPSILON));
	}
}

TEST_CASE("Vector2D unary operations", "[vectormath]")
{
	V2 v(5.0f, -6.0f);

	SECTION("Unary minus")
	{
		V2 negated = -v; // Should be (-5, 6)
		REQUIRE(negated.x == Approx(-5.0f).margin(EPSILON));
		REQUIRE(negated.y == Approx(6.0f).margin(EPSILON));
	}
}

TEST_CASE("Vector2D normalization operations", "[vectormath]")
{
	V2 a(3.0f, 4.0f);

	SECTION("Normalized vector")
	{
		V2 normalized = a.normalized(); // Should not modify 'a' and should return a normalized vector
		REQUIRE(normalized.length() == Approx(1.0f).margin(EPSILON));
		REQUIRE(a.length() == Approx(5.0f).margin(EPSILON)); // Original vector should remain unchanged
	}

	SECTION("Normalize zero length vector")
	{
		V2 zeroVec(0.0f, 0.0f);
		zeroVec.normalize();
		// Since the result is undefined, we won't perform an equality check
		// This section serves as documentation that the behavior has been considered and tested
	}
}
