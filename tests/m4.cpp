/*
	a quick & dirty 3D game engine for text terminals

  MIT License

  Copyright (c) 2023 Daniel Balster

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

#include "common.h"

// Tests for M4 constructor initializing identity matrix.
TEST_CASE("M4 default constructor initializes identity matrix", "[M4]")
{
	const M4 matrix;
	// Expected identity matrix
	const float expected[4][4] = {{1.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 1.0f}};
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			REQUIRE(matrix.m[row][col] == Approx(expected[row][col]));
		}
	}
}

// Tests for M4::translate static method with vector argument.
TEST_CASE("M4::translate creates translation matrix from vector", "[M4]")
{
	const V3 translationVector(2.0f, 3.0f, 4.0f);
	const M4 translationMatrix = M4::translate(translationVector);
	// Expected translation matrix
	const M4 expected = {1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 2.0f, 3.0f, 4.0f, 1.0f};

	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			REQUIRE(translationMatrix.m[row][col] == Approx(expected.m[row][col]));
		}
	}
}

// Tests for M4::scale static method.
TEST_CASE("M4::scale creates scale matrix from scalars", "[M4]")
{
	const float scaleX = 2.0f, scaleY = 3.0f, scaleZ = 4.0f;
	const M4 scaleMatrix = M4::scale(scaleX, scaleY, scaleZ);
	// Expected scale matrix
	const float expected[4][4] = {{2.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 3.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 4.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 1.0f}};
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			REQUIRE(scaleMatrix.m[row][col] == Approx(expected[row][col]));
		}
	}
}

// Tests for M4::rotate static method.
TEST_CASE("M4::rotate creates rotation matrix from axis and angle", "[M4]")
{
	const V3 axis(0.0f, 1.0f, 0.0f); // Y-axis
	const float angle = 90.0f;		 // 90 degrees
	const M4 rotationMatrix = M4::rotate(axis, angle);

	// Since checking complete rotation matrix requires more complex validation,
	// here we only check if some of the values match expected rotation around Y-axis.
	REQUIRE(rotationMatrix.m[0][0] == Approx(std::cos(M_PI / 2.0)).margin(EPSILON));  // Cosine of 90 degrees
	REQUIRE(rotationMatrix.m[2][2] == Approx(std::cos(M_PI / 2.0)).margin(EPSILON));  // Cosine of 90 degrees
	REQUIRE(rotationMatrix.m[0][2] == Approx(-std::sin(M_PI / 2.0)).margin(EPSILON)); // Negative sine of 90 degrees
	REQUIRE(rotationMatrix.m[2][0] == Approx(std::sin(M_PI / 2.0)).margin(EPSILON));  // Sine of 90 degrees
																					  // Other elements should be checked as well.
}
// Tests for M4::perspectiveFovLH method.
TEST_CASE("M4::perspectiveFovLH creates correct left-handed perspective FOV matrix", "[M4]")
{
	const float yFov = 60.0f;		   // Field of view in degrees
	const float aspect = 16.0f / 9.0f; // Aspect ratio
	const float nearZ = 0.1f;		   // Near plane Z distance
	const float farZ = 100.0f;		   // Far plane Z distance
	M4 perspMatrix;
	perspMatrix.perspectiveFovLH(yFov, aspect, nearZ, farZ);

	// Check the first cell of the matrix to confirm it matches the x-scale (xₛ) part
	REQUIRE(perspMatrix.m[1][1] == Approx(1.0f / tan((yFov * (M_PI / 180)) / 2.0f)));

	// Additional checks should verify other cells and properties of a perspective projection matrix.
}

// Tests for M4::ortho method.
TEST_CASE("M4::ortho creates correct orthographic projection matrix", "[M4]")
{
	const float left = -1.0f, right = 1.0f;
	const float top = 1.0f, bottom = -1.0f;
	const float nearZ = 0.1f, farZ = 100.0f;
	M4 orthoMatrix = M4().ortho(left, right, top, bottom, nearZ, farZ);

	// Check if the diagonal elements are set correctly.
	REQUIRE(orthoMatrix.m[0][0] == Approx(2.0f / (right - left)));
	REQUIRE(orthoMatrix.m[1][1] == Approx(2.0f / (top - bottom)));
	REQUIRE(orthoMatrix.m[2][2] == Approx(-2.0f / (farZ - nearZ)));

	// Additional checks for orthographic properties can be implemented here.
}

// Tests for M4::invert method.
TEST_CASE("M4::invert inverts a matrix correctly", "[M4]")
{
	M4 originalMatrix = M4::translate(1.0f, 2.0f, 3.0f);
	M4 invertedMatrix = originalMatrix.inverted();

	M4 identity = originalMatrix * invertedMatrix;

	// Check if the product of the matrix and its inverse results in an identity matrix.
	const float expectedIdentity[4][4] = {{1.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 1.0f}};
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			REQUIRE(identity.m[row][col] == Approx(expectedIdentity[row][col]));
		}
	}
}

// Tests for M4::transposed method.
TEST_CASE("M4::transposed returns the transpose of a matrix", "[M4]")
{
	M4 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f, 16.0f);
	M4 transposedMatrix = matrix.transposed();

	// Check if the rows and columns have been swapped.
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			REQUIRE(transposedMatrix.m[row][col] == Approx(matrix.m[col][row]));
		}
	}
}

// Additional test cases should be added to cover all possible operations,
// edge cases, and special scenarios that might occur when using the methods
// from M4 class.
// Tests for M4::invert method producing an identity matrix upon self-multiplication.
TEST_CASE("Multiplying a matrix by its inverse should produce an identity matrix", "[M4]")
{
	M4 m(1, 0, 2, 3, 0, 4, 5, 6, 7, 0, 8, 9, 1, 0, 0, 5);

	auto inv = m.inverted();
	auto result = m * inv;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			REQUIRE(result.m[i][j] == Approx(i == j ? 1.0f : 0.0f).margin(EPSILON));
		}
	}
}

// Tests for M4 operator* overloading for matrix multiplication.
TEST_CASE("M4 operator* multiplies matrices correctly", "[M4]")
{
	M4 a(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f, 16.0f);
	M4 b(16.0f, 15.0f, 14.0f, 13.0f, 12.0f, 11.0f, 10.0f, 9.0f, 8.0f, 7.0f, 6.0f, 5.0f, 4.0f, 3.0f, 2.0f, 1.0f);

	/*
		⎧ (1*16 + 2*12 + 3*8 + 4*4)     (1*15 + 2*11 + 3*7 + 4*3)     (1*14 + 2*10 + 3*6 + 4*2)     (1*13 + 2*9 + 3*5 + 4*1)     ⎫
		⎪ (5*16 + 6*12 + 7*8 + 8*4)     (5*15 + 6*11 + 7*7 + 8*3)     (5*14 + 6*10 + 7*6 + 8*2)     (5*13 + 6*9 + 7*5 + 8*1)     ⎪
		⎪ (9*16 + 10*12 + 11*8 + 12*4)  (9*15 + 10*11 + 11*7 + 12*3)  (9*14 + 10*10 + 11*6 + 12*2)  (9*13 + 10*9 + 11*5 + 12*1)  ⎪
		⎩ (13*16 + 14*12 + 15*8 + 16*4) (13*15 + 14*11 + 15*7 + 16*3) (13*14 + 14*10 + 15*6 + 16*2) (13*13 + 14*9 + 15*5 + 16*1) ⎭

		⎧  80  70  60  50 ⎫
		⎪ 240 214 188 162 ⎪
		⎪ 400 358 316 274 ⎪
		⎩ 560 502 444 386 ⎭
	*/

	M4 result = b * a;
	M4 const expected = {80, 70, 60, 50, 240, 214, 188, 162, 400, 358, 316, 274, 560, 502, 444, 386};

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			REQUIRE(result.m[i][j] == Approx(expected.m[i][j]).margin(EPSILON));
		}
	}
}

// Tests for M4 operator* overloading with vector V3.
TEST_CASE("M4 operator* applies matrix transform to V3 correctly", "[M4]")
{
	M4 m(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 3.0f, 4.0f, 5.0f, 1.0f);
	V3 v(1.0f, 2.0f, 3.0f);

	V3 transformed = m * v;
	// Expected results considering that m represents a translation matrix.
	REQUIRE(transformed.x == Approx(4.0f).margin(EPSILON));
	REQUIRE(transformed.y == Approx(6.0f).margin(EPSILON));
	REQUIRE(transformed.z == Approx(8.0f).margin(EPSILON));
}

// Tests for M4::quaternion static method.
TEST_CASE("M4::quaternion creates a rotation matrix from a quaternion", "[M4]")
{
	Q quat(0.0f, 0.0f, sqrt(2) / 2, sqrt(2) / 2); // Represents 90 degree rotation around Z-axis
	M4 rotationMatrix = M4::quaternion(quat);

	REQUIRE(rotationMatrix.m[0][0] == Approx(0.0f).margin(EPSILON));
	REQUIRE(rotationMatrix.m[0][1] == Approx(1.0f).margin(EPSILON));
	REQUIRE(rotationMatrix.m[1][0] == Approx(-1.0f).margin(EPSILON));
	REQUIRE(rotationMatrix.m[1][1] == Approx(0.0f).margin(EPSILON));
	// Other elements should be checked as well.
}

// Tests for M4::invert method ensuring an inverted identity matrix is still an identity matrix.
TEST_CASE("Inverting an identity matrix should produce an identity matrix", "[M4]")
{
	M4 identity;
	M4 invertedIdentity = identity.inverted();

	// The inverted matrix should also be an identity matrix.
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			REQUIRE(invertedIdentity.m[row][col] == Approx(row == col ? 1.0f : 0.0f));
		}
	}
}

// Tests for M4 operator* overloading with vector V4.
TEST_CASE("M4 operator* applies matrix transform to V4 correctly", "[M4]")
{
	M4 m(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 3.0f, 4.0f, 5.0f, 1.0f);
	V4 v(1.0f, 2.0f, 3.0f, 1.0f);

	V4 transformed = m * v;

	REQUIRE(transformed.x == Approx(4.0f).margin(EPSILON));
	REQUIRE(transformed.y == Approx(6.0f).margin(EPSILON));
	REQUIRE(transformed.z == Approx(8.0f).margin(EPSILON));
	REQUIRE(transformed.w == Approx(1.0f).margin(EPSILON));
}

// Tests for M4::transpose method checking the in-place transpose operation.
TEST_CASE("M4::transpose performs in-place transposition of a matrix", "[M4]")
{
	M4 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f, 16.0f);
	matrix.transpose();

	// Check if the rows and columns have been swapped in the original matrix.
	const float expectedTransposed[4][4] = {{1.0f, 5.0f, 9.0f, 13.0f}, {2.0f, 6.0f, 10.0f, 14.0f}, {3.0f, 7.0f, 11.0f, 15.0f}, {4.0f, 8.0f, 12.0f, 16.0f}};

	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			REQUIRE(matrix.m[row][col] == Approx(expectedTransposed[row][col]));
		}
	}
}

// More tests can be added for other methods like euler angle conversions, perspectiveRH/LH, persp, frustum, etc.
