#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file

#include "vectormath.h" // Assuming vectormath.h is the corresponding header with V2 class definition
#include <catch2/catch.hpp>
#include <iostream>

constexpr float EPSILON = 1e-4f;
